//===-- LexerTests.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/Lexer/Lexer.h"
#include <functional>
#include <gtest/gtest.h>
#include <memory>

class LexerTests : public ::testing::Test {
protected:
  std::unique_ptr<KT::Lexer> lexer;
  void SetUp() override { lexer = std::make_unique<KT::Lexer>(""); }
};

TEST_F(LexerTests, DemoTest) { EXPECT_EQ(4, (2 + 2)); }
