set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

get_filename_component(comp_command_json "compile_commands.json"
  REALPATH BASE_DIR "${PROJECT_BINARY_DIR}")
file(RELATIVE_PATH comp_command_json_rel "${PROJECT_SOURCE_DIR}" "${comp_command_json}")

# need to check if file exists.
if(NOT EXISTS ${PROJECT_SOURCE_DIR}/compile_commands.json)
  add_custom_target(lnCompileCommands ALL
    COMMAND ln -s ${comp_command_json_rel} compile_commands.json
    BYPRODUCTS ${PROJECT_SOURCE_DIR}/compile_commands.json
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    COMMENT "symbolic link compile commands to top level directory for clangd."
    )
endif()
