# embed llvm in project
find_package(LLVM REQUIRED CONFIG)

message(STATUS "Found LLVM ${LLVM_PACKAGE_VERSION}")
message(STATUS "Using LLVMConfig.cmake in: ${LLVM_DIR}")
message(STATUS "Headers: ${LLVM_INCLUDE_DIRS}")
message(STATUS "LLVM_DIR: ${LLVM_DIR}")
message(STATUS "Definitions: ${LLVM_DEFINITIONS}")
message(STATUS "CMakes: ${LLVM_CMAKE_DIR}")

# Use LLVM CMake module, which enable AddLLVM etc.
list(APPEND CMAKE_MODULE_PATH ${LLVM_DIR})

# include(AddLLVM)
# include(HandleLLVMOptions)

# the path of the header files from LLVM is added to the include search path
include_directories(${LLVM_INCLUDE_DIRS})

# llvm defintions
separate_arguments(LLVM_DEFINITIONS_LIST NATIVE_COMMAND ${LLVM_DEFINITIONS})
add_definitions(${LLVM_DEFINITIONS_LIST})

# Now build our tools
#add_executable(simple-tool tool.cpp)

# Find the libraries that correspond to the LLVM components
# that we wish to use
#llvm_map_components_to_libnames(llvm_libs support core irreader)

# Link against LLVM libraries
#target_link_libraries(simple-tool ${llvm_libs})
