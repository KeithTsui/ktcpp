include(GNUInstallDirs)
# # Support Library
# install(TARGETS Utilities
#   EXPORT LCCLibrary
#   ARCHIVE COMPONENT development
#   LIBRARY COMPONENT runtime
#   PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/lcc/Utilities
#   COMPONENT runtime
#   )


# # Assembler library
# install(TARGETS Assembler
#   EXPORT LCCLibrary
#   ARCHIVE COMPONENT development
#   LIBRARY COMPONENT runtime
#   PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/lcc/Assembler
#   COMPONENT runtime
#   )

# if (UNIX)
#   install(CODE "execute_process(COMMAND ldconfig)"
#     COMPONENT runtime
#     )
# endif()

# install(EXPORT LCCLibrary
#   DESTINATION ${CMAKE_INSTALL_LIBDIR}/lcc/cmake
#   NAMESPACE LCC::
#   COMPONENT runtime
#   )

# install(FILES "LCCConfig.cmake"
#   DESTINATION ${CMAKE_INSTALL_LIBDIR}/lcc/cmake
#   )


# CPack configuration
set(CPACK_PACKAGE_VENDOR "Keith Tsui")
set(CPACK_PACKAGE_CONTACT "pixxf@me.com")
set(CPACK_PACKAGE_DESCRIPTION "Keith Compiler in CPP")
include(CPack)
