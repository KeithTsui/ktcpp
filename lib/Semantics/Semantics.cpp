//===-- Semantics.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/Semantics/Semantics.h"
#include "kt/AST/AST.h"
#include "kt/Support/Utilities.h"
#include "kt/SymbolTable/Environment.h"
#include "kt/SymbolTable/Record.h"
#include "kt/SymbolTable/Symbol.h"
#include "kt/SymbolTable/SymbolTable.h"
#include "kt/SymbolTable/Types.h"
#include "kt/Translate/Translate.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/Error.h"
#include <cassert>
#include <cstddef>
#include <iostream>
#include <memory>
#include <ostream>
#include <utility>
#include <vector>

namespace KT {

namespace Sema {

Semantics::Semantics(AST::TigerProgram *p) : program{p} {
  std::cout << "construct sema" << std::endl;
  env_list.push_back(Env::Environment());
  Env::Environment::setBase(getCurrnetScopeEnv());
  std::cout << "construct sema end" << std::endl;
};

std::list<std::unique_ptr<LCCFrame::Fragment>> *Semantics::transProgram() {

  translator.enterFunction("main", {});
  auto prog = transExp(program);

  if (prog.expr) {
    std::cout << "Raw IR: " << std::endl;
    auto rawIR = prog.expr->prettyPrint();
    std::cout << rawIR << std::endl;
  } else {
    std::cout << "empty" << std::endl;
  }

  translator.procEntryExit(translator.currentLevel(), std::move(prog.expr));
  translator.exitFunction();

  return &translator.fragments;
}

Type *Semantics::lookupType(std::string name) {
  Type *ret;
  for (auto i = env_list.rbegin(); i != env_list.rend(); ++i) {
    auto &env = *i;
    ret = env.typeEnv.lookup(name);
  }
  return ret;
}

ValueSymbolTable::Entry *Semantics::lookupValue(std::string name) {
  ValueSymbolTable::Entry *ret;
  for (auto i = env_list.rbegin(); i != env_list.rend(); ++i) {
    auto &env = *i;
    ret = env.valueEnv.lookup(name);
  }
  return ret;
}

void Semantics::insertType(std::string name, std::unique_ptr<Type> &&ty) {
  auto &curEnv = getCurrnetScopeEnv();
  // check if that name exists.
  if (curEnv.typeEnv.lookup(name)) {
    std::string errMsg = "Type name already exists: ";
    errMsg += name;
    llvm::report_fatal_error(errMsg);
  } else {
    curEnv.typeEnv.enter(name, std::move(ty));
  }
}

void Semantics::insertVariable(std::string name, Type *ty,
                               std::unique_ptr<Trans::Access> &&acc) {
  auto &curEnv = getCurrnetScopeEnv();
  if (curEnv.valueEnv.lookup(name)) {
    std::string errMsg = "Type name already exists: ";
    errMsg += name;
    llvm::report_fatal_error(errMsg);
  } else {
    auto var =
        std::make_unique<ValueSymbolTable::VariableEntry>(std::move(acc), ty);
    curEnv.valueEnv.enter(name, std::move(var));
  }
}

void Semantics::insertVariable(std::string name, Type *ty) {
  auto acc = translator.allocLocal();
  insertVariable(name, ty, std::move(acc));
}

void Semantics::insertFunction(std::string name,
                               std::vector<Type *> &&paraTypes, Type *retType) {
  auto &curEnv = getCurrnetScopeEnv();
  if (curEnv.valueEnv.lookup(name)) {
    std::string errMsg = "Type name already exists: ";
    errMsg += name;
    llvm::report_fatal_error(errMsg);
  } else {
    // auto var = std::make_unique<ValueSymbolTable::VariableEntry>(ty);
    auto f = std::make_unique<ValueSymbolTable::FunctionEntry>(
        name, std::move(paraTypes), retType);
    curEnv.valueEnv.enter(name, std::move(f));
  }
}

Semantics::TransResult Semantics::transExp(AST::Expression *exp) {
  if (auto *e = llvm::dyn_cast_or_null<AST::NilExpression>(exp)) {
    return transExpNil(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::IntExpression>(exp)) {
    return transExpInt(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::FloatExpression>(exp)) {
    return transExpFloat(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::StringExpression>(exp)) {
    return transExpString(e);
  } else if (auto *callExp = llvm::dyn_cast_or_null<AST::CallExpression>(exp)) {
    return transExpCall(callExp);
  } else if (auto *e =
                 llvm::dyn_cast_or_null<AST::BinaryOperatorExpression>(exp)) {
    return transExpOp(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::RecordExpression>(exp)) {
    return transExpRecord(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::ArrayExpression>(exp)) {
    return transExpArray(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::SequenceExpression>(exp)) {
    return transExpSeq(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::SimpleVariable>(exp)) {
    return transVarSimple(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::FieldVariable>(exp)) {
    return transVarField(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::SubscriptVariable>(exp)) {
    return transVarSubscript(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::VariableExpression>(exp)) {
    return transExpVar(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::AssignmentStatement>(exp)) {
    return transExpAssign(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::IfStatement>(exp)) {
    return transExpIf(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::WhileStatement>(exp)) {
    return transExpWhile(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::ForStatement>(exp)) {
    return transExpFor(e);
  } else if (auto *e = llvm::dyn_cast_or_null<AST::BreakStatement>(exp)) {
    return transExpBreak(e);
  } else if (auto *let = llvm::dyn_cast_or_null<AST::LetStatement>(exp)) {
    return transExpLet(let);
  } else {
    auto msg = exp->getKindName();
    std::cerr << msg << std::endl;
    llvm_unreachable("not implemented yet");
  }
  return {lookupType("unit")};
}

Semantics::TransResult Semantics::transExpNil(AST::NilExpression *) {
  return {Trans::nilExp(), lookupType("nil")};
}

Semantics::TransResult Semantics::transExpInt(AST::IntExpression *exp) {
  return {translator.intLiteralsToEx(exp->value), lookupType("int")};
}

Semantics::TransResult Semantics::transExpFloat(AST::FloatExpression *exp) {
  // fix me
  return {translator.intLiteralsToEx(exp->value), lookupType("float")};
}

Semantics::TransResult Semantics::transExpString(AST::StringExpression *exp) {
  return {translator.stringLiteralsToEx(exp->value), lookupType("string")};
}

Semantics::TransResult Semantics::transExpCall(AST::CallExpression *callExp) {
  // check argument typs and parameter types are matched in order.
  auto entry = lookupValue(callExp->func.name);
  auto funcEntry = static_cast<ValueSymbolTable::FunctionEntry *>(entry);
  for (size_t i = 0; i < callExp->args.size(); ++i) {
    auto &arg = callExp->args[i];
    auto argTy = transExp(arg.get());
    auto &paraTy = funcEntry->parameterTypes[i];
    assert(*argTy.type == *paraTy && "not the same type");
  }
  return {funcEntry->returnType};
}

Semantics::TransResult
Semantics::transExpOp(AST::BinaryOperatorExpression *exp) {
  auto lexpr = transExp(exp->lhs.get());
  auto rexpr = transExp(exp->rhs.get());
  if (isIn(exp->opr, AST::BinaryOperator::Plus, AST::BinaryOperator::Minus,
           AST::BinaryOperator::Times, AST::BinaryOperator::Divide)) {
    return {
        Trans::binop(exp->opr, std::move(lexpr.expr), std::move(rexpr.expr)),
        lookupType("int")};
  } else {
    return {
        Trans::relOp(exp->opr, std::move(lexpr.expr), std::move(rexpr.expr)),
        lookupType("int")};
  }
}

Semantics::TransResult Semantics::transExpRecord(AST::RecordExpression *exp) {
  llvm_unreachable("not implemented yet");
}

Semantics::TransResult Semantics::transExpArray(AST::ArrayExpression *exp) {
  auto sizeRes = transExp(exp->size.get());
  auto initRes = transExp(exp->initializer.get());

  // return { translator.array(std::move(sizeRes.expr),
  // std::move(initRes.expr)), }
  llvm_unreachable("not implemented yet");
}

Semantics::TransResult Semantics::transExpSeq(AST::SequenceExpression *exp) {
  std::vector<Trans::TExprPtr> exps;
  for (auto &e : exp->exps) {
    exps.push_back(transExp(e.get()).expr);
  }
  auto lastExp = exp->exps.back().get();
  auto last = transExp(lastExp);
  return {translator.sequence(std::move(exps)), last.type};
}

Semantics::TransResult Semantics::transVar(AST::Variable *var) {
  if (auto *svar = llvm::dyn_cast_or_null<AST::SimpleVariable>(var)) {
    return transVarSimple(svar);
  } else if (auto *fieldVar = llvm::dyn_cast_or_null<AST::FieldVariable>(var)) {
    return transVarField(fieldVar);
  } else if (auto *subVar =
                 llvm::dyn_cast_or_null<AST::SubscriptVariable>(var)) {
    return transVarSubscript(subVar);
  } else {
    llvm_unreachable("transvar should be exhausted.");
  }
}

Semantics::TransResult Semantics::transVarSimple(AST::SimpleVariable *svar) {
  // a
  if (auto entry = lookupValue(svar->sym.name)) {
    if (auto *ventry =
            llvm::dyn_cast_or_null<ValueSymbolTable::VariableEntry>(entry)) {
      return {
          translator.simpleVar(ventry->access.get(), translator.currentLevel()),
          ventry->type};

    } else {
      llvm_unreachable("expected variable, but function found");
    }
  } else {
    std::cerr << svar->line << ":" << svar->column << " " << svar->prettyPrint()
              << std::endl;
    llvm_unreachable("undefined variable: ");
  }
}

Semantics::TransResult Semantics::transVarField(AST::FieldVariable *fieldVar) {
  // a.b
  auto res = transVar(fieldVar->var.get());
  if (auto *resTy = llvm::dyn_cast_or_null<RecordType>(res.type)) {
    std::vector<std::string> names;
    for (auto &f : resTy->fields) {
      names.push_back(f.sym.name);
    }
    for (auto &field : resTy->fields) {
      if (field.sym.name == fieldVar->sym.name) {
        return {translator.fieldVar(std::move(res.expr), field.sym.name, names),
                field.type};
      } else {
        llvm_unreachable("id not found");
      }
    }
    llvm_unreachable("unrecognize member");
  } else {
    llvm_unreachable("must be record type");
  }
}

Semantics::TransResult
Semantics::transVarSubscript(AST::SubscriptVariable *subVar) {
  auto res = transVar(subVar->var.get());
  if (auto *resTy = llvm::dyn_cast_or_null<ArrayType>(res.type)) {
    auto expRes = transExp(subVar->exp.get());
    if (*expRes.type == *lookupType("int")) {
      return {
          translator.subscriptVar(std::move(res.expr), std::move(expRes.expr)),
          resTy->type};

    } else {
      llvm_unreachable("array subscript should be int");
    }
  } else {
    llvm_unreachable("must be array type");
  }
}

Semantics::TransResult Semantics::transExpVar(AST::VariableExpression *exp) {
  return transVar(exp->var.get());
}

Semantics::TransResult
Semantics::transExpAssign(AST::AssignmentStatement *assignStm) {

  auto ltype = transVar(assignStm->var.get());
  auto rtype = transExp(assignStm->exp.get());

  // fix me for type equality.
  assert(*ltype.type == *rtype.type && "not the same type");

  return {translator.assign(std::move(ltype.expr), std::move(rtype.expr)),
          ltype.type};
}

Semantics::TransResult Semantics::transExpIf(AST::IfStatement *exp) {
  std::vector<Trans::TExprPtr> stms;
  for (auto &stm : exp->thenBranch) {
    stms.push_back(transExp(stm.get()).expr);
  }
  auto thenBranch = translator.sequence(std::move(stms));

  stms.clear();
  for (auto &stm : exp->thenBranch) {
    stms.push_back(transExp(stm.get()).expr);
  }
  auto elseBranch = translator.sequence(std::move(stms));

  auto test = transExp(exp->condition.get());

  return {translator.ifelse(std::move(test.expr), std::move(thenBranch),
                            std::move(elseBranch)),
          lookupType("unit")};
}

Semantics::TransResult Semantics::transExpWhile(AST::WhileStatement *exp) {
  llvm_unreachable("not implemented yet");
}

Semantics::TransResult Semantics::transExpFor(AST::ForStatement *forStatement) {
  // for i := 0 to 100 do /* for loop for dot product */
  //   sum:
  //     = sum + X[i] * Y[i];
  //     enddo;

  // 1. ensure types of lowerbound and higherbound are int;
  auto ltype = transExp(forStatement->lowerBound.get());
  auto htype = transExp(forStatement->higherBound.get());
  assert(*ltype.type == *htype.type && "not the same type");
  assert(*ltype.type == *lookupType("int") && "not as int");

  // 2. enter a new scope;
  enterScope();
  // 4. insert var into new scope.
  insertVariable(forStatement->var.name, ltype.type);

  // 5. translate body;
  for (auto &stm : forStatement->body) {
    transExp(stm.get());
  }
  // 6. exit new scope;
  exitScope();
}

Semantics::TransResult Semantics::transExpBreak(AST::BreakStatement *exp) {
  return {translator.break_(*breakLabel.get()), lookupType("unit")};
}

Semantics::TransResult Semantics::transExpLet(AST::LetStatement *let) {
  // translate declaration
  for (auto &decl : let->declarations) {
    transDecl(decl.get());
  }

  // translate body statements;
  std::vector<Trans::TExprPtr> transExps;
  for (auto &stm : let->body) {
    auto res = transExp(stm.get());
    if (res.expr) {
      transExps.push_back(std::move(res.expr));
    }
  }
  auto transExpsSeq = translator.sequence(std::move(transExps));

  // no result type;
  return Semantics::TransResult{std::move(transExpsSeq), lookupType("unit")};
}

// fix me
Semantics::TransResult Semantics::transExpReturn(AST::ReturnStatement *exp) {

  llvm_unreachable("not implemented yet");
}

void Semantics::transDecl(AST::Declaration *decl) {
  // update env, type check variable type and its initializer.
  // var a: int = 1 + 2;
  if (auto *var = llvm::dyn_cast_or_null<AST::VariableDeclaration>(decl)) {
    transDeclVar(var);
  } else if (auto *func =
                 llvm::dyn_cast_or_null<AST::FunctionDeclaration>(decl)) {
    transDeclFunc(func);
  } else if (auto *type = llvm::dyn_cast_or_null<AST::TypeDeclaration>(decl)) {
    transDeclType(type);
  } else {
    llvm_unreachable("Not a declaration");
  }
}

void Semantics::transDeclVar(AST::VariableDeclaration *var) {
  auto acc = translator.allocLocal();
  auto varExp = translator.simpleVar(acc.get(), translator.currentLevel());
  // 1. variable has type and has initializer.
  if (var->type && var->initializer) {
    auto varType = transType(var->type.get());
    auto initType = transExp(var->initializer.get());
    if (*varType != *initType.type) {
      assert(false && "type error");
    }
    insertVariable(var->name.name, varType, std::move(acc));
  } else if (var->type &&
             (!var->initializer)) { // 2. var has type and no initializer
    auto varType = transType(var->type.get());
    insertVariable(var->name.name, varType, std::move(acc));
  } else if ((!var->type) &&
             (var->initializer)) { // 3. var no type but initializer
    auto initType = transExp(var->initializer.get());
    insertVariable(var->name.name, initType.type, std::move(acc));
  } else { // 4. var no type and no initializer
    assert(false && "a variable cannot be no type and no initializer");
  }
}

void Semantics::transDeclFunc(AST::FunctionDeclaration *func) {
  auto const &fname = func->name.name;
  auto const &paras = func->parameters;
  auto const &retType = func->resultType;
  auto const &body = func->body;

  // 1. ensure types of parameter and return type exist
  // this time, only support named type of parameter and return type.
  // 1.1 parameter types
  std::vector<Type *> semaTypes(func->parameters.size() + 1);
  for (auto &para : func->parameters) {
    auto ty = para.type.get();
    assert(llvm::isa<AST::NamedType>(ty) &&
           "this time, only support named type of parameter and return type.");
    auto tyName = static_cast<AST::NamedType *>(ty)->name.name;
    auto semaType = lookupType(tyName);
    assert(semaType && "type not exists");
    semaTypes.push_back(semaType);
  }
  // 1.2 return type
  //  may exists or not
  auto *retTy = lookupType("unit");
  if (func->resultType.get()) {
    auto ty = func->resultType.get();
    assert(llvm::isa<AST::NamedType>(ty) &&
           "this time, only support named type of parameter and return type.");
    auto tyName = static_cast<AST::NamedType *>(ty)->name.name;
    auto semaType = lookupType(tyName);
    assert(semaType && "type not exists");
    retTy = semaType;
  }

  // 2. insert function value
  insertFunction(func->name.name, std::move(semaTypes), retTy);
  // 3. enter a new scope;
  enterScope();
  // 4. insert parameters into new scope.
  for (auto &para : func->parameters) {
    auto ty = para.type.get();
    assert(llvm::isa<AST::NamedType>(ty) &&
           "this time, only support named type of parameter and return type.");
    auto tyName = static_cast<AST::NamedType *>(ty)->name.name;
    auto semaType = lookupType(tyName);
    assert(semaType && "type not exists");
    insertVariable(para.name.name, semaType);
  }
  // 5. translate body;
  for (auto &stm : func->body) {
    transExp(stm.get());
  }
  // 6. exit new scope;
  exitScope();

  // type myInt = Int.
}

void Semantics::transDeclType(AST::TypeDeclaration *type) {
  auto semaType = makeTypeFrom(type->type.get());
  insertType(type->name.name, std::move(semaType));
}

std::unique_ptr<Type> Semantics::makeTypeFrom(AST::Type *astType) {
  if (auto *nt = llvm::dyn_cast_or_null<AST::NamedType>(astType)) {
    if (auto ty = lookupType(nt->name.name)) {
      return std::make_unique<AliasType>(ty);
    }
  } else if (auto *rt = llvm::dyn_cast_or_null<AST::RecordType>(astType)) {
    std::vector<RecordType::Field> fields;
    for (auto &field : rt->fields) {
      Symbol fieldName = field.name;
      auto ty = transType(field.type.get());
      fields.emplace_back(fieldName, ty);
    }
    return std::make_unique<RecordType>(std::move(fields));
  } else if (auto *at = llvm::dyn_cast_or_null<AST::ArraryType>(astType)) {
    auto ty = lookupType(at->type->getKindName());
    return std::make_unique<ArrayType>(ty, at->size);
  } else {
    llvm_unreachable("");
  }
}

Type *Semantics::transType(AST::Type *astType) {
  if (auto *nt = llvm::dyn_cast_or_null<AST::NamedType>(astType)) {
    auto tname = nt->name;
    return lookupType(tname.name);
  } else if (auto *rt = llvm::dyn_cast_or_null<AST::RecordType>(astType)) {
    std::vector<RecordType::Field> fields;
    for (auto &field : rt->fields) {
      Symbol fieldName = field.name;
      auto ty = transType(field.type.get());
      fields.emplace_back(fieldName, ty);
    }
    auto ty = std::make_unique<RecordType>(std::move(fields));
    auto sym = Symbol::make();
    insertType(sym.name, std::move(ty));
    return lookupType(sym.name);

  } else if (auto *at = llvm::dyn_cast_or_null<AST::ArraryType>(astType)) {
    auto ty = transType(at->type.get());
    auto x = std::make_unique<ArrayType>(ty, at->size);
    auto sym = Symbol::make();
    insertType(sym.name, std::move(x));
    return lookupType(sym.name);
  }
  llvm_unreachable("");
}

} // namespace Sema

} // namespace KT
