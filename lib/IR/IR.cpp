//===-- IR.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/IR/IR.h"
#include <sstream>
#include <string>

namespace KT {

namespace IR {

std::map<BiOp, std::string> biOpNames{
    {BiOp::PLUS, "Plus"},       {BiOp::MINUS, "Minus"},
    {BiOp::MUL, "Mul"},         {BiOp::DIV, "Div"},
    {BiOp::AND, "And"},         {BiOp::OR, "Or"},
    {BiOp::LSHIFT, "LShift"},   {BiOp::RSHIFT, "RShift"},
    {BiOp::ARSHIFT, "ARShift"}, {BiOp::XOR, "Xor"},
};

std::map<RelOp, std::string> relOpNames{
    {RelOp::EQ, "Equal"},
    {RelOp::NE, "Not Equal"},
    {RelOp::LT, "Less Than"},
    {RelOp::GT, "Greater"},
    {RelOp::LE, "Less or Equal"},
    {RelOp::GE, "Greater or Equal"},
    {RelOp::ULT, "Unsigned Less Than"},
    {RelOp::ULE, "Unsigned Less or Equal"},
    {RelOp::UGT, "Unsigned Greater"},
    {RelOp::UGE, "Unsigned Greater or Equal"},
};

std::string ConstantExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<const>";
  ss << i;
  ss << "<const/>";
  return ss.str();
}

std::string NameExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<name>";
  ss << name.sym.prettyPrint();
  ss << "<name/>";
  return ss.str();
}

std::string TemporaryExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<temporary>";
  ss << temp.prettyPrint();
  ss << "<temporary/>";
  return ss.str();
}

std::string BinaryOperationExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<BinaryOperation>";
  ss << lhs->prettyPrint();
  ss << "<op>";
  ss << biOpNames[op];
  ss << "<op/>";
  ss << rhs->prettyPrint();
  ss << "<BinaryOperation/>";
  return ss.str();
}

std::string MemoryExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<Memory>";
  ss << exp->prettyPrint();
  ss << "<Memory/>";
  return ss.str();
}

std::string CallExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<call>";
  ss << "<function>";
  ss << func->prettyPrint();
  ss << "<function/>";
  ss << "<args>";
  for (auto &arg : arguments) {
    ss << arg->prettyPrint();
  }
  ss << "<args/>";
  ss << "<call/>";
  return ss.str();
}

std::string SequenceExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<SequenceExpr>";
  ss << stm->prettyPrint();
  ss << exp->prettyPrint();
  ss << "<SequenceExpr/>";
  return ss.str();
}

std::string MoveStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<move>";
  ss << dst->prettyPrint();
  ss << src->prettyPrint();
  ss << "<move/>";
  return ss.str();
}

std::string ExpressionStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<ExpressionStatement>";
  ss << exp->prettyPrint();
  ss << "<ExpressionStatement/>";
  return ss.str();
}

std::string JumpStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<jump>";
  ss << exp->prettyPrint();
  for (auto &l : possibleLocations) {
    ss << l.sym.prettyPrint();
  }
  ss << "<jump/>";
  return ss.str();
}

std::string CJumpStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<cjump>";
  ss << "<op>";
  ss << relOpNames[op];
  ss << "<op/>";
  ss << lhs->prettyPrint();
  ss << rhs->prettyPrint();
  ss << yes.sym.prettyPrint();
  ss << no.sym.prettyPrint();
  ss << "<cjump/>";
  return ss.str();
}

std::string SequenceStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<SequenceStm>";
  ss << head->prettyPrint();
  ss << tail->prettyPrint();
  ss << "<SequenceStm/>";
  return ss.str();
}

std::string LabelStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<Label>";
  ss << name.sym.prettyPrint();
  ss << "<Label/>";
  return ss.str();
}

} // namespace IR

} // namespace KT
