//===-- Canonical.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/IR/Canonical.h"
#include "kt/IR/IR.h"
#include "kt/Temporary/Temporary.h"
#include "llvm/Support/ErrorHandling.h"
#include <cstddef>
#include <functional>
#include <list>
#include <memory>
#include <utility>
#include <vector>

namespace KT {

namespace IR {

namespace {

StmPtr concat(StmPtr &&lhs, StmPtr &&rhs);
bool isCommute(StmPtr &lhs, ExprPtr &rhs);
StmPtr nop();
std::pair<StmPtr, std::list<ExprPtr>> reorder(std::list<ExprPtr> &&exps);

StmPtr reorderStatement(std::list<ExprPtr> &&l,
                        std::function<StmPtr(std::list<ExprPtr>) &&>);

std::pair<StmPtr, ExprPtr>
reorderExpression(std::list<ExprPtr> &&l,
                  std::function<ExprPtr(std::list<ExprPtr>) &&>);

StmPtr doStatement(StmPtr &&statement);

std::pair<StmPtr, ExprPtr> doExpression(ExprPtr &&exp);

std::list<StmPtr> linear(StmPtr &&s, std::list<StmPtr> &&l) {
  if (auto seq = llvm::dyn_cast_or_null<IR::SequenceStatement>(s.get())) {
    auto a = std::move(seq->head);
    auto b = std::move(seq->tail);
    auto c = linear(std::move(b), std::move(l));
    auto d = linear(std::move(a), std::move(c));
    return std::move(d);
  } else {
    l.push_front(std::move(s));
    return std::move(l);
  }
}

StmPtr concat(StmPtr &&lhs, StmPtr &&rhs) {
  if (auto expStm =
          llvm::dyn_cast_or_null<IR::ExpressionStatement>(lhs.get())) {
    if (auto constant =
            llvm::dyn_cast_or_null<IR::ConstantExpression>(expStm->exp.get())) {
      return std::move(rhs);
    }
  }

  if (auto expStm =
          llvm::dyn_cast_or_null<IR::ExpressionStatement>(rhs.get())) {
    if (auto constant =
            llvm::dyn_cast_or_null<IR::ConstantExpression>(expStm->exp.get())) {
      return std::move(lhs);
    }
  }
  return std::make_unique<IR::SequenceStatement>(std::move(lhs),
                                                 std::move(rhs));
}

bool isCommute(StmPtr &lhs, ExprPtr &rhs) {
  if (auto expStm =
          llvm::dyn_cast_or_null<IR::ExpressionStatement>(lhs.get())) {
    if (auto constant =
            llvm::dyn_cast_or_null<IR::ConstantExpression>(expStm->exp.get())) {
      return true;
    }
  }

  if (auto expStm = llvm::dyn_cast_or_null<IR::NameExpression>(rhs.get())) {
    return true;
  }

  if (auto expStm = llvm::dyn_cast_or_null<IR::ConstantExpression>(rhs.get())) {
    return true;
  }

  return false;
}

StmPtr nop() {
  return std::make_unique<IR::ExpressionStatement>(
      std::make_unique<IR::ConstantExpression>(0));
}

std::pair<StmPtr, std::list<ExprPtr>> reorder(std::list<ExprPtr> &&exps) {
  if (exps.empty()) {
    return {nop(), std::move(exps)};
  }
  auto head = std::move(exps.front());
  exps.pop_front();
  if (auto e = llvm::dyn_cast_or_null<IR::CallExpression>(head.get())) {
    auto t = Temporary();
    auto temp = std::make_unique<IR::TemporaryExpression>(t);
    auto move =
        std::make_unique<IR::MoveStatement>(std::move(temp), std::move(head));
    auto temp2 = std::make_unique<IR::TemporaryExpression>(t);
    auto eseq = std::make_unique<IR::SequenceExpression>(std::move(move),
                                                         std::move(temp2));
    exps.push_front(std::move(eseq));
    return reorder(std::move(exps));
  }
  auto stmse = doExpression(std::move(head));
  auto stmsel = reorder(std::move(exps));
  if (isCommute(stmsel.first, stmse.second)) {
    auto a = concat(std::move(stmse.first), std::move(stmsel.first));
    stmsel.second.push_front(std::move(stmse.second));
    return {std::move(a), std::move(stmsel.second)};
  } else {
    auto t = Temporary();
    auto temp = std::make_unique<IR::TemporaryExpression>(t);
    auto move = std::make_unique<IR::MoveStatement>(std::move(temp),
                                                    std::move(stmse.second));
    auto s1 = concat(std::move(stmse.first), std::move(move));
    auto s2 = concat(std::move(s1), std::move(stmsel.first));
    auto temp2 = std::make_unique<IR::TemporaryExpression>(t);
    stmsel.second.push_front(std::move(temp2));
    return {std::move(s2), std::move(stmsel.second)};
  }
}

// takes two arguments – a list l of subexpressions and a build function.
// It pulls all the ESEQs out of the l,
// yielding a statement s1 that contains all the statements from the ESEQs
// and a list l′ of cleaned-up expressions. Then it makes  SEQ(s1, build(l′)).
StmPtr reorderStatement(std::list<ExprPtr> &&l,
                        std::function<StmPtr(std::list<ExprPtr> &&)> build) {
  auto stmsel = reorder(std::move(l));
  auto built = build(std::move(stmsel.second));
  return concat(std::move(stmsel.first), std::move(built));
}

// it is similar to reorderStatement,
// except that it returns a pair (s, e)
// where s is a statement containing all the side effects pulled out of l,
// and e is build(l′).
std::pair<StmPtr, ExprPtr>
reorderExpression(std::list<ExprPtr> &&l,
                  std::function<ExprPtr(std::list<ExprPtr> &&)> build) {
  auto stmsel = reorder(std::move(l));
  auto built = build(std::move(stmsel.second));
  return {std::move(stmsel.first), std::move(built)};
}

StmPtr doStatement(StmPtr &&statement) {
  if (auto x = llvm::dyn_cast_or_null<IR::SequenceStatement>(statement.get())) {
    auto a = doStatement(std::move(x->head));
    auto b = doStatement(std::move(x->tail));
    auto c = concat(std::move(a), std::move(b));
    return std::move(c);
  } else if (auto x =
                 llvm::dyn_cast_or_null<IR::JumpStatement>(statement.get())) {
    std::list<ExprPtr> a;
    a.push_back(std::move(x->exp));

    return reorderStatement(std::move(a),
                            [b = std::move(x->possibleLocations)](
                                std::list<ExprPtr> &&es) -> StmPtr {
                              ExprPtr e = std::move(es.front());
                              // auto ret = std::make_unique<IR::JumpStatement>(
                              //     std::move(e), std::move(b));
                              // return ret;
                              llvm_unreachable("not implemented yet");
                            });
  } else if (auto x =
                 llvm::dyn_cast_or_null<IR::CJumpStatement>(statement.get())) {
    llvm_unreachable("not implemented yet");

  } else if (auto x =
                 llvm::dyn_cast_or_null<IR::MoveStatement>(statement.get())) {
    if (auto temp =
            llvm::dyn_cast_or_null<IR::TemporaryExpression>(x->dst.get())) {
      if (auto call =
              llvm::dyn_cast_or_null<IR::CallExpression>(x->src.get())) {
        auto t = temp->temp;
        auto f = std::move(call->func);
        auto args = std::move(call->arguments);

        std::list<ExprPtr> exps;
        exps.push_back(std::move(f));
        for (auto &ar : args) {
          exps.push_back(std::move(ar));
        }

        return reorderStatement(
            std::move(exps), [t](std::list<ExprPtr> &&exps) -> StmPtr {
              auto head = std::move(exps.front());
              exps.pop_front();
              auto temp = std::make_unique<TemporaryExpression>(t);
              std::vector<ExprPtr> args;
              for (auto &ar : exps) {
                args.push_back(std::move(ar));
              }
              auto call = std::make_unique<CallExpression>(std::move(head),
                                                           std::move(args));
              return std::make_unique<MoveStatement>(std::move(temp),
                                                     std::move(call));
            });
      }
    } else if (auto mem =
                   llvm::dyn_cast_or_null<IR::MemoryExpression>(x->dst.get())) {
      auto &e = mem->exp;
      auto &b = x->src;
      std::list<ExprPtr> es;
      es.push_back(std::move(e));
      es.push_back(std::move(b));
      return reorderStatement(
          std::move(es), [](std::list<ExprPtr> &&exps) -> StmPtr {
            auto head = std::move(exps.front());
            exps.pop_front();
            auto next = std::move(exps.front());
            exps.pop_front();
            auto mem = std::make_unique<MemoryExpression>(std::move(head));
            auto move = std::make_unique<MoveStatement>(std::move(mem),
                                                        std::move(next));
            return move;
          });
    }
  }
  llvm_unreachable("not implemented yet");
}

// To pull all the ESEQs from l,
// the functions reorder_stm and reorder_exp must call do_exp recursively.
std::pair<StmPtr, ExprPtr> doExpression(ExprPtr &&exp) {

  if (auto biop =
          llvm::dyn_cast_or_null<BinaryOperationExpression>(exp.get())) {
    auto &a = biop->lhs;
    auto &b = biop->rhs;
    auto p = biop->op;
    std::list<ExprPtr> es;
    es.push_back(std::move(a));
    es.push_back(std::move(b));
    return reorderExpression(
        std::move(es), [p](std::list<ExprPtr> &&exps) -> ExprPtr {
          auto a = std::move(exps.front());
          exps.pop_front();
          auto b = std::move(exps.front());
          exps.pop_front();
          return std::make_unique<BinaryOperationExpression>(p, std::move(a),
                                                             std::move(b));
        });
  } else if (auto mem = llvm::dyn_cast_or_null<MemoryExpression>(exp.get())) {
    auto &a = mem->exp;
    std::list<ExprPtr> es;
    es.push_back(std::move(a));
    return reorderExpression(
        std::move(es), [](std::list<ExprPtr> &&exps) -> ExprPtr {
          auto a = std::move(exps.front());
          exps.pop_front();
          return std::make_unique<MemoryExpression>(std::move(a));
        });
  } else if (auto eseq =
                 llvm::dyn_cast_or_null<SequenceExpression>(exp.get())) {
    auto &s = eseq->stm;
    auto &e = eseq->exp;
    auto stms = doStatement(std::move(s));
    auto stmse = doExpression(std::move(e));
    auto stms_ = std::move(stmse.first);
    auto e_ = std::move(stmse.second);
    auto stmss = concat(std::move(stms), std::move(stms_));
    return {std::move(stmss), std::move(e_)};
  } else if (auto call = llvm::dyn_cast_or_null<CallExpression>(exp.get())) {
    auto &e = call->func;
    auto &el = call->arguments;
    std::list<ExprPtr> es;
    es.push_back(std::move(e));
    for (auto &e_ : el) {
      es.push_back(std::move(e_));
    }
    return reorderExpression(std::move(es),
                             [](std::list<ExprPtr> &&exps) -> ExprPtr {
                               auto a = std::move(exps.front());
                               exps.pop_front();
                               std::vector<ExprPtr> args;
                               for (auto &e : exps) {
                                 args.push_back(std::move(e));
                               }
                               return std::make_unique<CallExpression>(
                                   std::move(a), std::move(args));
                             });
  } else {
    return {std::move(nop()), std::move(exp)};
  }

  llvm_unreachable("not implemented yet");
}

} // namespace

// 1. No SEQ and ESEQ.
// 2. The parent of each CALL is either EXP(...) or MOVE(TEMP t, ...).
std::list<StmPtr> linearize(StmPtr &&stm) {
  auto x = doStatement(std::move(stm));
  auto l = std::list<StmPtr>();
  auto stms = linear(std::move(x), std::move(l));
  return stms;
}

struct BasicBlockMaker {
  using BasicBlock = std::list<IR::StmPtr>;
  std::list<BasicBlock> bbs;
  BasicBlock currentBB;
  Label done;

  void next(std::list<IR::StmPtr> &&stms) {
    auto &head = stms.front();
    if (auto l = llvm::dyn_cast_or_null<IR::JumpStatement>(head.get())) {
      currentBB.push_front(std::move(head));
      stms.pop_front();
      endBlock(std::move(stms));
      return;
    }

    if (auto l = llvm::dyn_cast_or_null<IR::CJumpStatement>(head.get())) {
      currentBB.push_front(std::move(head));
      stms.pop_front();
      endBlock(std::move(stms));

      return;
    }

    if (auto l = llvm::dyn_cast_or_null<IR::LabelStatement>(head.get())) {
      std::list<IR::StmPtr> lst;
      Label lab = l->name;
      auto name = std::make_unique<IR::NameExpression>(lab);
      std::vector<Label> labels{lab};
      auto jump = std::make_unique<IR::JumpStatement>(std::move(name),
                                                      std::move(labels));
      stms.push_front(std::move(jump));
      next(std::move(stms));
      return;
    }

    if (stms.empty()) {
      std::list<IR::StmPtr> lst;
      auto name = std::make_unique<IR::NameExpression>(done);
      std::vector<Label> labels{done};
      auto jump = std::make_unique<IR::JumpStatement>(std::move(name),
                                                      std::move(labels));
      lst.push_back(std::move(jump));
      next(std::move(lst));
      return;
    } else {
      currentBB.push_front(std::move(head));
      stms.pop_front();
      next(std::move(stms));
    }
  }

  void blocks(std::list<IR::StmPtr> &&stms) {
    auto &head = stms.front();
    if (auto l = llvm::dyn_cast_or_null<IR::LabelStatement>(head.get())) {
      currentBB.push_back(std::move(head));
      stms.pop_front();
      next(std::move(stms));
      return;
    }
    if (stms.empty()) {
      bbs.reverse();
      return;
    }
    auto l = std::make_unique<IR::LabelStatement>(Label());
    stms.push_front(std::move(l));
    blocks(std::move(stms));
    return;
  }

  void endBlock(std::list<IR::StmPtr> &&stms) {
    currentBB.reverse();
    bbs.push_back(std::move(currentBB));
    currentBB.clear();
    blocks(std::move(stms));
  }

  std::list<BasicBlock> toBasicBlock(std::list<StmPtr> &&ss) {
    bbs.clear();
    currentBB.clear();
    blocks(std::move(ss));
    return std::move(bbs);
  }
};

std::list<BasicBlock> toBasicBlocks(std::list<StmPtr> &&statements) {
  BasicBlockMaker bbm;
  return bbm.toBasicBlock(std::move(statements));
}

template <typename T> std::pair<std::list<T>, T> splitLast(std::list<T> &&ts) {
  if (ts.empty()) {
    llvm_unreachable("cannot be empty");
  }
  if (ts.size() == 1) {
    std::list<T> a;
    return {std::move(a), std::move(ts.front())};
  }
  auto head = std::move(ts.front());
  ts.pop_front();
  auto next = splitLast(std::move(ts));
  next.first.push_front(std::move(head));
  return {std::move(next.first), std::move(next.second)};
}

struct TraceScheduler {
  std::map<Label, std::list<IR::StmPtr>> mapping;

  void enterBlock(std::list<IR::StmPtr> &&stms) {
    if (auto l =
            llvm::dyn_cast_or_null<IR::LabelStatement>(stms.front().get())) {
      mapping[l->name] = std::move(stms);
    }
  }

  std::list<IR::StmPtr> trace(std::list<IR::StmPtr> &&stms,
                              std::list<BasicBlock> &&rest) {
    if (auto lab =
            llvm::dyn_cast_or_null<IR::LabelStatement>(stms.front().get())) {
      // mapping[lab->name] = {};
      auto res = splitLast(std::move(stms));
      auto most = std::move(res.first);
      auto last = std::move(res.second);
      auto &b = stms;

      if (auto x = llvm::dyn_cast_or_null<IR::JumpStatement>(last.get())) {
        if (auto y = llvm::dyn_cast_or_null<IR::NameExpression>(x->exp.get())) {
          auto found = mapping.find(y->name);
          if (found != mapping.end()) {
            std::list<IR::StmPtr> ret;
            for (auto &s : most) {
              ret.push_back(std::move(s));
            }
            auto &z = mapping[y->name];
            auto w = trace(std::move(z), std::move(rest));
            for (auto &s : w) {
              ret.push_back(std::move(s));
            }
            return std::move(ret);

          } else {
            std::list<IR::StmPtr> ret;
            for (auto &s : most) {
              ret.push_back(std::move(s));
            }
            auto z = getNext(std::move(rest));
            for (auto &s : z) {
              ret.push_back(std::move(s));
            }
            return std::move(ret);
          }
        }
      }

      if (auto x = llvm::dyn_cast_or_null<IR::CJumpStatement>(last.get())) {
        auto foundT = mapping.find(x->yes);
        auto foundF = mapping.find(x->no);
        if (foundF != mapping.end()) {
          std::list<IR::StmPtr> ret;
          for (auto &s : most) {
            ret.push_back(std::move(s));
          }
          auto &z = mapping[x->no];
          auto w = trace(std::move(z), std::move(rest));
          for (auto &s : w) {
            ret.push_back(std::move(s));
          }
          return std::move(ret);

        } else if (foundT != mapping.end()) {

          std::list<IR::StmPtr> ret;
          for (auto &s : most) {
            ret.push_back(std::move(s));
          }
          auto cjump = std::make_unique<IR::CJumpStatement>(
              IR::notRel(x->op), std::move(x->lhs), std::move(x->rhs), x->yes,
              x->no);
          ret.push_back(std::move(cjump));
          auto &z = mapping[x->yes];
          auto w = trace(std::move(z), std::move(rest));
          for (auto &s : w) {
            ret.push_back(std::move(s));
          }
          return std::move(ret);

        } else {
          auto f = Label();
          std::list<IR::StmPtr> ret;
          for (auto &s : most) {
            ret.push_back(std::move(s));
          }
          auto cjump = std::make_unique<IR::CJumpStatement>(
              x->op, std::move(x->lhs), std::move(x->rhs), x->yes, f);
          ret.push_back(std::move(cjump));
          ret.push_back(std::make_unique<IR::LabelStatement>(f));
          std::vector<Label> labs;
          labs.push_back(f);
          ret.push_back(std::make_unique<IR::JumpStatement>(
              std::make_unique<NameExpression>(f), std::move(labs)));
          auto z = getNext(std::move(rest));
          for (auto &s : z) {
            ret.push_back(std::move(s));
          }
          return std::move(ret);
        }
      }

      if (auto x = llvm::dyn_cast_or_null<IR::JumpStatement>(last.get())) {
        std::list<IR::StmPtr> ret;
        for (auto &s : b) {
          ret.push_back(std::move(s));
        }
        auto z = getNext(std::move(rest));
        for (auto &s : z) {
          ret.push_back(std::move(s));
        }
        return std::move(ret);
      }
    }

    llvm_unreachable("");
  }

  std::list<IR::StmPtr> getNext(std::list<BasicBlock> bbs) {
    if (bbs.empty())
      return {};
    if (auto l = llvm::dyn_cast_or_null<IR::LabelStatement>(
            bbs.front().front().get())) {
      auto found = mapping.find(l->name);
      if (found != mapping.end()) {
        auto b = std::move(bbs.front());
        bbs.pop_front();
        return trace(std::move(b), std::move(bbs));
      } else {
        bbs.pop_front();
        return getNext(std::move(bbs));
      }
    }
    return {};
    llvm_unreachable("");
  }

  std::list<IR::StmPtr> traceScheduling(std::list<BasicBlock> &&basicBlocks,
                                        Label done) {
    auto lab = std::make_unique<IR::LabelStatement>(done);

    // fixme:: basicblocks use twice;
    for (auto &bb : basicBlocks) {
      enterBlock(std::move(bb));
    }
    auto ret = getNext(std::move(basicBlocks));

    ret.push_back(std::move(lab));
    return ret;
  }
};

std::list<StmPtr> traceScheduling(std::list<BasicBlock> &&basicBlocks) {
  TraceScheduler ts;
  return ts.traceScheduling(std::move(basicBlocks), Label());
}

} // namespace IR

} // namespace KT
