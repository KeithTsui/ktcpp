//===-- InterferenceGraph.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/Analystics/InterferenceGraph.h"
#include "kt/Analystics/CFG.h"
#include "kt/Analystics/Liveness.h"
#include "kt/Temporary/Temporary.h"
#include <list>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <utility>

namespace KT {

namespace Analystics {

// std::string IGNode::prettyPrint() {
//   std::stringstream ss;
//   ss << "{"
//      << "temp: " << temp.prettyPrint() << ";"
//      << "status: " << status << ";"
//      << "degree: " << degree << ";"
//      << "adjacents: [";
//   for (auto adj : adjacent) {
//     ss << adj->temp.prettyPrint() << ",";
//   }
//   ss << "]"
//      << "}";
//   return ss.str();
// }

} // namespace Analystics

} // namespace KT
