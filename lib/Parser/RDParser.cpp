//===-- RDParser.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#include "kt/Parser/RDParser.h"
#include "kt/AST/AST.h"
#include "kt/Support/Utilities.h"
#include "kt/Token/Token.h"
#include "kt/Token/TokenType.h"
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <vector>

#include "kt/SymbolTable/Symbol.h"
#include "llvm/Support/ErrorHandling.h"

namespace KT {

template <typename T> std::vector<T> toVector(std::list<T> &&ts) {
  std::vector<T> ret;
  for (auto &t : ts) {
    ret.push_back(std::move(t));
  }
  return ret;
}

/// enable explicit constructor
RDParser::RDParser(std::string const &fileName)
    : lexer{fileName}, filename{fileName}, numErrors{0} {}

// tokenize the input program
std::vector<Token> RDParser::tokenize() {
  reset();
  std::vector<Token> ts;
  while (true) {
    auto t = lexer.getToken();
    ts.push_back(t);
    if (t.getType() == Term::EOFF)
      break;
  }
  return ts;
}

void RDParser::reset() {
  numErrors = 0;
  lexer.reset();
}

/// parse scanner's tokens
std::unique_ptr<AST::TigerProgram> RDParser::parse() {
  reset();
  std::cout << "\n\n# [ RUN ] parsing code... \n\n";
  return parseTigerProgram();
}

/**
   <tiger-program> -> <let-statement>
 */
std::unique_ptr<AST::TigerProgram> RDParser::parseTigerProgram() {
  return parseLetStatement();
}

/**
   <let-statement> -> let <declaration-list> in <statement-list> end ;
 */
std::unique_ptr<AST::TigerProgram> RDParser::parseLetStatement() {
  auto let = lexer.getToken();
  assert(let.getType() == TokenType::LET && "expecting let");
  auto declarations = parseDeclarationList();
  auto in = lexer.getToken();
  assert(in.getType() == TokenType::Terminal::IN && "expecting in");
  auto statements = parseStatementList();
  auto end = lexer.getToken();
  assert(end.getType() == TokenType::Terminal::END && "expecting end");
  auto semicolon = lexer.getToken();
  assert(semicolon.getType() == TokenType::Terminal::SEMI && "expecting ;");
  auto ret = std::make_unique<AST::TigerProgram>(
      toVector(std::move(declarations)), std::move(statements));
  ret->line = let.line;
  ret->column = let.column;
  return ret;
}

/**
   <declaration-list> -> <declaration> <declaration-list>
                       | NULL
 */
std::list<std::unique_ptr<AST::Declaration>> RDParser::parseDeclarationList() {

  auto peek = peek1();
  if (peek.getType() == TokenType::Terminal::IN) {
    return {};
  }
  auto declaration = parseDeclaration();
  peek = peek1();
  if (peek.getType() == TokenType::Terminal::IN) {
    std::list<std::unique_ptr<AST::Declaration>> ret;
    ret.push_back(std::move(declaration));
    return ret;
  } else {
    auto decls = parseDeclarationList();
    decls.push_front(std::move(declaration));
    return decls;
  }
}

/**
   <declaration> -> <type-declaration>
                  | <var-declaration>
                  | <function-declaration>
 */
std::unique_ptr<AST::Declaration> RDParser::parseDeclaration() {
  auto peek = peek1();
  switch (peek.getType().getValue()) {
  case TokenType::Terminal::TYPE:
    return parseTypeDeclaration();
  case TokenType::Terminal::VAR:
    return parseVaraibleDeclaration();
  case TokenType::Terminal::FUNCTION:
    return parseFunctionDeclaration();
  default:
    std::string msg{"In parsing Declaration, expecting type, var or function, "
                    "but get "};
    msg += peek.getTokenString();
    msg += "\n";
    llvm_unreachable(msg.c_str());
  }
}

/**
   <type-declaration> -> type id = <type> ;
 */
std::unique_ptr<AST::TypeDeclaration> RDParser::parseTypeDeclaration() {
  auto type = lexer.getToken();
  assert(type.getType() == TokenType::Terminal::TYPE && "expecting type");
  auto id = lexer.getToken();
  assert(id.getType() == TokenType::Terminal::ID && "expecting ID");
  auto assign = lexer.getToken();
  assert(assign.getType() == TokenType::Terminal::EQ && "expecting Assign");
  auto t = parseType();
  auto s = Symbol::make(id.getTokenString());
  auto semicolon = lexer.getToken();
  assert(semicolon.getType() == Term::SEMI && "expecting semicomma");
  auto ret = std::make_unique<AST::TypeDeclaration>(s, std::move(t));
  ret->line = type.line;
  ret->column = type.column;
  return ret;
}

/**
   <type> -> <named-type>
           | <array-type>
           | <record-type>
 */
std::unique_ptr<AST::Type> RDParser::parseType() {
  auto peek = peek1();
  switch (peek.getType().getValue()) {
  case TokenType::Terminal::INT:    // int
  case TokenType::Terminal::FLOAT:  // float
  case TokenType::Terminal::STRING: // string
  case TokenType::Terminal::ID:     // User-defined type
    return parseNamedType();
  case TokenType::Terminal::ARRAY: // array
    return parseArrayType();
  case TokenType::Terminal::LBRACE: // record
    return parseRecordType();
  default:
    std::string msg{
        "In parsing Declaration, expecting int, float, array or {, but get "};
    msg += peek.getTokenString();
    msg += "\n";
    llvm_unreachable(msg.c_str());
  }
}

/**
   <named-type> -> int | float | string | id
 */
std::unique_ptr<AST::NamedType> RDParser::parseNamedType() {
  auto t = lexer.getToken();
  assert((isIn(t.getType(), Term::ID, Term::INT, Term::FLOAT, Term::STRING)) &&
         "expecting ID, int, float, or string");
  auto ret = std::make_unique<AST::NamedType>(Symbol::make(t.getTokenString()));
  ret->line = t.line;
  ret->column = t.column;
  return ret;
}

/**
   <arrary-type> -> array [ IntLiteral ] of <type> # array type
 */
std::unique_ptr<AST::ArraryType> RDParser::parseArrayType() {
  auto array = lexer.getToken();
  assert(array.getType() == TokenType::Terminal::ARRAY && "expecting Arrary");
  auto lsb = lexer.getToken();
  assert(lsb.getType() == TokenType::Terminal::LBRACK && "expecting [");
  auto intLiteral = parseIntExpression();
  assert(intLiteral && "expecting int literals");
  auto rsb = lexer.getToken();
  assert(rsb.getType() == TokenType::Terminal::RBRACK && "expecting ]");
  auto of = lexer.getToken();
  assert((of.getType() == TokenType::Terminal::OF) && "expecting of");
  auto type = parseType();
  assert(type && "expecting a named type");
  auto ret =
      std::make_unique<AST::ArraryType>(std::move(type), intLiteral->value);
  ret->line = array.line;
  ret->line = array.column;
  return ret;
}

/**
   <record-type> -> {<name-type-pair-list>} # record type
 */
std::unique_ptr<AST::RecordType> RDParser::parseRecordType() {
  auto lb = lexer.getToken();
  assert(lb.getType() == TokenType::Terminal::LBRACE && "expecting {");
  auto nameTypePairs = parseNameTypePairList();
  auto rb = lexer.getToken();
  assert(rb.getType() == TokenType::Terminal::RBRACE && "expecting }");

  std::vector<AST::RecordField> fields;
  for (auto &p : nameTypePairs) {
    fields.emplace_back(p.name, std::move(p.type));
  }
  auto ret = std::make_unique<AST::RecordType>(fields);
  ret->line = lb.line;
  ret->line = lb.column;
  return ret;
}

/**
   <var-declaration> -> var id : <type> <optional-initializer> ;
 */
std::unique_ptr<AST::VariableDeclaration> RDParser::parseVaraibleDeclaration() {
  auto var = lexer.getToken();
  assert(var.getType() == TokenType::Terminal::VAR && "expecting var");
  auto id = lexer.getToken();
  assert(id.getType() == TokenType::Terminal::ID && "expecting id");
  auto colon = lexer.getToken();
  assert(colon.getType() == TokenType::Terminal::COLON && "expecting :");
  auto type = parseType();
  assert(type && "expecting a type");
  auto init = parseOptionalInitializer();
  auto comma = lexer.getToken();
  assert(comma.getType() == Term::SEMI && "expecting semicomma");
  auto ret = std::make_unique<AST::VariableDeclaration>(
      Symbol::make(id.getTokenString()), false, std::move(type),
      std::move(init));
  ret->line = var.line;
  ret->column = var.column;
  return ret;
}

/**
<optional-initializer> -> := <expression>
                        | NULL
 */
std::unique_ptr<AST::Expression> RDParser::parseOptionalInitializer() {
  auto peek = peek1();
  if (!(peek.getType() == TokenType::Terminal::ASSIGN)) {
    return nullptr;
  } else {
    auto assign = lexer.getToken();
    assert(assign.getType() == TokenType::Terminal::ASSIGN && "expecting :=");
    auto exp = parseExpression();
    assert(exp && "expecting an expression");
    return exp;
  }
}

/**
   <function-declaration> -> function id ( <name-type-pair-list> ) : <type>
                             begin <statement-list> end ;
 */
std::unique_ptr<AST::FunctionDeclaration> RDParser::parseFunctionDeclaration() {
  auto f = lexer.getToken();
  assert(f.getType() == TokenType::Terminal::FUNCTION && "expecting function");
  auto id = lexer.getToken();
  assert(id.getType() == TokenType::Terminal::ID && "expecting id");
  auto lp = lexer.getToken();
  assert(lp.getType() == TokenType::Terminal::LPAREN && "expecting (");
  auto parameters = parseNameTypePairList();
  auto rp = lexer.getToken();
  assert(rp.getType() == TokenType::Terminal::RPAREN && "expecting )");

  std::unique_ptr<AST::Type> retType;
  auto peek = peek1();
  if (peek.getType() == Term::COLON) { // has return type
    retType = parseReturnType();
  }

  auto begin = lexer.getToken();
  assert(begin.getType() == TokenType::Terminal::BEGIN && "expecting begin");
  auto statements = parseStatementList();
  auto end = lexer.getToken();
  assert(end.getType() == TokenType::Terminal::END && "expecting end");
  auto semicolon = lexer.getToken();
  assert(semicolon.getType() == Term::SEMI && "expecting semicomma");
  auto ret = std::make_unique<AST::FunctionDeclaration>(
      Symbol::make(id.getTokenString()), std::move(parameters),
      std::move(retType), std::move(statements));

  ret->line = f.line;
  ret->column = f.column;
  return ret;
}

/**
   <name-type-pair-list> -> <name-type-pair> <name-type-pair-tail>
                          | NULL
 */
std::vector<AST::Parameter> RDParser::parseNameTypePairList() {
  std::vector<AST::Parameter> ret;
  auto peek = peek1();
  if (!isIn(peek.getType(), Term::RPAREN, Term::RBRACE)) {
    auto parameter = parseParameter();
    assert(parameter && "expecting a parameter");
    ret.push_back(std::move(*parameter.release()));
    auto rest = parseParameterTail();
    for (auto &para : rest) {
      ret.push_back(std::move(para));
    }
  }
  return ret;
}

/**
   <name-type-pair-tail> -> , <name-type-pair> <name-type-pair-tail>
                          | NULL
 */
std::vector<AST::Parameter> RDParser::parseParameterTail() {
  std::vector<AST::Parameter> ret;
  auto peek = peek1();
  if (!isIn(peek.getType(), Term::RPAREN, Term::RBRACE)) {
    auto comma = lexer.getToken();
    assert(comma.getType() == TokenType::Terminal::COMMA && "expecting comma");
    auto para = parseParameter();
    assert(para && "expecting a parameter");
    ret.push_back(std::move(*para.release()));
    auto rest = parseParameterTail();
    for (auto &pa : rest) {
      ret.push_back(std::move(pa));
    }
  }
  return ret;
}

// 26 : <ret-type> -> : <type>
std::unique_ptr<AST::Type> RDParser::parseReturnType() {
  auto colon = lexer.getToken();
  assert(colon.getType() == Term::COLON && "expecting :");
  return parseType();
}

/**
   <name-type-pair> -> id : <type>
 */
std::unique_ptr<AST::Parameter> RDParser::parseParameter() {
  auto id = lexer.getToken();
  assert(id.getType() == TokenType::Terminal::ID && "expecting ID");
  auto colon = lexer.getToken();
  assert(colon.getType() == TokenType::Terminal::COLON && "expecting colon");
  auto type = parseType();
  assert(type && "expecting a type");
  auto ret = std::make_unique<AST::Parameter>(Symbol::make(id.getTokenString()),
                                              std::move(type));
  return ret;
}

/**
   <statement-list> -> <statement> <statement-list>
                     | NULL
 */
std::vector<std::unique_ptr<AST::Statement>> RDParser::parseStatementList() {
  std::vector<std::unique_ptr<AST::Statement>> ret;
  auto peek = peek1();
  if (!isIn(peek.getType(), Term::END, Term::ENDDO, Term::ENDIF, Term::ELSE)) {
    ret.push_back(parseStatement());
    auto rest = parseStatementList();
    for (auto &e : rest) {
      ret.push_back(std::move(e));
    }
  }
  return ret;
}

/**
   <statement> -> <if-statement>
               -> <while-statement>
               -> <for-statement>
               -> <break-statement>
               -> <let-statement>
               -> <call-or-assignment-statement>
 */
std::unique_ptr<AST::Statement> RDParser::parseStatement() {
  auto fs = peek2();
  auto f = fs.first;
  auto s = fs.second;

  switch (f.getType().getValue()) {
  case TokenType::Terminal::IF:
    return parseIfStatement();
  case TokenType::Terminal::WHILE:
    return parseWhileStatement();
  case TokenType::Terminal::FOR:
    return parseForStatement();
  case TokenType::Terminal::BREAK:
    return parseBreakStatement();
  case TokenType::Terminal::LET:
    return parseLetStatement();
  case TokenType::Terminal::ID:
    return parseCallOrAssignStatement();
  case Term::RETURN:
    return parseReturnStatement();
  }
  assert(false && "cannot reach here");
}

/**
   <if-statement> -> if <expression> then <statement-list> <if-statement-tail>
   <if-statement-tail> -> else <statement-list> endif ;
                        | endif ;
 */
std::unique_ptr<AST::IfStatement> RDParser::parseIfStatement() {
  auto _if = lexer.getToken();
  assert(_if.getType() == Term::IF && "expecting if");

  auto expr = parseExpression();
  assert(expr && "expecting an expression");

  auto then = lexer.getToken();
  assert(then.getType() == Term::THEN && "expecting then");

  auto statements = parseStatementList();
  std::vector<std::unique_ptr<AST::Statement>> elseStatements;

  auto next = lexer.getToken();
  if (next.getType() == Term::ELSE) {

    elseStatements = parseStatementList();
    auto endif = lexer.getToken();
    assert(endif.getType() == Term::ENDIF && "expecting endif");
  } else if (next.getType() == Term::ENDIF) {
  } else {
    assert(false && "cannot reach here");
  }
  auto semi = lexer.getToken();
  assert(semi.getType() == Term::SEMI && "expecting semicomma");
  auto ret = std::make_unique<AST::IfStatement>(
      std::move(expr), std::move(statements), std::move(elseStatements));
  ret->line = _if.line;
  ret->column = _if.column;
  return ret;
}

/**
   <while-statement> -> while <expression>
                        do <statement-list> enddo ;
 */
std::unique_ptr<AST::WhileStatement> RDParser::parseWhileStatement() {
  auto _while = lexer.getToken();
  assert(_while.getType() == Term::WHILE && "expecting while");
  auto expr = parseExpression();
  assert(expr && "expecting an expression");
  auto _do = lexer.getToken();
  assert(_do.getType() == Term::DO && "expecting do");
  auto statements = parseStatementList();
  auto enddo = lexer.getToken();
  assert(enddo.getType() == Term::ENDDO && "expecting enddo");
  auto semi = lexer.getToken();
  assert(semi.getType() == Term::SEMI && "expecting semicomma");
  auto ret = std::make_unique<AST::WhileStatement>(std::move(expr),
                                                   std::move(statements));
  ret->line = _while.line;
  ret->column = _while.column;
  return ret;
}

/**
   <for-statement> -> for id := <expression> to <expression>
                      do <statement-list> enddo;
 */
std::unique_ptr<AST::ForStatement> RDParser::parseForStatement() {
  auto _for = lexer.getToken();
  assert(_for.getType() == Term::FOR && "expecting for");
  auto id = lexer.getToken();
  assert(id.getType() == Term::ID && "expecting an id");
  auto assign = lexer.getToken();
  assert(assign.getType() == Term::ASSIGN && "expecting assignment");
  auto expr1 = parseExpression();
  assert(expr1 && "expecting an expression");
  auto to = lexer.getToken();
  assert(to.getType() == Term::TO && "expecting assignment");
  auto expr2 = parseExpression();
  assert(expr2 && "expecting an expression");
  auto _do = lexer.getToken();
  assert(_do.getType() == Term::DO && "expecting do");
  auto statements = parseStatementList();
  auto enddo = lexer.getToken();
  assert(enddo.getType() == Term::ENDDO && "expecting enddo");
  auto semi = lexer.getToken();
  assert(semi.getType() == Term::SEMI && "expecting semicomma");
  auto ret = std::make_unique<AST::ForStatement>(
      Symbol::make(id.getTokenString()), std::move(expr1), std::move(expr2),
      std::move(statements));

  ret->line = _for.line;
  ret->column = _for.column;
  return ret;
}

/**
   <break-statement> -> break ;
 */
std::unique_ptr<AST::BreakStatement> RDParser::parseBreakStatement() {
  auto br = lexer.getToken();
  assert(br.getType() == Term::BREAK && "expecting break");
  auto semi = lexer.getToken();
  assert(semi.getType() == Term::SEMI && "expecting semicomma");
  auto ret = std::make_unique<AST::BreakStatement>();
  ret->line = br.line;
  ret->column = br.column;
  return ret;
}

/**
   <call-or-assignment-statement> -> <call-expression> ;
                                   | <lvalue-expression> := <expression>
 */
std::unique_ptr<AST::Statement> RDParser::parseCallOrAssignStatement() {
  auto id = lexer.getToken();
  assert(id.getType() == Term::ID && "expecting ID");
  auto peek = peek1();
  if (peek.getType() == Term::LPAREN) { // call expression
    lexer.putToken(id);
    auto call = parseCallExpression();
    auto semi = lexer.getToken();
    assert(semi.getType() == Term::SEMI && "expecting semicomma");
    return call;
  } else { // assignment statement lvalue := expr;
    lexer.putToken(id);
    auto lvalue = parseLvalue();
    auto assign = lexer.getToken();
    assert(assign.getType() == Term::ASSIGN && "expecting assignment");
    auto expr = parseExpression();
    assert(expr && "expecting an expression");
    auto semi = lexer.getToken();
    assert(semi.getType() == Term::SEMI && "expecting semicomma");
    auto ret = std::make_unique<AST::AssignmentStatement>(std::move(lvalue),
                                                          std::move(expr));
    ret->line = id.line;
    ret->column = id.column;
    return ret;
  }
}

/**
     <return-statement> -> return;
                         | return <expression>;
   */
std::unique_ptr<AST::Statement> RDParser::parseReturnStatement() {
  auto KW_Ret = lexer.getToken();
  assert(KW_Ret.getType() == Term::RETURN && "expecting return");

  std::unique_ptr<AST::Expression> expr;
  auto peek = peek1();
  if (peek.getType() != Term::SEMI) {
    expr = parseExpression();
  }
  auto semi = lexer.getToken();
  assert(semi.getType() == Term::SEMI && "expecting semicomma");
  auto ret = std::make_unique<AST::ReturnStatement>(std::move(expr));
  ret->line = KW_Ret.line;
  ret->column = KW_Ret.column;
  return ret;
}

/**
   <call-statement> -> ID ( <expression-list> ) ;
*/
std::unique_ptr<AST::CallExpression> RDParser::parseCallExpression() {
  auto id = lexer.getToken();
  assert(id.getType() == Term::ID && "expecting ID");
  auto lp = lexer.getToken();
  assert(lp.getType() == Term::LPAREN && "expecting (");
  auto exprs = parseExpressions();
  auto rp = lexer.getToken();
  assert(rp.getType() == Term::RPAREN && "expecting )");
  // auto semi = lexer.getToken();
  // assert(semi.getType() == Term::SEMI && "expecting semicomma");
  auto ret = std::make_unique<AST::CallExpression>(
      Symbol::make(id.getTokenString()), std::move(exprs));
  ret->line = id.line;
  ret->column = id.column;
  return ret;
}

/**
   <expression-list> -> <expression> <expression-list-tail>
                      | NULL
   <expression-list-tail> -> , <expression> <expression-list-tail>
                           | NULL
 */
std::vector<std::unique_ptr<AST::Expression>> RDParser::parseExpressions() {
  std::vector<std::unique_ptr<AST::Expression>> ret;
  auto peek = peek1();
  if (!isIn(peek.getType(), Term::RPAREN, Term::SEMI)) {
    auto expr = parseExpression();
    assert(expr && "expecting a expression");
    ret.push_back(std::move(expr));
    auto rest = parseExpressionTail();
    for (auto &para : rest) {
      ret.push_back(std::move(para));
    }
  }
  return ret;
}

/**
   <expression-list-tail> -> , <expression> <expression-list-tail>
                           | NULL
 */
std::vector<std::unique_ptr<AST::Expression>> RDParser::parseExpressionTail() {
  std::vector<std::unique_ptr<AST::Expression>> ret;
  auto peek = peek1();
  if (!isIn(peek.getType(), Term::RPAREN, Term::SEMI)) {
    auto comma = lexer.getToken();
    assert(comma.getType() == TokenType::Terminal::COMMA && "expecting comma");
    auto para = parseExpression();
    assert(para && "expecting a parameter");
    ret.push_back(std::move(para));
    auto rest = parseExpressionTail();
    for (auto &e : rest) {
      ret.push_back(std::move(e));
    }
  }
  return ret;
}

/**
   <expression> -> <or-expression>
 */
std::unique_ptr<AST::Expression> RDParser::parseExpression() {
  return parseOrExpression();
}

/**
   <or-expression> -> <and-expression> <or-op> <and-expression>
 */
std::unique_ptr<AST::Expression> RDParser::parseOrExpression() {
  auto expr1 = parseAndExpression();
  auto peek = peek1();
  if (peek.getType() != Term::OR) {
    return expr1;
  }
  auto op = lexer.getToken();
  assert(op.getType() == Term::OR && "expecting or");
  auto expr2 = parseAndExpression();
  auto ret = std::make_unique<AST::BinaryOperatorExpression>(
      std::move(expr1), std::move(expr2), AST::BinaryOperator::Or);
  ret->line = op.line;
  ret->column = op.column;
  return ret;
}

/**
   <and-expression> -> <compare-expression> <and-op> <compare-expression>
 */
std::unique_ptr<AST::Expression> RDParser::parseAndExpression() {
  auto expr1 = parseCmpExpression();
  auto peek = peek1();
  if (peek.getType() != Term::AND) {
    return expr1;
  }
  auto op = lexer.getToken();
  assert(op.getType() == Term::AND && "expecting and");
  auto expr2 = parseCmpExpression();
  auto ret = std::make_unique<AST::BinaryOperatorExpression>(
      std::move(expr1), std::move(expr2), AST::BinaryOperator::And);
  ret->line = op.line;
  ret->column = op.column;
  return ret;
}

/**
   <compare-expression> -> <term-expression>
                         | <term-expression> <cmp-op> <term-expression>
 */
std::unique_ptr<AST::Expression> RDParser::parseCmpExpression() {
  auto expr1 = parseTermExpression();
  auto peek = peek1();
  if (!isIn(peek.getType(), Term::EQ, Term::NEQ, Term::LESSEREQ, Term::LESSER,
            Term::GREATEREQ, Term::GREATER)) {
    return expr1;
  }
  auto op = lexer.getToken();
  // EQ, NEQ, LESSER, GREATER, LESSEREQ, GREATEREQ,
  assert(isIn(op.getType(), Term::EQ, Term::NEQ, Term::LESSEREQ, Term::LESSER,
              Term::GREATEREQ, Term::GREATER) &&
         "");
  auto expr2 = parseTermExpression();

  AST::BinaryOperator opr;
  switch (op.getType().getValue()) {
  case Term::EQ:
    opr = AST::BinaryOperator::Equal;
    break;
  case Term::NEQ:
    opr = AST::BinaryOperator::NotEqual;
    break;
  case Term::LESSEREQ:
    opr = AST::BinaryOperator::LessOrEqual;
    break;
  case Term::LESSER:
    opr = AST::BinaryOperator::Less;
    break;
  case Term::GREATEREQ:
    opr = AST::BinaryOperator::GreaterOrEqual;
    break;
  case Term::GREATER:
    opr = AST::BinaryOperator::Greater;
    break;
  }

  auto ret = std::make_unique<AST::BinaryOperatorExpression>(
      std::move(expr1), std::move(expr2), opr);
  ret->line = op.line;
  ret->column = op.column;
  return ret;
}

/**
   <term-expression> -> <factor-expression>
                      | <factor-expression> <term-op> <factor-expression>
 */
std::unique_ptr<AST::Expression> RDParser::parseTermExpression() {
  auto expr1 = parseFactorExpression();
  auto peek = peek1();
  if (!isIn(peek.getType(), Term::PLUS, Term::MINUS)) {
    return expr1;
  }
  auto op = lexer.getToken();
  assert(isIn(op.getType(), Term::PLUS, Term::MINUS) && "");
  auto expr2 = parseFactorExpression();

  AST::BinaryOperator opr;
  switch (op.getType().getValue()) {
  case Term::PLUS:
    opr = AST::BinaryOperator::Plus;
    break;
  case Term::MINUS:
    opr = AST::BinaryOperator::Minus;
    break;
  }

  auto ret = std::make_unique<AST::BinaryOperatorExpression>(
      std::move(expr1), std::move(expr2), opr);
  ret->line = op.line;
  ret->column = op.column;
  return ret;
}

/**
   <factor-expression> -> <atom-expression>
                        | <atom-expression> <factor-op> <atom-expression>
 */
std::unique_ptr<AST::Expression> RDParser::parseFactorExpression() {

  auto expr1 = parseAtomExpression();
  auto peek = peek1();
  if (!isIn(peek.getType(), Term::MULT, Term::DIV)) {
    return expr1;
  }
  auto op = lexer.getToken();
  assert(isIn(op.getType(), Term::MULT, Term::DIV) && "");
  auto expr2 = parseAtomExpression();

  AST::BinaryOperator opr;
  switch (op.getType().getValue()) {
  case Term::MULT:
    opr = AST::BinaryOperator::Times;
    break;
  case Term::DIV:
    opr = AST::BinaryOperator::Divide;
    break;
  }

  auto ret = std::make_unique<AST::BinaryOperatorExpression>(
      std::move(expr1), std::move(expr2), opr);
  ret->line = op.line;
  ret->column = op.column;
  return ret;
}

/**
   <atom-expression> -> ( <expression> )
                      | <constant-expression>
                      | <lvalue-expression>
                      | <call-expression>
 */
std::unique_ptr<AST::Expression> RDParser::parseAtomExpression() {

  auto fs = peek2();
  auto f = fs.first;
  auto s = fs.second;

  switch (f.getType().getValue()) {
  case Term::LPAREN: {
    lexer.getToken();
    auto expr = parseExpression();
    auto rp = lexer.getToken();
    assert(rp.getType() == Term::RPAREN && "expecting right parenthesis");
    return expr;
  }
  case Term::INTLIT:
  case Term::FLOATLIT:
  case Term::STRINGLIT:
    return parseConstantExpression();
  case Term::ID:
    if (s.getType() == Term::LPAREN)
      return parseCallExpression();
    else
      return parseLvalue();
  default:
    std::cout
        << "In parsing Declaration, expecting type, var or function, but get "
        << f.getTokenString() << "\n";
    exit(1);
  }
}

/**
   <constant-expression> -> <int-expression>
                          | <float-expression>
                          | <string-expression>
 */
std::unique_ptr<AST::Expression> RDParser::parseConstantExpression() {
  auto peek = peek1();
  switch (peek.getType().getValue()) {
  case Term::INTLIT:
    return parseIntExpression();
  case Term::FLOATLIT:
    return parseFloatExpression();
  case Term::STRINGLIT:
    return parseStringExpression();
  default:
    std::cout
        << "In parsing Declaration, expecting type, var or function, but get "
        << peek.getTokenString() << "\n";
    exit(1);
  }
}

/**
   <int-expression> -> IntegerLiteral
 */
std::unique_ptr<AST::IntExpression> RDParser::parseIntExpression() {
  auto i = lexer.getToken();
  assert(i.getType() == Term::INTLIT && "expecting float literals");
  auto ret =
      std::make_unique<AST::IntExpression>(atoi(i.getTokenString().c_str()));
  ret->line = i.line;
  ret->column = i.column;
  return ret;
}

/**
   <float-expression> -> FloatLiteral
 */
std::unique_ptr<AST::FloatExpression> RDParser::parseFloatExpression() {
  auto f = lexer.getToken();
  assert(f.getType() == Term::FLOATLIT && "expecting float literals");
  auto ret =
      std::make_unique<AST::FloatExpression>(atof(f.getTokenString().c_str()));
  ret->line = f.line;
  ret->column = f.column;
  return ret;
}

/**
   <string-expression> -> StringLiteral
 */
std::unique_ptr<AST::StringExpression> RDParser::parseStringExpression() {
  auto str = lexer.getToken();
  assert(str.getType() == Term::SEMI);
  auto ret = std::make_unique<AST::StringExpression>(str.getTokenString());
  ret->line = str.line;
  ret->column = str.column;
  return ret;
}

/**
   <lvalue-expression> -> id <lvalue-expression-tail>
 */
std::unique_ptr<AST::Variable> RDParser::parseLvalue() {
  auto id = lexer.getToken();
  assert(id.getType() == Term::ID && "expecting ID");
  auto var =
      std::make_unique<AST::SimpleVariable>(Symbol::make(id.getTokenString()));
  auto ret = parseLvalueTail(std::move(var));
  ret->line = id.line;
  ret->column = id.column;
  return ret;
}

/**
   <lvalue-expression-tail> -> [ <int-expression> ] <lvalue-expression-tail>
                             | . id <lvalue-expression-tail>
                             | NULL
 */
std::unique_ptr<AST::Variable>
RDParser::parseLvalueTail(std::unique_ptr<AST::Variable> &&var) {
  auto peek = peek1();
  if (peek.getType() == Term::LBRACK) {
    auto _ = lexer.getToken();
    auto expr = parseExpression();
    auto rb = lexer.getToken();
    assert(rb.getType() == Term::RBRACK && "expecting ]");
    return parseLvalueTail(std::make_unique<AST::SubscriptVariable>(
        std::move(var), std::move(expr)));
  }

  if (peek.getType() == Term::PERIOD) {
    auto _ = lexer.getToken();
    auto id = lexer.getToken();
    assert(id.getType() == Term::ID && "expecting ID");
    return parseLvalueTail(std::make_unique<AST::FieldVariable>(
        std::move(var), Symbol::make(id.getTokenString())));
  }
  return std::move(var);
}

} // namespace KT
