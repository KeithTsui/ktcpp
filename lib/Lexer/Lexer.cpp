//===-- Lexer.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/Lexer/Lexer.h"
#include "kt/Token/Token.h"
#include "kt/Token/TokenType.h"
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <unordered_map>

namespace KT {

std::map<std::string, TokenType::Terminal> Lexer::keywords{
    {"array", Term::ARRAY},
    {"break", Term::BREAK},
    {"do", Term::DO},
    {"else", Term::ELSE},
    {"end", Term::END},
    {"for", Term::FOR},
    {"function", Term::FUNCTION},
    {"if", Term::IF},
    {"in", Term::IN},
    {"let", Term::LET},
    {"of", Term::OF},
    {"then", Term::THEN},
    {"to", Term::TO},
    {"type", Term::TYPE},
    {"var", Term::VAR},
    {"while", Term::WHILE},
    {"endif", Term::ENDIF},
    {"begin", Term::BEGIN},
    {"enddo", Term::ENDDO},
    {"return", Term::RETURN},
    {"int", Term::INT},
    {"float", Term::FLOAT},
    {"string", Term::STRING},
    // {"bool", Term::BOOL},
    // {"true", Term::TRUE},
    // {"false", Term::FALSE},

    // "array", "break", "do",    "else",   "end", "for",  "function", "if",
    // "in",    "let",   "of",    "then",   "to",  "type", "var",      "while",
    // "endif", "begin", "enddo", "return", "int", "float", "bool", "true", "false"
};

std::unordered_map<int, std::string> TokenType::tokenTypeNames{
    {TokenType::Terminal::ARRAY, "array"},
    {TokenType::Terminal::BREAK, "break"},
    {TokenType::Terminal::DO, "do"},
    {TokenType::Terminal::ELSE, "else"},
    {TokenType::Terminal::END, "end"},
    {TokenType::Terminal::FOR, "for"},
    {TokenType::Terminal::FUNCTION, "function"},
    {TokenType::Terminal::IF, "if"},
    {TokenType::Terminal::IN, "in"},
    {TokenType::Terminal::LET, "let"},
    {TokenType::Terminal::OF, "of"},
    {TokenType::Terminal::THEN, "then"},
    {TokenType::Terminal::TO, "to"},
    {TokenType::Terminal::TYPE, "type"},
    {TokenType::Terminal::VAR, "var"},
    {TokenType::Terminal::WHILE, "while"},
    {TokenType::Terminal::ENDIF, "endif"},
    {TokenType::Terminal::BEGIN, "begin"},
    {TokenType::Terminal::ENDDO, "enddo"},
    {TokenType::Terminal::RETURN, "return"},
    {TokenType::Terminal::INT, "int"},
    {TokenType::Terminal::FLOAT, "float"},
    {TokenType::Terminal::FLOAT, "string"},
    {TokenType::Terminal::COMMA, ","},
    {TokenType::Terminal::COLON, ":"},
    {TokenType::Terminal::SEMI, ";"},
    {TokenType::Terminal::LPAREN, "("},
    {TokenType::Terminal::RPAREN, ")"},
    {TokenType::Terminal::LBRACK, "["},
    {TokenType::Terminal::RBRACK, "]"},
    {TokenType::Terminal::LBRACE, "{"},
    {TokenType::Terminal::RBRACE, "}"},
    {TokenType::Terminal::PERIOD, "."},
    {TokenType::Terminal::PLUS, "+"},
    {TokenType::Terminal::MINUS, "-"},
    {TokenType::Terminal::MULT, "*"},
    {TokenType::Terminal::DIV, "/"},
    {TokenType::Terminal::EQ, "="},
    {TokenType::Terminal::NEQ, "<>"},
    {TokenType::Terminal::LESSER, "<"},
    {TokenType::Terminal::GREATER, ">"},
    {TokenType::Terminal::LESSEREQ, "<="},
    {TokenType::Terminal::GREATEREQ, ">="},
    {TokenType::Terminal::AND, "&"},
    {TokenType::Terminal::OR, "|"},
    {TokenType::Terminal::ASSIGN, ":="},
    {TokenType::Terminal::ID, "id"},
    {TokenType::Terminal::INTLIT, "intlit"},
    {TokenType::Terminal::FLOATLIT, "floatlit"},
    {TokenType::Terminal::STRINGLIT, "stringlit"},
    {TokenType::Terminal::NULLL, "epsilon"},
    {TokenType::Terminal::EOFF, "EOF"},
    {TokenType::Nonterminal::TIGER_PROGRAM, "TIGER_PROGRAM"},
    {TokenType::Nonterminal::DECLARATION_LIST, "DECLARATION_LIST"},
    {TokenType::Nonterminal::STAT_SEQ, "STAT_SEQ"},
    {TokenType::Nonterminal::TYPE_DECLARATION_LIST, "TYPE_DECLARATION_LIST"},
    {TokenType::Nonterminal::VAR_DECLARATION_LIST, "VAR_DECLARATION_LIST"},
    {TokenType::Nonterminal::FUNCT_DECLARATION_LIST, "FUNCT_DECLARATION_LIST"},
    {TokenType::Nonterminal::TYPE_DECLARATION, "TYPE_DECLARATION"},
    {TokenType::Nonterminal::VAR_DECLARATION, "VAR_DECLARATION"},
    {TokenType::Nonterminal::FUNCT_DECLARATION, "FUNCT_DECLARATION"},
    {TokenType::Nonterminal::TYPE_EXPR, "TYPE_EXPR"},
    {TokenType::Nonterminal::TYPE_ID, "TYPE_ID"},
    {TokenType::Nonterminal::ID_LIST, "ID_LIST"},
    {TokenType::Nonterminal::OPTIONAL_INIT, "OPTIONAL_INIT"},
    {TokenType::Nonterminal::ID_LIST_TAIL, "ID_LIST_TAIL"},
    {TokenType::Nonterminal::CONST, "CONST"},
    {TokenType::Nonterminal::PARAM_LIST, "PARAM_LIST"},
    {TokenType::Nonterminal::RET_TYPE, "RET_TYPE"},
    {TokenType::Nonterminal::PARAM, "PARAM"},
    {TokenType::Nonterminal::PARAM_LIST_TAIL, "PARAM_LIST_TAIL"},
    {TokenType::Nonterminal::STAT, "STAT"},
    {TokenType::Nonterminal::STAT_SEQ_TAIL, "STAT_SEQ_TAIL"},
    {TokenType::Nonterminal::EXPR, "EXPR"},
    {TokenType::Nonterminal::STAT_IF_TAIL, "STAT_IF_TAIL"},
    {TokenType::Nonterminal::STAT_FUNCT_OR_ASSIGN, "STAT_FUNCT_OR_ASSIGN"},
    {TokenType::Nonterminal::LVALUE_TAIL, "LVALUE_TAIL"},
    {TokenType::Nonterminal::STAT_ASSIGN, "STAT_ASSIGN"},
    {TokenType::Nonterminal::EXPR_LIST, "EXPR_LIST"},
    {TokenType::Nonterminal::STAT_ASSIGN_STUFF, "STAT_ASSIGN_STUFF"},
    {TokenType::Nonterminal::STAT_ASSIGN_TAIL, "STAT_ASSIGN_TAIL"},
    {TokenType::Nonterminal::EXPR_TAIL, "EXPR_TAIL"},
    {TokenType::Nonterminal::OR_EXPR_TAIL, "OR_EXPR_TAIL"},
    {TokenType::Nonterminal::AND_EXPR_TAIL, "AND_EXPR_TAIL"},
    {TokenType::Nonterminal::COMPARE_TAIL, "COMPARE_TAIL"},
    {TokenType::Nonterminal::TERM_TAIL, "TERM_TAIL"},
    {TokenType::Nonterminal::OR_EXPR, "OR_EXPR"},
    {TokenType::Nonterminal::OR_OP, "OR_OP"},
    {TokenType::Nonterminal::AND_EXPR, "AND_EXPR"},
    {TokenType::Nonterminal::AND_OP, "AND_OP"},
    {TokenType::Nonterminal::COMPARE, "COMPARE"},
    {TokenType::Nonterminal::COMPARE_OP, "COMPARE_OP"},
    {TokenType::Nonterminal::TERM, "TERM"},
    {TokenType::Nonterminal::ADD_OP, "ADD_OP"},
    {TokenType::Nonterminal::FACTOR, "FACTOR"},
    {TokenType::Nonterminal::MUL_OP, "MUL_OP"},
    {TokenType::Nonterminal::LVALUE, "LVALUE"},
    {TokenType::Nonterminal::EXPR_LIST_TAIL, "EXPR_LIST_TAIL"}};

void Lexer::reset() {
  tokenPeekBuffer.clear();
  file.clear();
  file.seekg(0, std::ios::beg);
}

void Lexer::error(std::string msg) {
  numErrors++;
  std::cout << filename << ":" << curLineNum << ": Lexical error: " << msg
            << "\n";
}

Lexer::Lexer(std::string const &filename)
    : curLineNum{0}, curColumnNum{0},
      columnNumBak{0}, numErrors{0}, filename{filename}, file{filename} {}

char Lexer::get() {
  char c;
  file.get(c);
  if (c == '\n') {
    ++curLineNum;
    columnNumBak = curColumnNum;
    curColumnNum = 0;
    parentLine = std::move(prefix);
    prefix = "";
  } else {
    ++curColumnNum;
    prefix += c;
  }
  return c;
}

void Lexer::putback(char c) {
  if (c == '\n') {
    --curLineNum;
    curColumnNum = columnNumBak;
    prefix = std::move(parentLine);
    parentLine = "";
  } else {
    prefix.pop_back();
  }
  file.putback(c);
}

std::string Lexer::readDigits() {
  std::string sNumber{""};
  auto currChar = get();
  while (isdigit(currChar)) {
    sNumber += currChar;
    currChar = get();
  }
  if (isalpha(currChar)) {
    while (isalnum(currChar) || currChar == '_') {
      sNumber += currChar;
      currChar = get();
    }
    error(sNumber + " is an invalid identifier");
    return std::string();
  }
  putback(currChar);
  return sNumber;
}

std::string Lexer::readWordChars() {
  std::string word{""};
  auto currChar = get();
  while (isalnum(currChar) || currChar == '_') {
    word += currChar;
    currChar = get();
  }
  putback(currChar);
  return word;
}

std::string Lexer::readStringLiterals() {
  std::string word{""};
  auto currChar = get();
  while (currChar != '"') {
    word += currChar;
    currChar = get();
  }
  return word;
}

void Lexer::putToken(Token const &t) { tokenPeekBuffer.push_back(t); }

Token Lexer::getToken() {
  if (!tokenPeekBuffer.empty()) {
    auto t = tokenPeekBuffer.back();
    tokenPeekBuffer.pop_back();
    return t;
  }

  auto t = getTokenImpl();
  if (isDebug)
    std::cout << t.emit() << "\n";

  return t;
}

Token Lexer::getTokenImpl() {
  while (true) {

    auto currChar = get();
    auto currLine = getCurrLine();
    auto currColumn = getCurColumn();

    // If EOFF, return nothing
    if (file.eof()) {
      return {TokenType::Terminal::EOFF, "EOF", currLine, currColumn, 1};
    }

    // Skip whitespace characters
    if (isspace(currChar)) {
      continue;
    }
    // Read a word
    if (isalpha(currChar)) {
      std::string word = currChar + readWordChars();
      // Check if it is a keyword
      if (keywords.count(word)) {
        return {keywords[word], word, currLine, currColumn, int(word.size())};
      } else {
        return {TokenType::Terminal::ID, word, currLine, currColumn,
                int(word.size())};
      }
    }

    // Read an int or float
    if (isdigit(currChar)) {
      putback(currChar);
      std::string sNumber = readDigits();

      // For the case with a bad identifier
      if (sNumber == std::string()) {
        continue;
      }

      // If the first character of the integer is a '0' and the
      // integer is not exactly "0", we have an error
      if (sNumber.length() > 1 && sNumber.at(0) == '0') {
        error("Invalid token" + sNumber);
        continue;
      }
      currChar = get();

      // An int followed by a "." must be a float
      if (currChar == '.') {
        std::string sDecimals = readDigits();
        return {TokenType::Terminal::FLOATLIT, sNumber + "." + sDecimals,
                currLine, currColumn,
                int(sNumber.size() + sDecimals.size() + 1)};
      }
      // Else it is just an int
      putback(currChar);
      return {TokenType::Terminal::INTLIT, sNumber, currLine, currColumn,
              int(sNumber.size())};
    }

    // Read string literals
    if (currChar == '"') {
      auto strLit = readStringLiterals();
      return {Term::STRINGLIT, strLit, currLine, currColumn,
              int(strLit.size())};
    }

    // Handle symbols and operators
    // Invalid tokens will be found here
    char nextChar;
    switch (currChar) {
    // Single-character symbols
    case ',':
      return {TokenType::Terminal::COMMA, ",", currLine, currColumn, 1};
    case ';':
      return {TokenType::Terminal::SEMI, ";", currLine, currColumn, 1};
    case '(':
      return Token(TokenType::Terminal::LPAREN, "(", currLine, currColumn, 1);
    case ')':
      return Token(TokenType::Terminal::RPAREN, ")", currLine, currColumn, 1);
    case '[':
      return Token(TokenType::Terminal::LBRACK, "[", currLine, currColumn, 1);
    case ']':
      return Token(TokenType::Terminal::RBRACK, "]", currLine, currColumn, 1);
    case '{':
      return Token(TokenType::Terminal::LBRACE, "{", currLine, currColumn, 1);
    case '}':
      return Token(TokenType::Terminal::RBRACE, "}", currLine, currColumn, 1);
    case '.':
      return Token(TokenType::Terminal::PERIOD, ".", currLine, currColumn, 1);
    case '+':
      return Token(TokenType::PLUS, "+", currLine, currColumn, 1);
    case '-':
      return Token(TokenType::Terminal::MINUS, "-", currLine, currColumn, 1);
    case '*':
      return Token(TokenType::Terminal::MULT, "*", currLine, currColumn, 1);
    case '&':
      return Token(TokenType::Terminal::AND, "&", currLine, currColumn, 1);
    case '|':
      return Token(TokenType::Terminal::OR, "|", currLine, currColumn, 1);
    case '=':
      return Token(TokenType::Terminal::EQ, "=", currLine, currColumn, 1);
    case '_':
      putback(currChar);
      error(readWordChars() + " is not a valid identifier");
      continue;
    // Two-character symbols
    case ':':
      nextChar = get();
      if (nextChar == '=') {
        return Token(TokenType::Terminal::ASSIGN, ":=", currLine, currColumn,
                     2);
      }
      putback(nextChar);
      return Token(TokenType::Terminal::COLON, ":", currLine, currColumn, 1);
    case '<':
      nextChar = get();
      switch (nextChar) {
      case '>':
        return Token(TokenType::Terminal::NEQ, "<>", currLine, currColumn, 2);
      case '=':
        return Token(TokenType::Terminal::LESSEREQ, "<=", currLine, currColumn,
                     2);
      default:
        putback(nextChar);
        return Token(TokenType::Terminal::LESSER, "<", currLine, currColumn, 1);
      }
    case '>':
      nextChar = get();
      if (nextChar == '=') {
        return Token(TokenType::Terminal::GREATEREQ, ">=", currLine, currColumn,
                     2);
      }
      putback(nextChar);
      return Token(TokenType::Terminal::GREATER, ">", currLine, currColumn, 1);
    case '/':
      nextChar = get();
      if (nextChar == '*') {
        std::string comment = "Unclosed comment: /*";
        int currLine = getCurrLine();
        int currColumn = getCurColumn();
        currChar = get();
        if (currChar == -1) {
          error(comment);
          return Token(TokenType::Terminal::EOFF, "EOF", currLine, currColumn,
                       1);
        }
        comment += currChar;
        currLine = getCurrLine();
        currColumn = getCurColumn();
        nextChar = get();
        if (nextChar == -1) {
          error(comment);
          return Token(TokenType::Terminal::EOFF, "EOF", currLine, currColumn,
                       1);
        }
        comment += nextChar;
        while (currChar != '*' || nextChar != '/') {
          currChar = nextChar;
          currLine = getCurrLine();
          currColumn = getCurColumn();
          nextChar = get();
          if (nextChar == -1) {
            error(comment);
            return Token(TokenType::Terminal::EOFF, "EOF", currLine, currColumn,
                         1);
          }
          comment += nextChar;
        }

        // Ignore the comment
        continue;
      }
      putback(nextChar);
      return Token(TokenType::Terminal::DIV, "/", currLine, currColumn, 1);
    default:
      std::stringstream ss;
      ss << "Invalid symbol ";
      ss << std::to_string(currChar);
      ss << "line " << currLine << " column " << currColumn << "\n";
      error(ss.str());
      exit(1);
    }
  }
}

} // namespace KT
