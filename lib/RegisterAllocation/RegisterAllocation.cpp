//===-- RegisterAllocation.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/RegisterAllocation/RegisterAllocation.h"
#include "kt/Analystics/CFG.h"
#include "kt/Analystics/InterferenceGraph.h"
#include "kt/Analystics/Liveness.h"
#include "kt/CodeGen/LCCCodegen.h"
#include "kt/Frame/Frame.h"
#include "kt/IR/IR.h"
#include "kt/MIR/mir.h"
#include "kt/RegisterAllocation/Color.h"
#include "kt/Temporary/Temporary.h"
#include <algorithm>
#include <functional>
#include <list>
#include <memory>
#include <set>
#include <utility>
#include <vector>

namespace KT {

namespace RA {

RegisterAllocator::RegisterAllocator(
    std::list<std::unique_ptr<MIR::MachineInstruction>> &&prog,
    LCCFrame::Frame *f)
    : program{std::move(prog)}, frame{f} {
  std::list<MIR::MachineInstruction *> progr;
  for (auto &instr : program) {
    progr.push_back(instr.get());
  }
  cfg = std::make_unique<Analystics::ControlFlowGraph>(std::move(progr));
  ifGraph = std::make_unique<Analystics::InterferenceGraph>(cfg.get());

  std::cout << ifGraph->prettyPrint() << std::endl;
}

double RegisterAllocator::spillCost(Temporary temp) {
  double numDu = 0, interferes = 0;
  for (auto &cfgNode : cfg->nodes) {
    auto inDef = cfgNode->defs.count(temp);
    auto inUse = cfgNode->uses.count(temp);
    numDu += (inDef + inUse);
  }

  for (auto &inode : ifGraph->graph) {
    if (inode->temp == temp) {
      interferes = inode->adjacent.size();
      break;
    }
  }

  return numDu / interferes;
}

bool RegisterAllocator::isRedundant(MIR::MachineInstruction *insrt) {
  auto mv = llvm::dyn_cast_or_null<MIR::MIRMove>(insrt);
  if (mv) {
    return allocation[mv->dst] == allocation[mv->src];
  }
  return false;
}

// MIRs
void RegisterAllocator::regAlloc() {

  std::set<Register *> regs;
  for (auto &reg : LCCFrame::RegisterInfo::registerToName) {
    regs.insert(reg.first);
  }
  auto coloring =
      Color{ifGraph.get(), Allocation{},
            [this](Temporary t) -> double { return spillCost(t); }, regs};
  coloring.run();
  allocation = coloring.colored;
  spills = coloring.spills;

  if (spills.empty()) {
    program.remove_if(
        [this](auto &instr) -> bool { return isRedundant(instr.get()); });
  } else {
    // rewrite program to accommodate spills,
    doSpilling();
    // then do register allocation again.
    regAlloc();
  }
}

// rewrite program to accommodate spilled temporary.
// list of instruction: program.
// frame
// spills
// output: list of instruction
void RegisterAllocator::doSpilling() {
  std::list<std::unique_ptr<MIR::MachineInstruction>> transformedProgram;
  for (auto &spill : spills) {
    for (auto &mir : program) {
      auto transformed = transformInstruction(std::move(mir), spill);
      for (auto &tmir : transformed) {
        transformedProgram.push_back(std::move(tmir));
      }
    }
  }
  program = std::move(transformedProgram);
}

std::list<std::unique_ptr<MIR::MachineInstruction>>
RegisterAllocator::genInstrs(bool isDef, Temporary t) {
  LCC::LCCCodeGen codegen;
  auto access = frame->allocLocal(false);
  auto ae = LCCFrame::toIRExpr(
      access,
      std::make_unique<IR::TemporaryExpression>(LCCFrame::RegisterInfo::FP));
  IR::StmPtr move;
  if (isDef) {
    move = std::make_unique<IR::MoveStatement>(
        std::move(ae), std::make_unique<IR::TemporaryExpression>(t));
  } else {
    move = std::make_unique<IR::MoveStatement>(
        std::make_unique<IR::TemporaryExpression>(t), std::move(ae));
  }
  return codegen.codegen(move.get());
}

std::pair<std::list<std::unique_ptr<MIR::MachineInstruction>>,
          std::list<Temporary>>
RegisterAllocator::alloc_du(bool isDef, std::list<Temporary> dus, Temporary t) {
  if (std::find(dus.begin(), dus.end(), t) != dus.end()) {
    Temporary nt;
    auto mirs = genInstrs(isDef, nt);
    std::list<Temporary> ndus;
    for (auto t_ : dus) {
      if (t == t_) {
        ndus.push_back(nt);
      } else {
        ndus.push_back(t_);
      }
    }
    return std::make_pair(std::move(mirs), ndus);
  } else {
    std::list<std::unique_ptr<MIR::MachineInstruction>> l;
    return std::make_pair(std::move(l), dus);
  }
}

std::list<std::unique_ptr<MIR::MachineInstruction>>
RegisterAllocator::transformInstruction(
    std::unique_ptr<MIR::MachineInstruction> &&i, Temporary t) {
  if (auto instr = llvm::dyn_cast_or_null<MIR::MIROperation>(i.get())) {
    auto stores = alloc_du(true, instr->dst, t);
    auto fetchs = alloc_du(false, instr->src, t);
    std::list<std::unique_ptr<MIR::MachineInstruction>> ret;
    for (auto &instr : fetchs.first) {
      ret.push_back(std::move(instr));
    }
    instr->src = fetchs.second;
    instr->dst = stores.second;
    ret.push_back(std::move(i));
    for (auto &instr : stores.first) {
      ret.push_back(std::move(instr));
    }
    return ret;
  } else if (auto instr = llvm::dyn_cast_or_null<MIR::MIRMove>(i.get())) {
    auto stores = alloc_du(true, {instr->dst}, t);
    auto fetchs = alloc_du(false, {instr->src}, t);
    std::list<std::unique_ptr<MIR::MachineInstruction>> ret;
    for (auto &instr : fetchs.first) {
      ret.push_back(std::move(instr));
    }
    instr->src = fetchs.second.front();
    instr->dst = stores.second.front();
    ret.push_back(std::move(i));
    for (auto &instr : stores.first) {
      ret.push_back(std::move(instr));
    }
    return ret;
  }

  std::list<std::unique_ptr<MIR::MachineInstruction>> ret;
  ret.push_back(std::move(i));
  return ret;
}

} // namespace RA

} // namespace KT
