//===-- Color.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/RegisterAllocation/Color.h"
#include "kt/Analystics/InterferenceGraph.h"
#include "kt/Frame/Frame.h"
#include "kt/Support/Utilities.h"
#include "kt/Temporary/Temporary.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <list>
#include <set>

namespace KT {

namespace RA {

void Color::println(std::string const &msg) const {
  std::cout << msg << std::endl;
}

void Color::remove(std::list<IGNode *> &l, IGNode const *n) {
  l.remove_if([&n](IGNode *x) { return x == n; });
}
bool Color::isMemberOf(std::list<IGNode *> const &l, IGNode const *n) const {
  return std::find(l.cbegin(), l.cend(), n) != l.cend();
}

std::string Color::nameOf(IGNode const *n) const {
  return LCCFrame::RegisterInfo::registerName(n->temp);
}

//   //(* get degree of a node *)
int Color::degreeOf(IGNode const *n) const {
  if (n->status == IGNode::Status::InGraph) {
    return n->degree;
  } else {
    llvm::report_fatal_error("Calling degree on Removed or Colored node");
  }
}

void Color::addMove(IGNode *n, Move mv) {
  if (moveList.count(n->temp)) {
    moveList[n->temp].insert(mv);
  } else {
    moveList[n->temp] = {mv};
  }
}

//   //(* precolorTable is a mapping from temp to register,
//   //* while initial is a list of uncolored nodes *)
void Color::buildAndInitialize() {
  //(* initialize colored and precolored *)
  for (auto &node : ifGraph->graph) {
    if (initialAllocation.count(node->temp)) {
      auto r = initialAllocation[node->temp];
      colored[node->temp] = r;
      precolored.insert(node.get());
    } else {
      initialUncoloredNodes.push_back(node.get());
    }
  }

  //(* associate each node with a empty move set *)
  for (auto &node : ifGraph->graph) {
    moveList[node->temp] = {};
  }

  //(* iniitalize worklistMS and moveList *)
  for (auto &mv : ifGraph->moves) {
    auto src = mv.source;
    auto dst = mv.destination;
    if (precolored.find(src) != precolored.end()) {
      addMove(src, mv);
    }
    if (precolored.find(dst) != precolored.end()) {
      addMove(dst, mv);
    }
    worklistMoves.insert(mv);
  }
}

std::set<Move> Color::movesOfNode(IGNode const *n) {
  if (moveList.count(n->temp)) {
    std::set<Move> activeAndWorkListMS{};
    std::set_union(
        activeMoves.begin(), activeMoves.end(), worklistMoves.begin(),
        worklistMoves.end(),
        std::inserter(activeAndWorkListMS, activeAndWorkListMS.begin()));
    std::set<Move> diff;
    auto &x = moveList[n->temp];
    std::set_intersection(activeAndWorkListMS.begin(),
                          activeAndWorkListMS.end(), x.begin(), x.end(),
                          std::inserter(diff, diff.begin()));
    return diff;
  }
  return {};
}

bool Color::isMoveRelated(IGNode const *n) { return !movesOfNode(n).empty(); }

void Color::makeWorkList() {
  for (auto &node : initialUncoloredNodes) {
    if (node->status == IGNode::Status::InGraph) {
      if (node->degree >= K) {
        spillWorklist.push_front(node);
      } else if (isMoveRelated(node)) {
        freezeWorklist.push_front(node);
      } else {
        simplifyWorklist.push_front(node);
      }
    } else {
      llvm_unreachable("error in make work list.");
    }
  }
}
void Color::enableMoves(std::set<IGNode *> const &nodes) {
  for (auto node : nodes) {
    for (auto &mv : movesOfNode(node)) {
      if (activeMoves.count(mv)) {
        activeMoves.erase(mv);
        worklistMoves.insert(mv);
      }
    }
  }
}

void Color::addWorkList(IGNode *node) {
  if (node->status == IGNode::Status::InGraph) {
    if (precolored.count(node) == 0 && !isMoveRelated(node) &&
        node->degree < K) {
      freezeWorklist.remove(node);
      simplifyWorklist.push_front(node);
    }
  } else {
    llvm_unreachable("error in add work list.");
  }
}

IGNode *Color::getAliasOf(IGNode *n) {
  if (coalescedNodes.count(n)) {
    return getAliasOf(alias[n->temp]);
  }
  return n;
}

//   // (* adjacenent nodes *)
std::set<IGNode *> Color::getAdjacencies(IGNode const *node) {
  std::set<IGNode *> selectStackSet;
  for (auto n : selectStatck) {
    selectStackSet.insert(n);
  }
  std::set<IGNode *> adjSet;
  for (auto n : node->adjacent) {
    adjSet.insert(n);
  }
  std::set<IGNode *> un;
  std::set_union(selectStackSet.begin(), selectStackSet.end(),
                 coalescedNodes.begin(), coalescedNodes.end(),
                 std::inserter(un, un.begin()));

  std::set<IGNode *> diff;
  std::set_difference(adjSet.begin(), adjSet.end(), un.begin(), un.end(),
                      std::inserter(diff, diff.begin()));

  return diff;
}

//   //(* decrement degree for graph node n, return
//   //* modified degreeMap and a (possibly augmented) simplify worklist *)
//   // (* only decrement those non-precolored nodes - for *)
//   //(* precolored nodes, we treat as if they have infinite *)
//   //(* degree, since we shouldn't reassign them to different registers *)

void Color::decrementDegree(IGNode *n) {
  if (n->status == IGNode::Status::InGraph) {
    if (initialAllocation.count(n->temp) == 0) {
      if (n->degree-- == K) {
        auto nodes = getAdjacencies(n);
        nodes.insert(n);
        enableMoves(nodes);
        spillWorklist.remove(n);
        if (isMoveRelated(n)) {
          freezeWorklist.push_front(n);
        } else {
          simplifyWorklist.push_front(n);
        }
      }
    }
  } else {
    llvm_unreachable("error in add work list.");
  }
}

bool Color::inAdjacent(IGNode const *a, IGNode const *b) {
  return std::find(a->adjacent.begin(), a->adjacent.end(), b) !=
         a->adjacent.end();
}

bool Color::isOK(IGNode *a, IGNode const *b) {
  return a->degree < K || precolored.count(a) != 0 || inAdjacent(a, b);
}
bool Color::isAllOK(IGNode *a, std::set<IGNode *> const &nodes) {
  bool ret = true;
  for (auto n : nodes) {
    ret &= isOK(a, n);
  }
  return ret;
}

bool Color::isConservative(std::set<IGNode *> const &nodes) {
  int k = 0;
  for (auto n : nodes) {
    if (n->degree >= K) {
      ++k;
    }
  }
  return k < K;
}

void Color::addEdge(IGNode *a, IGNode *b) {
  if (a->status == IGNode::Status::InGraph &&
      b->status == IGNode::Status::InGraph) {
    if (!inAdjacent(a, b) && a != b) {
      if (precolored.count(a)) {
        a->adjacent.insert(b);
        ++a->degree;
      }

      if (precolored.count(b)) {
        b->adjacent.insert(a);
        ++b->degree;
      }
    }

  } else {
    llvm_unreachable("error in add Edge.");
  }
}

void Color::combine(IGNode *a, IGNode *b) {
  if (std::find(freezeWorklist.begin(), freezeWorklist.end(), b) !=
      freezeWorklist.end()) {
    freezeWorklist.remove(b);
  } else {
    spillWorklist.remove(b);
  }
  coalescedNodes.insert(b);
  alias[b->temp] = a;
  auto mv_a = moveList[a->temp];
  auto mv_b = moveList[b->temp];
  std::set<Move> un;
  std::set_union(mv_a.begin(), mv_a.end(), mv_b.begin(), mv_b.end(),
                 std::inserter(un, un.begin()));
  moveList[a->temp] = un;
  enableMoves({b});
  for (auto n : getAdjacencies(b)) {
    addEdge(n, a);
    decrementDegree(n);
  }
  if (a->degree >= K && (std::find(freezeWorklist.begin(), freezeWorklist.end(),
                                   a) != freezeWorklist.end())) {
    freezeWorklist.remove(a);
    spillWorklist.push_front(a);
  }
}

void Color::coalesce() {
  assert(!worklistMoves.empty() &&
         "ensure that work list moves is not empty when coalesce");
  auto mv = *worklistMoves.begin();
  auto x = getAliasOf(mv.source);
  auto y = getAliasOf(mv.destination);
  IGNode *u = x, *v = y;
  if (precolored.count(y)) {
    u = y;
    v = x;
  }
  worklistMoves.erase(mv);
  if (u == v) {
    coalescedMoves.insert(mv);
    addWorkList(u);
  } else {
    if (precolored.count(v) || inAdjacent(u, v)) {
      constraintedMoves.insert(mv);
      addWorkList(u);
      addWorkList(v);
    } else {
      auto uAdjs = getAdjacencies(u);
      auto vAdjs = getAdjacencies(v);
      std::set<IGNode *> un;
      std::set_union(uAdjs.begin(), uAdjs.end(), vAdjs.begin(), vAdjs.end(),
                     std::inserter(un, un.begin()));

      if ((precolored.count(u) && isAllOK(u, getAdjacencies(v))) ||
          (!precolored.count(u) && isConservative(un))) {
        coalescedMoves.insert(mv);
        combine(u, v);
        addWorkList(u);
      } else {
        activeMoves.insert(mv);
      }
    }
  }
}

void Color::freezeMoves(IGNode *n) {
  auto mvs = movesOfNode(n);
  for (auto m : mvs) {
    auto x = m.source;
    auto y = m.destination;
    auto tx = x->temp;
    auto ty = x->temp;
    auto v = getAliasOf(n) == getAliasOf(y) ? getAliasOf(x) : getAliasOf(y);
    activeMoves.erase(m);
    frozenMoves.insert(m);
    if (movesOfNode(v).empty() && v->degree < K && !precolored.count(v)) {
      freezeWorklist.remove(v);
      simplifyWorklist.push_front(v);
    }
  }
}

void Color::freeze() {
  if (!freezeWorklist.empty()) {
    auto u = freezeWorklist.front();
    freezeWorklist.pop_front();

    simplifyWorklist.push_front(u);
    freezeMoves(u);
  }
}

void Color::simplify() {
  if (simplifyWorklist.empty())
    return;
  auto n = simplifyWorklist.front();
  simplifyWorklist.pop_front();
  selectStatck.push_front(n);
  auto adjs = getAdjacencies(n);
  for (auto r : adjs) {
    decrementDegree(r);
  }
}

void Color::selectSpill() {
  if (!spillWorklist.empty()) {
    IGNode *min = spillWorklist.front();
    auto minCost = spillCost(min->temp);
    for (auto n : spillWorklist) {
      auto nCost = spillCost(n->temp);
      if (minCost >= nCost) {
        min = n;
        minCost = nCost;
      }
    }
    spillWorklist.remove(min);
    simplifyWorklist.push_front(min);
    freezeMoves(min);
  }
}

Register *Color::pickColor(std::set<Register *> const &registers) {
  return *registers.begin();
}

//   //(* assign color to all nodes on select stack. The parameter
//   //* colored is all nodes that are already assigned a color. *)
void Color::assignColors() {
  while (!selectStatck.empty()) {
    auto n = selectStatck.front();
    selectStatck.pop_front();

    std::set<Register *> avaliableColors = registers;
    for (auto w : n->adjacent) {
      if (colored.count(w->temp)) {
        avaliableColors.erase(colored[w->temp]);
      }

      auto w_ = getAliasOf(w);
      if (coloredNodes.count(w_) && precolored.count(w_)) {
        if (colored.count(w_->temp)) {
          avaliableColors.erase(colored[w_->temp]);
        }
      }
    }

    if (avaliableColors.empty()) {
      spillNodes.insert(n);
    } else {
      auto r = pickColor(avaliableColors);
      coloredNodes.insert(n);
      colored[n->temp] = r;
    }
  }

  for (auto n : coalescedNodes) {
    auto t = getAliasOf(n)->temp;
    if (colored.count(t)) {
      auto c = colored[t];
      colored[n->temp] = c;
    }
  }
}

void Color::iteration() {
  if (!simplifyWorklist.empty()) {
    simplify();
    iteration();
  } else if (!worklistMoves.empty()) {
    coalesce();
    iteration();
  } else if (!freezeWorklist.empty()) {
    freeze();
    iteration();
  } else if (!spillWorklist.empty()) {
    selectSpill();
    iteration();
  }
}

void Color::run() {
  colored.clear();
  spills.clear();
  buildAndInitialize();
  makeWorkList();

  // show lists
  std::cout << "SimplifyWorkList: \n";
  outputPtrContainer(std::cout, simplifyWorklist) << "\n";

  std::cout << "SpillWorkList: \n";
  outputPtrContainer(std::cout, spillWorklist) << "\n";

  std::cout << "FreezeWorkList: \n";
  outputPtrContainer(std::cout, freezeWorklist) << "\n";

  iteration();
  assignColors();
  for (auto n : spillNodes) {
    spills.push_back(n->temp);
  }
}

} // namespace RA

} // namespace KT
