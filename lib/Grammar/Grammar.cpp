//===-- Grammar.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/Grammar/Gramma.h"
#include "kt/Token/TokenType.h"

namespace KT {

namespace Grammar {

std::vector<ProductionRule> ktGrammar{
    //# tiger-program
    // 1: <tiger-program> -> let <declaration-list> in <stat-seq> end
    {Nonterm::TIGER_PROGRAM,
     {Term::LET, Nonterm::DECLARATION_LIST, Term::IN, Nonterm::STAT_SEQ}},

    // #declarations
    // 2: <declaration-list> -> <declaration> <declaration-list>
    {Nonterm::DECLARATION_LIST,
     {Nonterm::DECLARATION, Nonterm::DECLARATION_LIST}},
    // 3: <declaration-list> -> NULL
    {Nonterm::DECLARATION_LIST, {Term::NULLL}},

    // #declaration
    // 4: <declaration> -> <type-declaration>
    {Nonterm::DECLARATION, {Nonterm::TYPE_DECLARATION}},
    // 5: <declaration> -> <var-declaration>
    {Nonterm::DECLARATION, {Nonterm::VAR_DECLARATION}},
    // 6: <declaration> -> <func-declaration>
    {Nonterm::DECLARATION, {Nonterm::FUNCT_DECLARATION}},

    // # type-declaration
    // 9: <type-declaration> -> type id = <type>;
    {Nonterm::TYPE_DECLARATION,
     {Term::TYPE, Term::ID, Term::EQ, Nonterm::TYPE_EXPR}},
    // 10: <type> -> <type-id>
    {Nonterm::TYPE_EXPR, {Nonterm::TYPE_ID}},
    // 11: <type> -> <array-type>
    {Nonterm::TYPE_EXPR, {Nonterm::ARRAY_TYPE}},

    {Nonterm::TYPE_EXPR, {Nonterm::RECORD_TYPE}},
    // 11: <array-type> -> array [INTLIT] of <type-id>
    {Nonterm::ARRAY_TYPE,
     {Term::ARRAY, Term::LBRACK, Term::INTLIT, Term::OF, Nonterm::TYPE_ID}},

    // 12: <type> -> {id: <type-id>}
    {Nonterm::RECORD_TYPE, {Term::LBRACE, Nonterm::PARAM_LIST, Term::RBRACE}},

    // 13: <type-id> -> id
    {Nonterm::TYPE_ID, {Term::ID}},
    // 14: <type-id> -> int
    {Nonterm::TYPE_ID, {Term::INT}},
    // 15: <type-id> -> float
    {Nonterm::TYPE_ID, {Term::FLOAT}},

    //       # var-declaration
    // 16: <var-declaration> -> var id : <type> <optional-init>;
    {Nonterm::VAR_DECLARATION,
     {Term::VAR, Term::ID, Term::COLON, Nonterm::TYPE_EXPR,
      Nonterm::OPTIONAL_INIT}},
    // 17: <optional-init> -> := <exp>
    {Nonterm::OPTIONAL_INIT, {Nonterm::EXPR}},
    // 18: <optional-init> -> NULL
    {Nonterm::OPTIONAL_INIT, {Term::NULLL}},

    // # funct-declaration
    // 19: <funct-declaration> -> function id (<param-list>) <ret-type> begin
    // <stat-seq> end;

    // 20: <param-list> -> <param> <param-list-tail>

    // 21: <param-list> -> NULL

    // 22: <param-list-tail> -> , <param> <param-list-tail>

    // 23: <param-list-tail> -> NULL

    // 24: <ret-type> -> : <type>

    // 25: <ret-type> -> NULL

    // 26: <param> -> id : <type>
    //
};

} // namespace Grammar

} // namespace KT
