//===-- LCCCodeGen.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#include "kt/CodeGen/LCCCodegen.h"
#include "kt/Frame/Frame.h"
#include "kt/IR/IR.h"
#include "kt/MIR/mir.h"
#include "kt/Temporary/Temporary.h"
#include <functional>
#include <list>
#include <memory>
#include <sstream>
#include <string>

namespace KT {

namespace LCC {

void LCCCodeGen::emit(std::unique_ptr<MIR::MachineInstruction> &&i) {
  instrs.push_back(std::move(i));
}

Temporary
LCCCodeGen::result(std::function<void(Temporary)> instructionGenerator) {
  Temporary t;
  instructionGenerator(t);
  return t;
}

std::string LCCCodeGen::intToStr(int n) {
  std::stringstream ss;
  ss << n;
  return ss.str();
}

std::list<Temporary> const &LCCCodeGen::getCallDefs() {
  static std::list<Temporary> defs;
  defs.push_back(LCCFrame::RegisterInfo::RV);
  return defs;
}

void LCCCodeGen::munchStatement(IR::Statement *stm) {
  if (auto x = llvm::dyn_cast_or_null<IR::SequenceStatement>(stm)) {
    munchStatement(x->head.get());
    munchStatement(x->tail.get());
    return;
  } else if (auto x = llvm::dyn_cast_or_null<IR::LabelStatement>(stm)) {
    emit(std::make_unique<MIR::MIRLabel>(x->name.sym.name + ":", x->name));
    return;
  }
  // data movement instructions
  else if (auto move = llvm::dyn_cast_or_null<IR::MoveStatement>(stm)) {

    // 1: store to memory (str)
    if (auto mem =
            llvm::dyn_cast_or_null<IR::MemoryExpression>(move->dst.get())) {
      if (auto biOp = llvm::dyn_cast_or_null<IR::BinaryOperationExpression>(
              mem->exp.get())) {

        if (biOp->op == IR::BiOp::PLUS) {
          if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                  biOp->rhs.get())) {
            std::stringstream asmStr;
            asmStr << "str `s0, `s1, " << const_->i;
            auto s1 = munchExpression(move->src.get());
            auto s2 = munchExpression(biOp->lhs.get());
            std::list<Temporary> s;
            s.push_back(s1);
            s.push_back(s2);
            std::list<Temporary> d;
            std::list<Label> j;
            emit(std::make_unique<MIR::MIROperation>(
                asmStr.str(), std::move(s), std::move(d), std::move(j)));
            return;
          } else if (auto const_ =
                         llvm::dyn_cast_or_null<IR::ConstantExpression>(
                             biOp->lhs.get())) {
            std::stringstream asmStr;
            asmStr << "str `s0, `s1, " << const_->i;
            auto s1 = munchExpression(move->src.get());
            auto s2 = munchExpression(biOp->rhs.get());
            std::list<Temporary> s;
            s.push_back(s1);
            s.push_back(s2);
            std::list<Temporary> d;
            std::list<Label> j;
            emit(std::make_unique<MIR::MIROperation>(
                asmStr.str(), std::move(s), std::move(d), std::move(j)));
            return;
          }
        } else if (biOp->op == IR::BiOp::MINUS) {
          if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                  biOp->rhs.get())) {
            std::stringstream asmStr;
            asmStr << "str `s0, `s1, " << -const_->i;
            auto s1 = munchExpression(move->src.get());
            auto s2 = munchExpression(biOp->lhs.get());
            std::list<Temporary> s;
            s.push_back(s1);
            s.push_back(s2);
            std::list<Temporary> d;
            std::list<Label> j;
            emit(std::make_unique<MIR::MIROperation>(
                asmStr.str(), std::move(s), std::move(d), std::move(j)));
            return;
          } else if (auto const_ =
                         llvm::dyn_cast_or_null<IR::ConstantExpression>(
                             biOp->lhs.get())) {
            std::stringstream asmStr;
            asmStr << "str `s0, `s1, " << -const_->i;
            auto s1 = munchExpression(move->src.get());
            auto s2 = munchExpression(biOp->rhs.get());
            std::list<Temporary> s;
            s.push_back(s1);
            s.push_back(s2);
            std::list<Temporary> d;
            std::list<Label> j;
            emit(std::make_unique<MIR::MIROperation>(
                asmStr.str(), std::move(s), std::move(d), std::move(j)));
            return;
          }
        }
      } else {
        std::stringstream asmStr;
        asmStr << "str `s0, `s1, 0";
        auto s1 = munchExpression(move->src.get());
        auto s2 = munchExpression(mem->exp.get());
        std::list<Temporary> s;
        s.push_back(s1);
        s.push_back(s2);
        std::list<Temporary> d;
        std::list<Label> j;
        emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                 std::move(d), std::move(j)));
        return;
      }
    }
    // 2: load to register (mvi)
    else if (auto temp = llvm::dyn_cast_or_null<IR::TemporaryExpression>(
                 move->dst.get())) {
      if (auto const_ =
              llvm::dyn_cast_or_null<IR::ConstantExpression>(move->src.get())) {
        std::stringstream asmStr;
        asmStr << "mvi `d0, " << const_->i;
        std::list<Temporary> s;
        std::list<Temporary> d;
        d.push_back(temp->temp);
        std::list<Label> j;
        emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(d),
                                                 std::move(s), std::move(j)));
        return;
      } else if (auto mem = llvm::dyn_cast_or_null<IR::MemoryExpression>(
                     move->src.get())) {
        if (auto biOp = llvm::dyn_cast_or_null<IR::BinaryOperationExpression>(
                mem->exp.get())) {

          if (biOp->op == IR::BiOp::PLUS) {
            if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                    biOp->rhs.get())) {
              std::stringstream asmStr;
              asmStr << "ldr `d0, `s0, " << const_->i;
              auto s2 = munchExpression(biOp->lhs.get());
              std::list<Temporary> s;
              s.push_back(s2);
              std::list<Temporary> d;
              d.push_back(temp->temp);
              std::list<Label> j;
              emit(std::make_unique<MIR::MIROperation>(
                  asmStr.str(), std::move(s), std::move(d), std::move(j)));
              return;
            } else if (auto const_ =
                           llvm::dyn_cast_or_null<IR::ConstantExpression>(
                               biOp->lhs.get())) {
              std::stringstream asmStr;
              asmStr << "ldr `d0, `s0, " << const_->i;
              auto s2 = munchExpression(biOp->rhs.get());
              std::list<Temporary> s;
              s.push_back(s2);
              std::list<Temporary> d;
              d.push_back(temp->temp);
              std::list<Label> j;
              emit(std::make_unique<MIR::MIROperation>(
                  asmStr.str(), std::move(s), std::move(d), std::move(j)));
              return;
            }
          } else if (biOp->op == IR::BiOp::MINUS) {
            if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                    biOp->rhs.get())) {
              std::stringstream asmStr;
              asmStr << "ldr `d0, `s0, " << -const_->i;
              auto s2 = munchExpression(biOp->lhs.get());
              std::list<Temporary> s;
              s.push_back(s2);
              std::list<Temporary> d;
              d.push_back(temp->temp);
              std::list<Label> j;
              emit(std::make_unique<MIR::MIROperation>(
                  asmStr.str(), std::move(s), std::move(d), std::move(j)));
              return;
            } else if (auto const_ =
                           llvm::dyn_cast_or_null<IR::ConstantExpression>(
                               biOp->lhs.get())) {
              std::stringstream asmStr;
              asmStr << "ldr `d0, `s0, " << -const_->i;

              // fix me: need to make a new IR expression for calculate the
              // source register.
              auto s2 = munchExpression(biOp->rhs.get());
              std::list<Temporary> s;

              s.push_back(s2);
              std::list<Temporary> d;
              d.push_back(temp->temp);
              std::list<Label> j;
              emit(std::make_unique<MIR::MIROperation>(
                  asmStr.str(), std::move(s), std::move(d), std::move(j)));
              return;
            }
          }
        }
        // 3. move from register to register
        else {
          std::stringstream asmStr;
          asmStr << "mvr `d0, `s0";
          auto s1 = munchExpression(move->src.get());
          std::list<Temporary> s;
          s.push_back(s1);
          std::list<Temporary> d;
          d.push_back(temp->temp);
          std::list<Label> j;
          emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                   std::move(d), std::move(j)));
          return;
        }
      }
    }

  }
  // branching
  else if (auto jump = llvm::dyn_cast_or_null<IR::JumpStatement>(stm)) {
    if (auto name =
            llvm::dyn_cast_or_null<IR::NameExpression>(jump->exp.get())) {
      std::stringstream asmStr;
      asmStr << "br `j0";
      std::list<Temporary> s;
      std::list<Temporary> d;
      std::list<Label> j;
      j.push_back(name->name);
      emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                               std::move(d), std::move(j)));
      return;
    } else {
      std::stringstream asmStr;
      asmStr << "jmp `s0";
      std::list<Temporary> s;
      auto s0 = munchExpression(jump->exp.get());
      s.push_back(s0);
      std::list<Temporary> d;
      std::list<Label> j;
      for (auto &l : jump->possibleLocations) {
        j.push_back(l);
      }
      emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                               std::move(d), std::move(j)));
      return;
    }
  }
  // conditional branching
  else if (auto cjump = llvm::dyn_cast_or_null<IR::CJumpStatement>(stm)) {
    llvm_unreachable("not implemented yet.");

  } else if (auto exp = llvm::dyn_cast_or_null<IR::ExpressionStatement>(stm)) {
    munchExpression(exp->exp.get());
    return;
  }

  llvm_unreachable("");
}

Temporary LCCCodeGen::munchExpression(IR::Expression *exp) {
  if (auto mem = llvm::dyn_cast_or_null<IR::MemoryExpression>(exp)) {
    if (auto const_ =
            llvm::dyn_cast_or_null<IR::ConstantExpression>(mem->exp.get())) {
      return result([i = const_->i, this](auto r) {
        std::stringstream asmStr;
        asmStr << "ldr `d0, `s0, 0";
        auto constExpr = std::make_unique<IR::ConstantExpression>(i);
        auto s0 = munchExpression(constExpr.get());
        std::list<Temporary> s, d;
        s.push_back(s0);
        d.push_back(r);
        std::list<Label> j;
        emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                 std::move(d), std::move(j)));
      });
    } else if (auto biOp =
                   llvm::dyn_cast_or_null<IR::BinaryOperationExpression>(
                       mem->exp.get())) {

      if (biOp->op == IR::BiOp::PLUS) {
        if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                biOp->rhs.get())) {

          std::stringstream asmStr;
          asmStr << "ldr `d0, `s0, " << const_->i;
          auto s2 = munchExpression(biOp->lhs.get());
          std::list<Temporary> s;
          s.push_back(s2);
          std::list<Temporary> d;
          Temporary t;
          d.push_back(t);
          std::list<Label> j;
          emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                   std::move(d), std::move(j)));
          return t;
        } else if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                       biOp->lhs.get())) {
          std::stringstream asmStr;
          asmStr << "ldr `d0, `s0, " << const_->i;
          auto s2 = munchExpression(biOp->rhs.get());
          std::list<Temporary> s;
          s.push_back(s2);
          std::list<Temporary> d;
          Temporary t;
          d.push_back(t);
          std::list<Label> j;
          emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                   std::move(d), std::move(j)));
          return t;
        }
      } else if (biOp->op == IR::BiOp::MINUS) {
        if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                biOp->rhs.get())) {
          std::stringstream asmStr;
          asmStr << "ldr `d0, `s0, " << -const_->i;
          auto s2 = munchExpression(biOp->lhs.get());
          std::list<Temporary> s;
          s.push_back(s2);
          std::list<Temporary> d;
          Temporary t;
          d.push_back(t);
          std::list<Label> j;
          emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                   std::move(d), std::move(j)));
          return t;
        } else if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                       biOp->lhs.get())) {
          std::stringstream asmStr;
          asmStr << "ldr `d0, `s0, " << -const_->i;
          auto s2 = munchExpression(biOp->rhs.get());
          std::list<Temporary> s;
          s.push_back(s2);
          std::list<Temporary> d;
          Temporary t;
          d.push_back(t);
          std::list<Label> j;
          emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                   std::move(d), std::move(j)));
          return t;
        } else {
          std::stringstream asmStr;
          asmStr << "ldr `d0, `s0, 0";
          auto s2 = munchExpression(mem->exp.get());
          std::list<Temporary> s;
          s.push_back(s2);
          std::list<Temporary> d;
          Temporary t;
          d.push_back(t);
          std::list<Label> j;
          emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                   std::move(d), std::move(j)));
          return t;
        }
      }
    }
  } else if (auto biOp =
                 llvm::dyn_cast_or_null<IR::BinaryOperationExpression>(exp)) {

    if (biOp->op == IR::BiOp::PLUS) {
      if (auto const_ =
              llvm::dyn_cast_or_null<IR::ConstantExpression>(biOp->rhs.get())) {

        std::stringstream asmStr;
        asmStr << "add `d0, `s0, " << const_->i;
        auto s2 = munchExpression(biOp->lhs.get());
        std::list<Temporary> s;
        s.push_back(s2);
        std::list<Temporary> d;
        Temporary t;
        d.push_back(t);
        std::list<Label> j;
        emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                 std::move(d), std::move(j)));
        return t;
      } else if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                     biOp->lhs.get())) {
        std::stringstream asmStr;
        asmStr << "add `d0, `s0, " << const_->i;
        auto s2 = munchExpression(biOp->rhs.get());
        std::list<Temporary> s;
        s.push_back(s2);
        std::list<Temporary> d;
        Temporary t;
        d.push_back(t);
        std::list<Label> j;
        emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                 std::move(d), std::move(j)));
        return t;
      } else {
        std::stringstream asmStr;
        asmStr << "add `d0, `s0, `s1" << const_->i;
        auto s1 = munchExpression(biOp->lhs.get());
        auto s2 = munchExpression(biOp->rhs.get());
        std::list<Temporary> s;
        s.push_back(s1);
        s.push_back(s2);
        std::list<Temporary> d;
        Temporary t;
        d.push_back(t);
        std::list<Label> j;
        emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                 std::move(d), std::move(j)));
        return t;
      }
    } else if (biOp->op == IR::BiOp::MINUS) {
      if (auto const_ =
              llvm::dyn_cast_or_null<IR::ConstantExpression>(biOp->rhs.get())) {
        std::stringstream asmStr;
        asmStr << "sub `d0, `s0, " << const_->i;
        auto s2 = munchExpression(biOp->lhs.get());
        std::list<Temporary> s;
        s.push_back(s2);
        std::list<Temporary> d;
        Temporary t;
        d.push_back(t);
        std::list<Label> j;
        emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                 std::move(d), std::move(j)));
        return t;
      } else if (biOp->op == IR::BiOp::DIV) {
        if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                biOp->rhs.get())) {
          std::stringstream asmStr;
          asmStr << "div `d0, `s0, `s1";
          auto s1 = munchExpression(biOp->lhs.get());
          auto s2 = munchExpression(biOp->rhs.get());
          std::list<Temporary> s;
          s.push_back(s1);
          s.push_back(s2);
          std::list<Temporary> d;
          Temporary t;
          d.push_back(t);
          std::list<Label> j;
          emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                   std::move(d), std::move(j)));
          return t;
        }
      } else if (biOp->op == IR::BiOp::MUL) {
        if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                biOp->rhs.get())) {
          std::stringstream asmStr;
          asmStr << "mul `d0, `s0, `s1";
          auto s1 = munchExpression(biOp->lhs.get());
          auto s2 = munchExpression(biOp->rhs.get());
          std::list<Temporary> s;
          s.push_back(s1);
          s.push_back(s2);
          std::list<Temporary> d;
          Temporary t;
          d.push_back(t);
          std::list<Label> j;
          emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                   std::move(d), std::move(j)));
          return t;
        }
      } else if (biOp->op == IR::BiOp::MINUS) {
        if (auto const_ = llvm::dyn_cast_or_null<IR::ConstantExpression>(
                biOp->rhs.get())) {
          std::stringstream asmStr;
          asmStr << "sub `d0, `s0, `s1";
          auto s1 = munchExpression(biOp->lhs.get());
          auto s2 = munchExpression(biOp->rhs.get());
          std::list<Temporary> s;
          s.push_back(s1);
          s.push_back(s2);
          std::list<Temporary> d;
          Temporary t;
          d.push_back(t);
          std::list<Label> j;
          emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                                   std::move(d), std::move(j)));
          return t;
        }
      }
    }
  } else if (auto const_ =
                 llvm::dyn_cast_or_null<IR::ConstantExpression>(exp)) {
    std::stringstream asmStr;
    asmStr << "mvi `d0, " << const_->i;
    std::list<Temporary> s;
    std::list<Temporary> d;
    Temporary t;
    d.push_back(t);
    std::list<Label> j;
    emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                             std::move(d), std::move(j)));
    return t;
  } else if (auto temp = llvm::dyn_cast_or_null<IR::TemporaryExpression>(exp)) {
    return temp->temp;
  } else if (auto label = llvm::dyn_cast_or_null<IR::NameExpression>(exp)) {
    std::stringstream asmStr;
    asmStr << "lea `d0, " << label->name.sym.name;
    std::list<Temporary> s;
    std::list<Temporary> d;
    Temporary t;
    d.push_back(t);
    std::list<Label> j;
    emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                             std::move(d), std::move(j)));
    return t;
  } else if (auto call = llvm::dyn_cast_or_null<IR::CallExpression>(exp)) {
    std::stringstream asmStr;
    asmStr << "jssr `s0 ";
    std::list<IR::Expression *> args;
    auto regFunc = munchExpression(call->func.get());
    std::list<Temporary> s;
    s.push_back(regFunc);
    std::list<Temporary> d;
    Temporary t;
    for (auto &r : getCallDefs()) {
      d.push_back(r);
    }
    std::list<Label> j;
    emit(std::make_unique<MIR::MIROperation>(asmStr.str(), std::move(s),
                                             std::move(d), std::move(j)));
    return t;
  }
  llvm_unreachable("");
}

void LCCCodeGen::munchArgs(std::list<IR::Expression *> args) {
  for (auto &arg : args) {
    std::list<Temporary> dest;
    std::list<Temporary> src;
    src.push_back(munchExpression(arg));
    std::list<Label> jump;
    emit(std::make_unique<MIR::MIROperation>("push `s0", std::move(dest),
                                             std::move(src), std::move(jump)));
  }
}

std::list<std::unique_ptr<MIR::MachineInstruction>>
LCCCodeGen::codegen(IR::Statement *stm) {
  instrs.clear();
  munchStatement(stm);
  return std::move(instrs);
}

} // namespace LCC

} // namespace KT
