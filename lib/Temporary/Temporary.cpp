//===-- Temporary.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/Temporary/Temporary.h"

namespace KT {

Temporary::TemporaryID Temporary::nextID = 100;

Label::LabelID Label::nextID = 0;

} // namespace KT
