//===-- Frame.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/Frame/Frame.h"
#include "kt/IR/IR.h"
#include "kt/Temporary/Temporary.h"
#include <list>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace KT {

namespace LCCFrame {

Access::~Access(){};
Fragment::~Fragment(){};

Frame::Frame(Label const n, std::vector<bool> const &fs)
    : name{n}, locals{0}, formals{fs.size()} {
  for (int i = wordSize; i < fs.size(); ++i) {
    /// auto x = std::make_unique<InFrame>(i + wordSize);
    formals.push_back(std::make_unique<InFrame>(i + wordSize));
  }
}

Frame::Frame(Frame &&f)
    : name{f.name}, formals{std::move(f.formals)}, locals{f.locals} {}

Register RegisterInfo::r1;
Register RegisterInfo::r2;
Register RegisterInfo::r3;
Register RegisterInfo::r4;
Register RegisterInfo::FP;
Register RegisterInfo::SP;
Register RegisterInfo::RA;
Register RegisterInfo::RV;

std::vector<Register *> RegisterInfo::specialArgs{
    &RegisterInfo::FP, &RegisterInfo::SP, &RegisterInfo::RA, &RegisterInfo::RV};
std::vector<Register *> RegisterInfo::argRegs{};
std::vector<Register *> RegisterInfo::calleeSaves{};
std::vector<Register *> RegisterInfo::callerSaves{
    &RegisterInfo::r1, &RegisterInfo::r2, &RegisterInfo::r3, &RegisterInfo::r4};
std::map<std::string, Register *> RegisterInfo::regList{
    {"r0", &RV}, {"r1", &r1}, {"r2", &r2}, {"r3", &r3},
    {"r4", &r4}, {"fp", &FP}, {"sp", &SP}, {"lr", &RA},
};

std::map<Register *, std::string> RegisterInfo::registerToName{
    {&RV, "r0"}, {&r1, "r1"}, {&r2, "r2"}, {&r3, "r3"},
    {&r4, "r4"}, {&FP, "fp"}, {&SP, "sp"}, {&RA, "lr"},
};

std::vector<std::string> RegisterInfo::registers{"r0", "r1", "r2", "r3",
                                                 "r4", "fp", "sp", "lr"};

std::string RegisterInfo::registerName(Register *r) {
  if (registerToName.count(r)) {
    return registerToName[r];
  } else {
    return r->prettyPrint();
  }
}

bool RegisterInfo::isRegister(Temporary t) {
  for (auto &reg : regList) {
    if (*reg.second == t)
      return true;
  }
  return false;
}

std::string RegisterInfo::registerName(Temporary t) {
  for (auto &reg : registerToName) {
    if (*reg.first == t)
      return reg.second;
  }
  return t.prettyPrint();
}

IR::ExprPtr toIRExpr(Access *frameAccess, IR::ExprPtr &&temp) {
  if (auto *inFrame = llvm::dyn_cast_or_null<InFrame>(frameAccess)) {
    auto c = std::make_unique<IR::ConstantExpression>(inFrame->offset);
    auto p = std::make_unique<IR::BinaryOperationExpression>(
        IR::BiOp::PLUS, std::move(temp), std::move(c));
    auto m = std::make_unique<IR::MemoryExpression>(std::move(p));
    return m;
  } else if (auto *inReg = llvm::dyn_cast_or_null<InReg>(frameAccess)) {
    std::make_unique<IR::TemporaryExpression>(inReg->reg);
  }
  llvm_unreachable("");
}

} // namespace LCCFrame

} // namespace KT
