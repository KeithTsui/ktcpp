//===-- AST.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/AST/AST.h"
#include "kt/SymbolTable/Symbol.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace KT {

namespace AST {

ASTNode::~ASTNode(){};

std::string SimpleVariable::prettyPrint() const {
  std::stringstream ss;
  ss << "<SimpleVariable>";
  ss << sym.prettyPrint();
  ss << "<SimpleVariable/>\n";
  return ss.str();
}

std::string FieldVariable::prettyPrint() const {
  std::stringstream ss;
  ss << "<FieldVariable>\n";
  if (var)
    ss << var->prettyPrint();
  ss << "<member>";
  ss << sym.prettyPrint();
  ss << "<member/>\n";
  ss << "<FieldVariable/>\n";
  return ss.str();
}

std::string SubscriptVariable::prettyPrint() const {
  std::stringstream ss;
  ss << "<SubscriptVariable>\n";
  if (var)
    ss << var->prettyPrint();
  ss << "<subscript>";
  if (exp)
    ss << exp->prettyPrint();
  ss << "<subscript/>\n";
  ss << "<SubscriptVariable/>\n";
  return ss.str();
}

std::string VariableExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<VariableExpression>\n";
  if (var)
    ss << var->prettyPrint();
  ss << "<VariableExpression/>\n";
  return ss.str();
}

std::string NilExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<NilExpression>";
  ss << "<NilExpression/>\n";
  return ss.str();
}

std::string IntExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<IntExpression>";
  ss << value;
  ss << "<IntExpression/>\n";
  return ss.str();
}

std::string FloatExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<FloatExpression>";
  ss << value;
  ss << "<FloatExpression/>\n";
  return ss.str();
}

std::string StringExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<StringExpression>";
  ss << value;
  ss << "<StringExpression/>\n";
  return ss.str();
}

std::string CallExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<CallExpression>";
  ss << "<function>";
  ss << func.prettyPrint();
  ss << "<function/>\n";
  ss << "<arguments>";
  std::vector<std::string> strs(args.size());
  std::transform(args.begin(), args.end(), strs.begin(),
                 [](auto &decl) { return decl ? decl->prettyPrint() : ""; });
  for (auto &str : strs) {
    ss << str << "\n";
  }
  ss << "<arguments/>\n";
  ss << "<CallExpression/>\n";
  return ss.str();
}

std::map<BinaryOperator, std::string> binOpNames{
    {BinaryOperator::Plus, "Plus"},
    {BinaryOperator::Minus, "Minus"},
    {BinaryOperator::Times, "Times"},
    {BinaryOperator::Divide, "Divide"},
    {BinaryOperator::Equal, "Equal"},
    {BinaryOperator::NotEqual, "NotEqual"},
    {BinaryOperator::Less, "Less"},
    {BinaryOperator::LessOrEqual, "LessOrEqual"},
    {BinaryOperator::Greater, "Greater"},
    {BinaryOperator::GreaterOrEqual, "GreaterOrEqual"},
    {BinaryOperator::And, "And"},
    {BinaryOperator::Or, "Or"}};

std::string BinaryOperatorExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<BinaryOperatorExpression>";
  ss << "<left>";
  if (lhs)
    ss << lhs->prettyPrint();
  ss << "<left/>\n";
  ss << "<op>";
  ss << " " << binOpNames[opr] << " ";
  ss << "<op/>\n";
  ss << "<right>";
  if (rhs)
    ss << rhs->prettyPrint();
  ss << "<right/>\n";
  ss << "<BinaryOperatorExpression/>\n";
  return ss.str();
}

std::string NamedArgument::prettyPrint() const {
  std::stringstream ss;
  ss << "<NamedArgument>";
  ss << "<name>";
  ss << sym.prettyPrint();
  ss << "<name/>\n";
  ss << "<init>";
  if (exp)
    ss << exp->prettyPrint();
  ss << "<init/>\n";
  ss << "<NamedArgument/>\n";
  return ss.str();
}

std::string RecordExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<RecordExpression>";
  ss << type.prettyPrint();
  ss << "<fields>";
  std::vector<std::string> strs(arguments.size());
  std::transform(arguments.begin(), arguments.end(), strs.begin(),
                 [](auto &decl) { return decl.prettyPrint(); });
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<fields>";
  ss << "<RecordExpression/>\n";
  return ss.str();
}

std::string SequenceExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<SequenceExpression>";
  std::vector<std::string> strs(exps.size());
  std::transform(exps.begin(), exps.end(), strs.begin(),
                 [](auto &decl) { return decl ? decl->prettyPrint() : ""; });
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<SequenceExpression/>\n";
  return ss.str();
}

std::string AssignmentStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<AssignmentStatement>";
  ss << "<lvalue>";
  if (var)
    ss << var->prettyPrint();
  ss << "<lvalue/>\n";
  ss << "<rvalue>";
  if (exp)
    ss << exp->prettyPrint();
  ss << "<rvalue/>\n";
  ss << "<AssignmentStatement/>\n";
  return ss.str();
}

std::string IfStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<IfStatement>";
  ss << "<condition>";
  if (condition)
    ss << condition->prettyPrint();
  ss << "<condition/>\n";
  ss << "<then>";
  std::vector<std::string> strs(thenBranch.size());
  std::transform(thenBranch.begin(), thenBranch.end(), strs.begin(),
                 [](auto &decl) { return decl ? decl->prettyPrint() : ""; });
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<then/>\n";
  ss << "<else>";
  strs.clear();
  strs.resize(elseBranch.size());
  std::transform(elseBranch.begin(), elseBranch.end(), strs.begin(),
                 [](auto &decl) { return decl ? decl->prettyPrint() : ""; });
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<else/>\n";
  ss << "<IfStatement/>\n";
  return ss.str();
}

std::string WhileStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<WhileStatement>";
  ss << "<condition>";
  if (condition)
    ss << condition->prettyPrint();
  ss << "<condition/>\n";
  ss << "<body>";
  std::vector<std::string> strs(body.size());
  std::transform(body.begin(), body.end(), strs.begin(),
                 [](auto &decl) { return decl ? decl->prettyPrint() : ""; });
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<body/>\n";
  ss << "<WhileStatement/>\n";
  return ss.str();
}

std::string ForStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<ForStatement>";

  ss << "<lvalue>";
  ss << var.prettyPrint();
  ss << "<lvalue/>\n";
  ss << "<low>";
  if (lowerBound)
    ss << "low: " << lowerBound->prettyPrint();
  ss << "<low/>\n";
  ss << "<high>";
  if (higherBound)
    ss << "high: " << higherBound->prettyPrint();
  ss << "<high/>\n";

  ss << "<body>";
  std::vector<std::string> strs(body.size());
  std::transform(body.begin(), body.end(), strs.begin(),
                 [](auto &decl) { return decl ? decl->prettyPrint() : ""; });
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<body>";
  ss << "<ForStatement/>\n";
  return ss.str();
}
std::string BreakStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<BreakStatement>";
  ss << "<BreakStatement/>\n";
  return ss.str();
}

std::string ReturnStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<ReturnStatement>";
  ss << "<ReturnStatement/>\n";
  return ss.str();
}

std::string LetStatement::prettyPrint() const {
  std::stringstream ss;
  ss << "<let>\n";
  std::vector<std::string> strs(declarations.size());
  std::transform(declarations.begin(), declarations.end(), strs.begin(),
                 [](auto &decl) { return decl ? decl->prettyPrint() : ""; });
  ss << "<declarations>\n";
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<declarations/>\n";
  strs.clear();

  strs.resize(body.size());
  std::transform(body.begin(), body.end(), strs.begin(),
                 [](auto &stm) { return stm ? stm->prettyPrint() : ""; });

  ss << "<body>\n";
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<body/>\n";

  ss << "<let/>\n";

  return ss.str();
}

std::string ArrayExpression::prettyPrint() const {
  std::stringstream ss;
  ss << "<ArrayExpression>";
  ss << "<type>";
  if (type)
    ss << type->prettyPrint();
  ss << "<type/>\n";
  ss << "<size>";
  if (size)
    ss << size->prettyPrint();
  ss << "<size/>\n";
  ss << "<init>";
  if (initializer)
    ss << initializer->prettyPrint();
  ss << "<init/>\n";
  ss << "<ArrayExpression/>\n";
  return ss.str();
}

std::string Parameter::prettyPrint() const {
  std::stringstream ss;
  ss << "<Parameter>";
  ss << "<name>";
  ss << name.prettyPrint();
  ss << "<name>";
  ss << "<type>";
  if (type)
    ss << type->prettyPrint();
  ss << "<type/>\n";
  ss << "<Parameter/>\n";
  return ss.str();
}

std::string RecordField::prettyPrint() const {
  std::stringstream ss;
  ss << "<RecordField>";
  ss << "<name>";
  ss << name.prettyPrint();
  ss << "<name/>\n";
  ss << "<type>";
  if (type)
    ss << type->prettyPrint();
  ss << "<type/>\n";
  ss << "<RecordField/>\n";
  return ss.str();
}

std::string FunctionDeclaration::prettyPrint() const {
  std::stringstream ss;
  ss << "<FunctionDeclaration>";
  ss << "<name>";
  ss << name.prettyPrint();
  ss << "<name/>\n";

  ss << "<parameters>";
  std::vector<std::string> strs(parameters.size());
  std::transform(parameters.begin(), parameters.end(), strs.begin(),
                 [](auto &decl) { return decl.prettyPrint(); });
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<parameters/>\n";

  ss << "<resultType>";
  if (resultType)
    ss << resultType->prettyPrint();
  ss << "<resultType/>\n";

  ss << "<body>";
  strs.clear();
  strs.resize(body.size());
  std::transform(body.begin(), body.end(), strs.begin(),
                 [](auto &decl) { return decl ? decl->prettyPrint() : ""; });
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<body/>\n";
  ss << "<FunctionDeclaration/>\n";
  return ss.str();
}

std::string VariableDeclaration::prettyPrint() const {
  std::stringstream ss;
  ss << "<VariableDeclaration>";
  ss << "<name>";
  ss << name.prettyPrint();
  ss << "<name/>\n";
  ss << "<type>";
  if (type)
    ss << type->prettyPrint();
  ss << "<type/>\n";
  ss << "<init>";
  if (initializer)
    ss << initializer->prettyPrint();
  ss << "<init/>\n";
  ss << "<VariableDeclaration/>\n";
  return ss.str();
}

std::string TypeDeclaration::prettyPrint() const {
  std::stringstream ss;
  ss << "<TypeDeclaration>";
  ss << "<name>";
  ss << name.prettyPrint();
  ss << "<name/>\n";
  ss << "<type>";
  if (type)
    ss << type->prettyPrint();
  ss << "<type/>\n";
  ss << "<TypeDeclaration/>\n";
  return ss.str();
}

std::string NamedType::prettyPrint() const {
  std::stringstream ss;
  ss << "<NamedType>";
  ss << name.prettyPrint();
  ss << "<NamedType/>\n";
  return ss.str();
}

std::string RecordType::prettyPrint() const {
  std::stringstream ss;
  ss << "<RecordType>";
  ss << "<fields>";
  std::vector<std::string> strs(fields.size());
  std::transform(fields.begin(), fields.end(), strs.begin(),
                 [](auto &decl) { return decl.prettyPrint(); });
  for (auto &str : strs) {
    ss << str;
  }
  ss << "<fields/>\n";
  ss << "<RecordType/>\n";
  return ss.str();
}

std::string ArraryType::prettyPrint() const {
  std::stringstream ss;
  ss << "<ArrayType>";
  ss << "<type>";
  ss << type->prettyPrint();
  ss << "<type/>\n";
  ss << "<size>";
  ss << size;
  ss << "<size/>\n";
  ss << "<Arraytype/>\n";
  return ss.str();
}

} // namespace AST

} // namespace KT
