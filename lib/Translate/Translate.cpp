//===-- Translate.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/Translate/Translate.h"
#include "kt/Frame/Frame.h"
#include "kt/IR/IR.h"
#include "kt/Temporary/Temporary.h"
#include <algorithm>
#include <functional>
#include <iostream>
#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace KT {

namespace Sema {

namespace Trans {

Level::~Level() {}
Expr::~Expr() {}

IR::ExprPtr externalCall(std::string const func,
                         std::vector<IR::ExprPtr> &&arguments) {
  auto n = std::make_unique<IR::NameExpression>(func);
  return std::make_unique<IR::CallExpression>(std::move(n),
                                              std::move(arguments));
}

IR::StmPtr toIRSeqStm(std::list<IR::StmPtr> &&stms) {
  if (stms.empty()) {
    return std::make_unique<IR::ExpressionStatement>(
        std::make_unique<IR::ConstantExpression>(0));
  }
  auto head = std::move(stms.front());
  stms.pop_front();
  auto tail = toIRSeqStm(std::move(stms));
  return std::make_unique<IR::SequenceStatement>(std::move(head),
                                                 std::move(tail));
}

IR::StmPtr toIRSeqStm(std::vector<IR::StmPtr> &&stms) {
  std::list<IR::StmPtr> stm_list;
  for (auto &stm : stms) {
    stm_list.push_back(std::move(stm));
  }
  return toIRSeqStm(std::move(stm_list));
}

IR::ExprPtr unEx(TExprPtr &&exp) {
  auto *expr = exp.get();
  if (auto ex = llvm::dyn_cast_or_null<Ex>(expr)) {
    return std::move(ex->irExp);
  } else if (auto nx = llvm::dyn_cast_or_null<Nx>(expr)) {
    return std::make_unique<IR::SequenceExpression>(
        std::move(nx->irStm), std::make_unique<IR::ConstantExpression>());
  } else if (auto cx = llvm::dyn_cast_or_null<Cx>(expr)) {
    auto r = Temporary();
    auto t = Label();
    auto f = Label();

    auto move1 = std::make_unique<IR::MoveStatement>(
        std::make_unique<IR::TemporaryExpression>(r),
        std::make_unique<IR::ConstantExpression>(1));
    auto stm = cx->conditionJumpStmGen(t, f);
    auto falseStm = std::make_unique<IR::LabelStatement>(f);
    auto move0 = std::make_unique<IR::MoveStatement>(
        std::make_unique<IR::TemporaryExpression>(r),
        std::make_unique<IR::ConstantExpression>(0));
    auto trueStm = std::make_unique<IR::LabelStatement>(t);

    std::vector<IR::StmPtr> stms{5};
    stms.push_back(std::move(move1));
    stms.push_back(std::move(stm));
    stms.push_back(std::move(falseStm));
    stms.push_back(std::move(move0));
    stms.push_back(std::move(trueStm));
    auto sequence = toIRSeqStm(std::move(stms));

    auto r2 = std::make_unique<IR::TemporaryExpression>(r);
    return std::make_unique<IR::SequenceExpression>(std::move(sequence),
                                                    std::move(r2));
  }
  std::cerr << exp->prettyPrint() << std::endl;
  llvm_unreachable("");
}

IR::StmPtr unNx(TExprPtr &&exp) {
  auto *expr = exp.get();
  if (auto ex = llvm::dyn_cast_or_null<Ex>(expr)) {
    return std::make_unique<IR::ExpressionStatement>(std::move(ex->irExp));
  } else if (auto nx = llvm::dyn_cast_or_null<Nx>(expr)) {
    return std::move(nx->irStm);
  } else if (auto cx = llvm::dyn_cast_or_null<Cx>(expr)) {
    auto t = Label();
    auto stm = cx->conditionJumpStmGen(t, t);
    return std::make_unique<IR::LabelStatement>(t);
  }
  llvm_unreachable("");
}
std::function<IR::StmPtr(Label, Label)> unCx(TExprPtr &&exp) {
  auto *expr = exp.get();
  if (auto ex = llvm::dyn_cast_or_null<Ex>(expr)) {
    auto cj = new IR::CJumpStatement{
        IR::RelOp::EQ, std::move(ex->irExp),
        std::make_unique<IR::ConstantExpression>(0), Label(), Label()};
    return [cj](Label t, Label f) -> IR::StmPtr {
      cj->yes = t;
      cj->no = f;
      return std::unique_ptr<IR::CJumpStatement>(cj);
    };
  } else if (auto cx = llvm::dyn_cast_or_null<Cx>(expr)) {
    return std::move(cx->conditionJumpStmGen);
  }
  llvm_unreachable("");
}

TExprPtr binop(AST::BinaryOperator op, std::unique_ptr<Expr> &&lhs,
               std::unique_ptr<Expr> &&rhs) {
  auto l = unEx(std::move(lhs));
  auto r = unEx(std::move(rhs));
  IR::BiOp irOp;
  switch (op) {
  case AST::BinaryOperator::Plus:
    irOp = IR::BiOp::PLUS;
    break;
  case AST::BinaryOperator::Minus:
    irOp = IR::BiOp::MINUS;
    break;
  case AST::BinaryOperator::Times:
    irOp = IR::BiOp::MUL;
    break;
  case AST::BinaryOperator::Divide:
    irOp = IR::BiOp::DIV;
    break;
  default:
    llvm_unreachable("biop exhausted");
  }
  auto b = std::make_unique<IR::BinaryOperationExpression>(irOp, std::move(l),
                                                           std::move(r));
  return std::make_unique<Ex>(std::move(b));
}

TExprPtr relOp(AST::BinaryOperator op, std::unique_ptr<Expr> &&lhs,
               std::unique_ptr<Expr> &&rhs) {
  auto l = unEx(std::move(lhs));
  auto r = unEx(std::move(rhs));
  IR::RelOp irOp;
  switch (op) {
  case AST::BinaryOperator::Equal:
    irOp = IR::RelOp::EQ;
    break;
  case AST::BinaryOperator::NotEqual:
    irOp = IR::RelOp::NE;
    break;
  case AST::BinaryOperator::Less:
    irOp = IR::RelOp::LT;
    break;
  case AST::BinaryOperator::LessOrEqual:
    irOp = IR::RelOp::LE;
    break;
  case AST::BinaryOperator::Greater:
    irOp = IR::RelOp::GT;
    break;
  case AST::BinaryOperator::GreaterOrEqual:
    irOp = IR::RelOp::GE;
    break;
  default:
    llvm_unreachable("Relop exhausted");
  }
  auto cj = new IR::CJumpStatement{irOp, std::move(l), std::move(r), Label(),
                                   Label()};
  return std::make_unique<Cx>([cj](Label t, Label f) -> IR::StmPtr {
    cj->yes = t;
    cj->no = f;
    return std::unique_ptr<IR::Statement>(cj);
  });
}

std::unique_ptr<IR::MemoryExpression> memPlus(IR::ExprPtr e1, IR::ExprPtr e2) {
  return std::make_unique<IR::MemoryExpression>(
      std::make_unique<IR::BinaryOperationExpression>(
          IR::BiOp::PLUS, std::move(e1), std::move(e2)));
}

std::unique_ptr<Ex> errExp() {
  return std::make_unique<Ex>(std::make_unique<IR::ConstantExpression>(0));
}

std::unique_ptr<Ex> nilExp() {
  return std::make_unique<Ex>(std::make_unique<IR::ConstantExpression>(0));
}

std::string Ex::prettyPrint() const {
  std::stringstream ss;
  ss << "<Ex>";
  ss << irExp->prettyPrint();
  ss << "<Ex/>";
  return ss.str();
}

std::string Nx::prettyPrint() const {
  std::stringstream ss;
  ss << "<Nx>";
  ss << irStm->prettyPrint();
  ss << "<Nx/>";
  return ss.str();
}

std::string Cx::prettyPrint() const {
  std::stringstream ss;
  ss << "<Cx>";
  auto res = conditionJumpStmGen(Label("T"), Label("N"));
  ss << res->prettyPrint();
  ss << "<Cx/>";
  return ss.str();
}

Level *Translator::currentLevel() {
  Level *curLevel = &outermost;
  while (curLevel->child) {
    curLevel = curLevel->child.get();
  }
  return curLevel;
}

void Translator::enterFunction(std::string const &name,
                               std::vector<bool> const &formals) {
  auto curLevel = currentLevel();
  curLevel->child = std::make_unique<Lev>(curLevel, name, formals);
}

void Translator::exitFunction() { currentLevel()->parent->child.reset(); }

std::unique_ptr<Access> Translator::allocLocal() {
  if (auto curLev = llvm::dyn_cast_or_null<Lev>(currentLevel())) {
    auto frameAccess = curLev->frame->allocLocal();
    return std::make_unique<Access>(currentLevel(), std::move(frameAccess));
  } else {
    llvm_unreachable("must not in top level, top level is no frame");
  }
}

std::unique_ptr<Ex> Translator::intLiteralsToEx(int i) {
  return std::make_unique<Ex>(std::make_unique<IR::ConstantExpression>(i));
}

std::unique_ptr<Ex> Translator::stringLiteralsToEx(std::string const &str) {
  for (auto &frag : fragments) {
    if (auto strFrag = llvm::dyn_cast_or_null<LCCFrame::String>(frag.get())) {
      if (strFrag->content == str) {
        return std::make_unique<Ex>(
            std::make_unique<IR::NameExpression>(strFrag->name));
      }
    }
  }
  fragments.push_back(std::make_unique<LCCFrame::String>(str));
  return std::make_unique<Ex>(std::make_unique<IR::NameExpression>());
}

//
TExprPtr Translator::simpleVar(Access *def, Level *currentLevel) {
  auto fp =
      std::make_unique<IR::TemporaryExpression>(LCCFrame::RegisterInfo::FP);

  auto defLev = def->level;

  IR::ExprPtr ret;

  while (true) {
    auto curLevel = currentLevel;
    IR::ExprPtr &&temp = std::move(fp);

    if (auto curLev = llvm::dyn_cast_or_null<Lev>(curLevel)) {
      if (curLevel == defLev) {
        ret = LCCFrame::toIRExpr(def->frameAccess.get(), std::move(temp));
        break;
      } else {
        // curLev->frame
        auto staticLink = curLev->frame->formals.front().get();
        auto parent = curLev->parent;
        auto next = LCCFrame::toIRExpr(staticLink, std::move(temp));
        curLevel = parent;
        temp = std::move(next);
      }
    } else {
      llvm_unreachable("");
    }
  }

  return std::make_unique<Ex>(std::move(ret));
}

TExprPtr Translator::subscriptVar(TExprPtr &&base, TExprPtr &&offset) {
  auto unexBase = unEx(std::move(base));
  auto unexOffset = unEx(std::move(offset));
  auto bi = std::make_unique<IR::BinaryOperationExpression>(
      IR::BiOp::MUL, std::move(unexOffset),
      std::make_unique<IR::ConstantExpression>(LCCFrame::wordSize));
  return std::make_unique<Ex>(memPlus(std::move(unexBase), std::move(bi)));
}

TExprPtr Translator::fieldVar(TExprPtr &&base, std::string const &id,
                              std::vector<std::string> const &fieldList) {
  auto offset =
      std::find(fieldList.begin(), fieldList.end(), id) - fieldList.begin();
  auto unexBase = unEx(std::move(base));
  auto bi = std::make_unique<IR::BinaryOperationExpression>(
      IR::BiOp::MUL, std::make_unique<IR::ConstantExpression>(offset),
      std::make_unique<IR::ConstantExpression>(LCCFrame::wordSize));
  return std::make_unique<Ex>(memPlus(std::move(unexBase), std::move(bi)));
}

TExprPtr Translator::ifelse(TExprPtr &&test, TExprPtr &&then,
                            TExprPtr &&else_) {
  auto r = Temporary();
  auto t = Temporary();
  auto f = Temporary();
  auto finish = Label();
  auto testFunc = unCx(std::move(test));

  if (auto exe = llvm::dyn_cast_or_null<Ex>(then.get())) {
    auto &e = exe->irExp;

  } else if (auto exe = llvm::dyn_cast_or_null<Nx>(then.get())) {
    auto &thenstm = exe->irStm;

  } else if (auto exe = llvm::dyn_cast_or_null<Cx>(then.get())) {
    auto &cf = exe->conditionJumpStmGen;

  } else {
  }
  llvm_unreachable("not implemented yet");
}
TExprPtr Translator::record(std::vector<TExprPtr> &&fields) {
  llvm_unreachable("not implemented yet");
}

TExprPtr Translator::array(TExprPtr &&size, TExprPtr &&init) {

  std::vector<IR::ExprPtr> args{2};

  args.push_back(unEx(std::move(size)));
  args.push_back(unEx(std::move(init)));

  IR::ExprPtr ir = externalCall("initArray", std::move(args));
  return std::make_unique<Ex>(std::move(ir));
}

TExprPtr Translator::assign(TExprPtr &&lvalue, TExprPtr &&rvalue) {
  return std::make_unique<Nx>(std::make_unique<IR::MoveStatement>(
      unEx(std::move(lvalue)), unEx(std::move(rvalue))));
}

TExprPtr Translator::loop(TExprPtr &&test, TExprPtr &&body, Label done) {
  auto testLabel = Label();
  auto bodyLabel = Label();

  IR::StmPtr testStm = std::make_unique<IR::LabelStatement>(testLabel);
  auto testExpr = unEx(std::move(test));
  IR::StmPtr cjump = std::make_unique<IR::CJumpStatement>(
      IR::RelOp::EQ, std::move(testExpr),
      std::make_unique<IR::ConstantExpression>(0), done, bodyLabel);
  IR::StmPtr bl = std::make_unique<IR::LabelStatement>(bodyLabel);
  IR::StmPtr b = unNx(std::move(body));
  std::vector<Label> tls = {testLabel};
  IR::StmPtr jump = std::make_unique<IR::JumpStatement>(
      std::make_unique<IR::NameExpression>(testLabel), std::move(tls));
  IR::StmPtr d = std::make_unique<IR::LabelStatement>(done);

  std::vector<IR::StmPtr> exps{6};
  exps.push_back(std::move(testStm));
  exps.push_back(std::move(cjump));
  exps.push_back(std::move(bl));
  exps.push_back(std::move(b));
  exps.push_back(std::move(jump));
  exps.push_back(std::move(d));

  auto seq = toIRSeqStm(std::move(exps));
  return std::make_unique<Nx>(std::move(seq));
}

TExprPtr Translator::break_(Label b) {
  std::vector<Label> ls = {b};
  return std::make_unique<Nx>(std::make_unique<IR::JumpStatement>(
      std::make_unique<IR::NameExpression>(b), std::move(ls)));
}

TExprPtr Translator::call(Level *use, Level *def, Label lab,
                          std::vector<TExprPtr> &&exps, bool isProcedure) {
  if (auto lv = llvm::dyn_cast_or_null<Lev>(def)) {
    if (auto par = llvm::dyn_cast_or_null<Top>(lv->parent)) {
      std::vector<IR::ExprPtr> args;
      for (auto &exp : exps) {
        args.push_back(unEx(std::move(exp)));
      }
      auto externCall = externalCall(lab.sym.name, std::move(args));
      if (isProcedure) {
        auto e =
            std::make_unique<IR::ExpressionStatement>(std::move(externCall));
        return std::make_unique<Nx>(std::move(e));
      } else {
        return std::make_unique<Ex>(std::move(externCall));
      }
    }
  }

  auto depth = [](Level *lvl) -> int {
    int ret = 0;
    while (true) {
      if (auto l = llvm::dyn_cast_or_null<Top>(lvl)) {
        break;
      } else if (auto l = llvm::dyn_cast_or_null<Lev>(lvl)) {
        ++ret;
        lvl = l->parent;
      }
    }
    return ret;
  };

  auto diff = depth(use) - depth(def) + 1;

  IR::ExprPtr ret =
      std::make_unique<IR::TemporaryExpression>(LCCFrame::RegisterInfo::FP);

  auto curLevel = use;

  while (true) {
    if (diff == 0) {
      break;
    }
    if (auto curLev = llvm::dyn_cast_or_null<Lev>(curLevel)) {
      // curLev->frame
      auto staticLink = curLev->frame->formals.front().get();
      auto parent = curLev->parent;
      auto next = LCCFrame::toIRExpr(staticLink, std::move(ret));
      curLevel = parent;
      ret = std::move(next);
    } else {
      llvm_unreachable("");
    }
  }

  IR::ExprPtr func = std::make_unique<IR::NameExpression>(lab);

  std::vector<IR::ExprPtr> args{1};
  args.push_back(std::move(ret));

  for (auto &exp : exps) {
    args.push_back(unEx(std::move(exp)));
  }
  auto call =
      std::make_unique<IR::CallExpression>(std::move(func), std::move(args));
  if (isProcedure) {
    return std::make_unique<Nx>(
        std::make_unique<IR::ExpressionStatement>(std::move(call)));
  } else {
    return std::make_unique<Ex>(std::move(call));
  }
}

TExprPtr Translator::sequence(std::vector<TExprPtr> &&exps) {
  if (exps.size() == 0) {
    IR::ExprPtr c = std::make_unique<IR::ConstantExpression>(0);
    auto e = std::make_unique<IR::ExpressionStatement>(std::move(c));
    return std::make_unique<Nx>(std::move(e));
  } else if (exps.size() == 1) {
    return std::move(exps.front());
  } else {
    std::vector<IR::StmPtr> stms;
    auto last = std::move(exps.back());
    exps.pop_back();
    for (auto &exp : exps) {
      stms.push_back(unNx(std::move(exp)));
    }
    auto first = toIRSeqStm(std::move(stms));
    if (auto nx = llvm::dyn_cast_or_null<Nx>(last.get())) {
      auto seq = std::make_unique<IR::SequenceStatement>(std::move(first),
                                                         std::move(nx->irStm));
      return std::make_unique<Nx>(std::move(seq));
    } else {
      auto eseq = std::make_unique<IR::SequenceExpression>(
          std::move(first), unEx(std::move(last)));
      return std::make_unique<Ex>(std::move(eseq));
    }
  }
}

TExprPtr Translator::letexp(std::vector<TExprPtr> &&decls,
                            std::vector<TExprPtr> &&body) {
  auto ret = std::move(decls);
  for (auto &stm : body) {
    ret.push_back(std::move(stm));
  }
  return sequence(std::move(ret));
}
void Translator::prettyPrint(Expr *exp) { std::cout << exp->prettyPrint(); }
void Translator::procEntryExit(Level *lv, TExprPtr &&body) {
  if (auto lvl = llvm::dyn_cast_or_null<Lev>(lv)) {
    // create a frame and add a procedure fragment.
    if (auto nx = llvm::dyn_cast_or_null<Nx>(body.get())) {
      auto procFragment = std::make_unique<LCCFrame::Procedure>(
          std::move(nx->irStm), std::move(lvl->frame));
      fragments.push_back(std::move(procFragment));
      return;
    }
  } else if (auto lvl = llvm::dyn_cast_or_null<Top>(lv)) {
    std::cout << "in top level." << std::endl;
  }
  llvm_unreachable("");
}

} // namespace Trans
} // namespace Sema
} // namespace KT
