//===-- Symbol.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/SymbolTable/Symbol.h"

#include <sstream>

namespace KT {

Symbol::Symbol(std::string const &n, ID const &_id) : name{n}, id{_id} {}

Symbol::ID Symbol::nextID = 0;

std::string Symbol::prettyPrint() const {
  std::stringstream ss;
  ss << "<symbol>";
  ss << "<id>" << id << "<id/>";
  ss << "<name>" << name << "<name/>";
  ss << "<symbol>\n";
  return ss.str();
}

} // namespace KT
