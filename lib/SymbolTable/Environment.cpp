//===-- Environment.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "kt/SymbolTable/Environment.h"
#include "kt/SymbolTable/Symbol.h"
#include "kt/SymbolTable/SymbolTable.h"
#include "kt/SymbolTable/Types.h"
#include <memory>
#include <vector>

namespace KT {

namespace Env {

Environment::Environment() {}

void Environment::setBase(Environment &base) {
  auto intSym = Symbol::make("int");
  auto floatSym = Symbol::make("float");
  auto stringSym = Symbol::make("string");
  auto unitSym = Symbol::make("unit");
  auto nilSym = Symbol::make("nilSym");
  base.typeEnv.enter("int", std::make_unique<Sema::IntType>());
  base.typeEnv.enter("float", std::make_unique<Sema::FloatType>());
  base.typeEnv.enter("string", std::make_unique<Sema::StringType>());
  base.typeEnv.enter("unit", std::make_unique<Sema::UnitType>());
  base.typeEnv.enter("nil", std::make_unique<Sema::NilType>());

  auto name = Symbol::make("name");
  base.valueEnv.enter("name",
                      std::make_unique<Sema::ValueSymbolTable::VariableEntry>(
                          base.typeEnv.lookup("string")));

  base.valueEnv.enter(
      "printi",
      std::make_unique<Sema::ValueSymbolTable::FunctionEntry>(
          "printi", std::vector<Sema::Type *>{base.typeEnv.lookup("string")},
          base.typeEnv.lookup("uint")));
}

} // namespace Env

} // namespace KT
