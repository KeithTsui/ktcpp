#include "kt/CodeGen/LCCCodegen.h"
#include "kt/Frame/Frame.h"
#include "kt/IR/Canonical.h"
#include "kt/MIR/mir.h"
#include "kt/Parser/RDParser.h"
#include "kt/RegisterAllocation/RegisterAllocation.h"
#include "kt/Semantics/Semantics.h"
#include <cxxabi.h>   // for __cxa_demangle
#include <dlfcn.h>    // for dladdr
#include <execinfo.h> // for backtrace
#include <iostream>
#include <list>
#include <memory>
#include <signal.h>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>

void handler(int) {
  void *callstack[128];
  const int nMaxFrames = sizeof(callstack) / sizeof(callstack[0]);
  char buf[1024];
  int nFrames = backtrace(callstack, nMaxFrames);
  char **symbols = backtrace_symbols(callstack, nFrames);

  std::ostringstream trace_buf;

  for (int i = 1; i < nFrames; i++) {

    Dl_info info;

    if (dladdr(callstack[i], &info)) {
      char *demangled = NULL;
      int status;
      demangled = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);

      snprintf(buf, sizeof(buf), "%-3d %*p %s + %zd\n", i,
               (int)(2 + sizeof(void *) * 2), callstack[i],
               status == 0 ? demangled : info.dli_sname,
               (char *)callstack[i] - (char *)info.dli_saddr);
      free(demangled);
    } else {
      snprintf(buf, sizeof(buf), "%-3d %*p\n", i, int(2 + sizeof(void *) * 2),
               callstack[i]);
    }

    trace_buf << buf;
  }

  free(symbols);
  if (nFrames == nMaxFrames)
    trace_buf << "[truncated]\n";

  std::cerr << trace_buf.str();

  exit(1);
}

/**
 * Prints helpful information for the user if they try to run the
 * program with a bad number of arguments
 */
void printHelp() {
  std::cout << "\n"
            << "You have entered an incorrect number of arguments."
            << "\n"
            << "\n"
            << "Please specify the file you wish to parse and"
            << "\n"
            << "optionally whether you want to flag for debugging."
            << "\n"
            << "\n"
            << "> parser <filename> -d"
            << "\n";
}

/**
 * There are two arguments: one mandatory and one optional
 *
 * The mandatory argument is the file to perform a parse on while the
 * optional flag -d indicates that the user wants to print debug info.,
 * namely the sequences of Tokens as the Scanner reads them
 */
int main(int argc, char **argv) {
  signal(SIGSEGV, handler); // install our handler
  // The user has given us a bad number of args
  if (argc > 4 || argc < 1) {
    printHelp();
    return 0;
  }

  // Initialize the Parser with the given filename
  KT::RDParser parser(argv[1]);

  // Print debug info. if flagged
  if (argc > 2 && strcmp(argv[2], "-d") == 0)
    parser.printDebug = true;

  if (argc > 2 && strcmp(argv[2], "-d") != 0)
    std::cout << "\nPlease use \"-d\" as flag for debugging.\n";

  auto ts = parser.tokenize();
  auto ast = parser.parse();
  std::cout << "Got AST: " << std::endl;
  auto str = ast->prettyPrint();
  std::cout << str << std::endl;

  std::cout << "Type Checking ....\n";

  KT::Sema::Semantics sema{ast.get()};
  auto frags = sema.transProgram();

  std::list<KT::LCCFrame::String *> strFrags;
  std::list<KT::LCCFrame::Procedure *> procFrags;

  // partition frags into procedure group and string group.
  for (auto &frag : *frags) {
    if (auto strFrag =
            llvm::dyn_cast_or_null<KT::LCCFrame::String>(frag.get())) {
      strFrags.push_back(strFrag);
    } else if (auto procFrag = llvm::dyn_cast_or_null<KT::LCCFrame::Procedure>(
                   frag.get())) {
      procFrags.push_back(procFrag);
    } else {
      llvm_unreachable("fragment should be string or procedure.");
    }
  }

  // emit string to asm file for string fragment group.
  for (auto str : strFrags) {
    auto assm = str->toAsm();
    std::cout << assm << std::endl;
  }

  // emit procedure fragment to asm file for procedure fragment group.
  for (auto proc : procFrags) {
    auto IR = proc->body.get();
    auto frame = proc->frame.get();

    std::cout << proc->frame.get()->name.sym.name << std::endl;
    std::cout << IR->prettyPrint() << std::endl;

    auto linearizedIR = KT::IR::linearize(std::move(proc->body));
    for (auto &lIR : linearizedIR) {
      std::cout << "After Linearized: " << std::endl;
      std::cout << lIR->prettyPrint() << std::endl;
    }
    // auto bbs = KT::IR::toBasicBlocks(std::move(linearizedIR));

    // auto scheduledIR = KT::IR::traceScheduling(std::move(bbs));

    // for (auto &lIR : scheduledIR) {
    //   std::cout << "After Scheduled: " << std::endl;
    //   std::cout << lIR->prettyPrint() << std::endl;
    // }

    // do isel for each IR statement then concate those generated MIRs together
    // for this procedure

    std::cout << "Instruction Selection: " << std::endl;
    std::list<std::unique_ptr<KT::MIR::MachineInstruction>> mirss;
    KT::LCC::LCCCodeGen codegen;
    for (auto &stm : linearizedIR) {
      auto mirs = codegen.codegen(stm.get());
      for (auto &mir : mirs) {
        std::cout << mir->prettyPrint() << std::endl;
        mirss.push_back(std::move(mir));
      }
    }

    std::cout << "Register Allocation: " << std::endl;
    // // do register allocation.
    KT::RA::RegisterAllocator ra{std::move(mirss), frame};
    ra.regAlloc();
    auto regAllocatedMIR = &ra.program;

    // // add prologue and epilogue to regAllocMIR;
    for (std::unique_ptr<KT::MIR::MachineInstruction> &instr :
         *regAllocatedMIR) {
      std::cout << instr.get()->prettyPrint() << std::endl;
    }

    for (auto &mp : ra.allocation) {
      std::cout << mp.first.prettyPrint() << " : "
                << KT::LCCFrame::RegisterInfo::registerName(mp.second)
                << std::endl;
    }
  }

  return 0;
}
