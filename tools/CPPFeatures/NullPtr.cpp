#include <stdio.h>

//#define FORGET_SET

static void (*FP)() = 0;
static void impl() { printf("hello\n"); }
void set() { FP = impl; }
void call() { FP(); }
int main() {
// #ifndef FORGET_SET
//   set();
// #endif
  call();
}
