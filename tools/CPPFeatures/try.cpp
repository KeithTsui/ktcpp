//===-- try.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
// #include "llvm/ADT/Optional.h"
// #include "llvm/Support/Error.h"
// #include "llvm/Support/raw_ostream.h"
// #include <iostream>
// #include <memory>
// #include <ostream>
// #include <string>
// #include <system_error>

// llvm::Optional<std::unique_ptr<int>> getAbsInt(int i) {
//   if (i >= 0) {
//     return std::make_unique<int>(i);
//   } else {
//     return std::make_unique<int>(-i);
//   }
// }

// class MyError : public llvm::ErrorInfo<MyError> {
// public:
//   static char ID;
//   MyError() : Msg{"Hello world."} {}
//   void log(llvm::raw_ostream &OS) const override {}
//   std::error_code convertToErrorCode() const override {
//     return std::error_code();
//   }
//   const std::string &getMessage() const { return Msg; }
//   std::string Msg;
// };

// char MyError::ID;

// llvm::Error run() { return llvm::make_error<MyError>(); }

// int main(int, char **) {
//   if (auto absIntOp = getAbsInt(-10)) {
//     std::cout << *absIntOp.getValue() << std::endl;
//   }

//   if (auto e = run()) {
//     if (e.isA<MyError>()) {
//       std::cout << "is MyError\n";
//     }
//     auto newE = llvm::handleErrors(std::move(e), [](MyError const &me) {
//       std::cout << me.Msg << std::endl;
//     });
//   }

//   return 0;
// }

// #include <iostream>
// #include <memory>

// struct POD {
//   int a;
//   float b;
// };

// struct NewPOD : POD {
//   long c;
//   double d;
// };

// struct NotPOD {
//   int a;
//   float b;
//   NotPOD() : a{0}, b{0} {}
// };

// struct B {
//   B() { std::cout << "B()\n"; }
//   virtual ~B() = 0;
// };

// B::~B() {}

// struct A : B {

//   A(float) : A(0) { std::cout << "A(float)\n"; }

// private:
//   A(int) { std::cout << "A(int)\n"; }
// };

// class Date;
// void doSomethingOn(Date d);

// class GC {
// public:
//   int healthPoint() {
//     auto i = doHealthPoint();

//     std::cout << "GC: " << i << std::endl;
//     return i;
//   }
//   virtual ~GC();

// private:
//   virtual int doHealthPoint() const { return 10; }
// };

// GC::~GC() {}

// class Monster : public GC {

// public:
//   int doHealthPoint() const override { return 11; }

// public:
//   void sayHi() { auto i = doHealthPoint(); }
// };

// int main(int, char **) {

//   POD aPod;
//   std::cout << aPod.a << " " << aPod.b << std::endl;

//   POD const *cPod = new POD;
//   POD *dPod = const_cast<POD *>(cPod);
//   POD const *ePod = dPod;
//   auto zero = std::make_shared<int>(0);
//   auto wzero = std::weak_ptr<int>(zero);
//   {
//     auto szero = wzero.lock();
//     auto i = *szero.get();
//     std::cout << i << std::endl;
//   }

//   NotPOD nPod;
//   std::cout << nPod.a << " " << nPod.b << std::endl;

//   NewPOD naPod;
//   std::cout << naPod.a << " " << naPod.b << std::endl;

//   float f{1.0};
//   A a(f);

//   Monster m;
//   m.healthPoint();

//   GC *gc = &m;
//   gc->healthPoint();

//   return 0;
// }

#include <iostream>
#include <map>
#include <ostream>
#include <unordered_map>

class X {
public:
  X() : i{-1} {}

  X(int i_) : i{i_} {}

  friend std::ostream &operator<<(std::ostream &os, X const &x);

private:
  int i;
};

std::ostream &operator<<(std::ostream &os, X const &x) {
  os << x.i;
  return os;
}

int main() {
  std::unordered_map<int, X> m;
  std::cout << m[10] << std::endl;
  return 0;
}
