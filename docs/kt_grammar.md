```bash
# entry for Keith Tiger language
<tiger-program> -> <let-statement>
<declaration-list> -> <declaration> <declaration-list>
                    | NULL

# declaration
<declaration> -> <type-declaration>
               | <var-declaration>
               | <function-declaration>

# type-declaration
<type-declaration> -> type id = <type> ;
<type> -> <named-type>
        | <array-type>
        | <record-type>
<named-type> -> int | float | string | id
<arrary-type> -> array [ IntLiteral ] of <type> # array type
<record-type> -> { <name-type-pair-list> } # record type

#var-declaration
<var-declaration> -> var id : <type> <optional-initializer> ;
<optional-initializer> -> := <expression>
                        | NULL

#function-declaration
<function-declaration> -> function id ( <name-type-pair-list> ) : <type>
                          begin <statement-list> end ;


<name-type-pair-list> -> <name-type-pair> <name-type-pair-list-tail>
                       | NULL


<name-type-pair> -> id : <type>
<name-type-pair-list-tail> -> , <name-type-pair> <name-type-pair-list-tail>
                            | NULL


#statement-list
<statement-list> -> <statement> <statement-list>
                  | NULL


<statement> -> <if-statement>
            -> <while-statement>
            -> <for-statement>
            -> <break-statement>
            -> <let-statement>
            -> <call-or-assignment-statement>
            -> <return-statement>


<if-statement> -> if <expression> then <statement-list> <if-statement-tail>
<if-statement-tail> -> else <statement-list> endif ;
                     | endif ;

<while-statement> -> while <expression>
                     do <statement-list> enddo ;

<for-statement> -> for id := <expression> to <expression>
                   do <statement-list> enddo ;
                   
<break-statement> -> break ;

<let-statement> -> let <declaration-list> in <statement-list> end ;

<call-or-assignment-statement> -> <call-expression> ;
                                | <lvalue-expression> := <expression> ;
                                
<call-statement> -> <call-expression> ;
<call-expression> -> ID ( <expression-list> ) 
<assignment-statement> -> <lvalue-expression> := <expression> ;

<return-statement> -> return <expression> ;


<expression-list> -> <expression> <expression-list-tail>
                   | NULL
<expression-list-tail> -> , <expression> <expression-list-tail>
                        | NULL
                        
<expression> -> <or-expression>

<or-expression> -> <and-expression> 
                 | <and-expression> <or-op> <and-expression>

<and-expression> -> <compare-expression>
                  | <compare-expression> <and-op> <compare-expression>

<compare-expression> -> <term-expression>
                      | <term-expression> <cmp-op> <term-expression>
                      
<term-expression> -> <factor-expression>
                   | <factor-expression> <term-op> <factor-expression>
                   
<factor-expression> -> <atom-expression>
                     | <atom-expression> <factor-op> <atom-expression>
                     
<atom-expression> -> ( <expression> )
                   | <constant-expression>
                   | <lvalue-expression>
                   | <call-expression>
                   
<constant-expression> -> <int-expression>
                       | <float-expression>
                       | <string-expression>
                       
<int-expression> -> IntegerLiteral

<float-expression> -> FloatLiteral

<string-expression> -> StringLiteral

<lvalue-expression> -> id <lvalue-expression-tail>
<lvalue-expression-tail> -> [ <expression> ] <lvalue-expression-tail>
                          | . id <lvalue-expression-tail>
                          | NULL


### FIXME
missing Array expression for array type
missing Record expression for record type

```
