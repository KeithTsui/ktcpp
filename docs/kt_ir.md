```bash
Expr = CONST i
     | NAME label # for JUMP (NAME label)
     | TEMP t
     | BINOP Expr * op * Expr
     | MEM Expr
     | CALL Expr * Expr List
     | ESEQ Stm * Expr

Stm = MOVE Expr * Expr
    | EXP Expr
    | JUMP Expr
    | CJUMP rop * Expr * Expr * label * label
    | SEQ stm * stm
    | LABEL label
```
