```bash

ASTnode -> Expression
         | Declaration
         | Type

Expression -> NilExpression ()
            | IntExpression (int64_t) # 12
            | FloatExpression (long double) # 3.14
            | StringExpression (std::string) # "abc"
            | CallExpression (Symbol, [Expressions]) # Symbol(Expressions)
            | BinaryOperatorExpression (BinaryOperator, Expression, Expression) # Expression BinaryOperator Expression)
            | RecordExpression (Fields, Type) # var a := {a: 1, b: 2, c: "name"}
            | ArraryExpression (Type, Expression, Expression) # var a := array [10] of int
            | SequenceExpression (Expressions) # expr1; expr2; expr3;
            | Variable   # variable is expression that return the value of itself.
            | Statement  # statement is expression that is no return value.

            
Variable -> SimpleVariable (Symbol)  # symbol
          | FieldVariable (Variable, Symbol) # Variable.Symbol
          | SubscriptVariable (Variable, Expression) # Variable[Expression]
          | VariableExpression (Variable) # Variable for lvalue.



Statement -> AssignmentStatement(Variable, Expression) # Variable := Expression
           | IfStatement(Expression, Expression, Expression) #if(Expression) then Expression else Expression
           | WhileStatement(Expression, Expression) # while(Expression) Expression
           | ForStatement(Variable, Expression, Expression, Expression) # for(Var:Exp,Exp)Exp
           | BreakStatement()
           | LetStatement(Declarations, Expressions)
           | ReturnStatement()

Declaration -> FunctionDeclaration(Symbol, Fields, Symbol, Expression) # function f (a:int):int Exp
             | VariableDeclaration(Symbol, Symbol, Expression) # var a: int := 10;
             | TypeDeclaration(Type) # type A = B

Type -> NamedType (Symbol) # B
      | RecordType (Fields) # {a:int, b :int , c:string}
      | ArraryType (Symbol, Size) # arrary [10] of int

```
