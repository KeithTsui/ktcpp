%token 
let in end NULL type id array of int float var colon semicolon lbracket rbracket 
%%
tiger_program : let declaration_segment in stat_seq end;
declaration_segment : type_declaration_list var_declaration_list funct_declaration_list;
type_declaration_list : type_declaration type_declaration_list;
type_declaration_list : NULL
var_declaration_list :
