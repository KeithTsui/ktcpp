# Keith Tiger Compiler, Implemented in CPP (KTCPP)
A rewritten in CPP version of Keith Tiger Compiler, [KT](https://gitlab.com/KeithTsui/kt), which is implemented in SML originally.

Please, to see SML version [KT](https://gitlab.com/KeithTsui/kt) for more description about Keith Tiger Compiler.

This program is just to see the technical details about implementation a compiler in CPP with CMake and LLVM utilities.

If you want a LCC backend in LLVM, please check out [LCCInLLVMLite](https://gitlab.com/KeithTsui/lccinllvmlite), that is a LCC backend using LLVM infrastructure.

# Be careful

This CPP version is not well tested, with some bugs and lots of debug information, even though it can pass some small tests, please refer to SML version for further experiments on Tiger Luangeuage.
