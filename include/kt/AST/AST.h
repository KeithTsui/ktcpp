//===-- AST.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef AST_AST_H
#define AST_AST_H

#include "kt/SymbolTable/Symbol.h"
#include "llvm/Support/Casting.h"
#include <algorithm>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace KT {

namespace AST {

using TigerInt = int64_t;
using TigerFloat = long double;
using TigerString = std::string;

struct Expression;
struct Statement;
enum class BinaryOperator;
struct NamedArgument;
struct Declaration;
struct Type;

#define ASTCLASSOF(CLASSNAME)                                                     \
  static bool classof(ASTNode const *const n) {                                \
    return n->getKind() == AK_##CLASSNAME;                                     \
  }

#define ASTCLASSOFRANG(CLASSNAME)                                                 \
  static bool classof(ASTNode const *const n) {                                \
    return n->getKind() >= AK_##CLASSNAME &&                                   \
           n->getKind() < AK_##CLASSNAME##_End;                                \
  }

struct ASTNode {
public:
  int line = 0, column = 0;

  // LLVM-style RTTI begin
public:
  enum ASTNodeKind {
    AK_Variable,
    AK_SimpleVariable,
    AK_FieldVariable,
    AK_SubscriptVariable,
    AK_VariableExpression,
    AK_Variable_End,
    AK_Expression,
    AK_NilExpression,
    AK_IntExpression,
    AK_FloatExpression,
    AK_StringExpression,
    AK_CallExpression,
    AK_BinaryOperatorExpression,
    AK_RecordExpression,
    AK_SequenceExpression,
    AK_ArrayExpression,

    AK_Statement,

    AK_AssignmentStatement,
    AK_IfStatement,
    AK_WhileStatement,
    AK_ForStatement,
    AK_BreakStatement,
    AK_LetStatement,
    AK_ReturnStatement,

    AK_Statement_End,

    AK_Expression_End,

    AK_Declaration,
    AK_FunctionDeclaration,
    AK_VariableDeclaration,
    AK_TypeDeclaration,
    AK_Declaration_End,
    AK_Type,
    AK_NamedType,
    AK_RecordType,
    AK_ArrayType,
    AK_Type_End
  };

private:
  ASTNodeKind const Kind;

public:
  ASTNodeKind getKind() const { return Kind; }
  std::string getKindName() const {
    static std::vector<std::string> names{"AK_Variable",
                                          "AK_SimpleVariable",
                                          "AK_FieldVariable",
                                          "AK_SubscriptVariable",
                                          "AK_VariableExpression",
                                          "AK_Variable_End",
                                          "AK_Expression",
                                          "AK_NilExpression",
                                          "AK_IntExpression",
                                          "AK_FloatExpression",
                                          "AK_StringExpression",
                                          "AK_CallExpression",
                                          "AK_BinaryOperatorExpression",
                                          "AK_RecordExpression",
                                          "AK_SequenceExpression",
                                          "AK_ArrayExpression",

                                          "AK_Statement",

                                          "AK_AssignmentStatement",
                                          "AK_IfStatement",
                                          "AK_WhileStatement",
                                          "AK_ForStatement",
                                          "AK_BreakStatement",
                                          "AK_LetStatement",
                                          "AK_ReturnStatement",

                                          "AK_Statement_End",

                                          "AK_Expression_End",

                                          "AK_Declaration",
                                          "AK_FunctionDeclaration",
                                          "AK_VariableDeclaration",
                                          "AK_TypeDeclaration",
                                          "AK_Declaration_End",
                                          "AK_Type",
                                          "AK_NamedType",
                                          "AK_RecordType",
                                          "AK_ArrayType",
                                          "AK_Type_End"};
    return names[Kind];
  }

  ASTNode(ASTNodeKind k) : Kind{k} {}

  // LLVM-style RTTI end
public:
  virtual ~ASTNode() = 0;

  virtual std::string prettyPrint() const = 0;
};

struct Expression : public ASTNode {
public:
  Expression(ASTNodeKind k = AK_Expression) : ASTNode{k} {}

  ASTCLASSOFRANG(Expression)
};

struct Variable : public Expression {
public:
  Variable(ASTNodeKind k = AK_Variable) : Expression{k} {}

  ASTCLASSOFRANG(Variable)
};

// x
struct SimpleVariable : public Variable {
  Symbol const sym;

public:
  SimpleVariable(Symbol const &s) : Variable{AK_SimpleVariable}, sym{s} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(SimpleVariable)
};

// x.a
struct FieldVariable : public Variable {
  std::unique_ptr<Variable> var;// x
  Symbol const sym; // a

public:
  FieldVariable(std::unique_ptr<Variable> &&v, Symbol const &s)
      : Variable{AK_FieldVariable}, var{std::move(v)}, sym{s} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(FieldVariable)
};

// x[a + 2]
struct SubscriptVariable : public Variable {
  std::unique_ptr<Variable> var;
  std::unique_ptr<Expression> exp;

public:
  SubscriptVariable(std::unique_ptr<Variable> &&v,
                    std::unique_ptr<Expression> &&e)
      : Variable{AK_SubscriptVariable}, var{std::move(v)}, exp{std::move(e)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(SubscriptVariable)
};

// x
struct VariableExpression : public Variable {
  std::unique_ptr<Variable> var;

public:
  VariableExpression(std::unique_ptr<Variable> &&v)
      : Variable{AK_VariableExpression}, var{std::move(v)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(VariableExpression)
};

struct Statement : public Expression {
public:
  Statement(ASTNodeKind k = AK_Statement) : Expression{k} {}
  ASTCLASSOFRANG(Statement)
};

// nil
struct NilExpression : public Expression {
public:
  NilExpression() : Expression{AK_NilExpression} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(NilExpression)
};

// 1
struct IntExpression : public Expression {
  TigerInt const value;

public:
  IntExpression(TigerInt i) : Expression{AK_IntExpression}, value{i} {}
  std::string prettyPrint() const override;

  ASTCLASSOF(IntExpression)
};

// 1.0
struct FloatExpression : public Expression {
  TigerFloat const value;

public:
  FloatExpression(TigerFloat f) : Expression{AK_FloatExpression}, value{f} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(FloatExpression)
};

// "abc"
struct StringExpression : public Expression {
  TigerString const value;

public:
  StringExpression(TigerString s) : Expression{AK_StringExpression}, value{s} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(StringExpression)
};

// f(arg0, arg1, ...)  id() or id(exp{, exp})
struct CallExpression : public Statement {
  Symbol const func;
  std::vector<std::unique_ptr<Expression>> const args;

public:
  CallExpression(Symbol const &f, std::vector<std::unique_ptr<Expression>> &&as)
      : Statement{AK_CallExpression}, func{f}, args{std::move(as)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(CallExpression)
};

// exp0 op exp1
struct BinaryOperatorExpression : public Expression {
  std::unique_ptr<Expression> lhs;
  std::unique_ptr<Expression> rhs;
  BinaryOperator const opr;

public:
  BinaryOperatorExpression(std::unique_ptr<Expression> &&l,
                           std::unique_ptr<Expression> &&r,
                           BinaryOperator const &op)
      : Expression{AK_BinaryOperatorExpression}, lhs{std::move(l)},
        rhs{std::move(r)}, opr{op} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(BinaryOperatorExpression)
};

enum class BinaryOperator {
  Plus,
  Minus,
  Times,
  Divide,
  Equal,
  NotEqual,
  Less,
  LessOrEqual,
  Greater,
  GreaterOrEqual,
  And,
  Or
};

extern std::map<BinaryOperator, std::string> binOpNames;

// a: 1
struct NamedArgument {
  Symbol const sym;
  std::unique_ptr<Expression> exp;
  std::string prettyPrint() const;
};

// {a: 1, b: 2, c: 1 + 2, d: "abc"}
struct RecordExpression : public Expression {
  std::vector<NamedArgument> const arguments;
  Symbol const type;

public:
  RecordExpression(std::vector<NamedArgument> args, Symbol const &ty)
      : Expression{AK_RecordExpression}, arguments{std::move(args)}, type{ty} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(RecordExpression)
};

// (exp1; exp2; exp3)
struct SequenceExpression : public Expression {
  std::vector<std::unique_ptr<Expression>> const exps;

public:
  SequenceExpression(std::vector<std::unique_ptr<Expression>> &es)
      : Expression{AK_SequenceExpression}, exps{std::move(es)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(SequenceExpression)
};

// x := 1
struct AssignmentStatement : public Statement {
  std::unique_ptr<Variable> var;
  std::unique_ptr<Expression> exp;

public:
  AssignmentStatement(std::unique_ptr<Variable> &&v,
                      std::unique_ptr<Expression> &&e)
      : Statement{AK_AssignmentStatement}, var{std::move(v)}, exp{std::move(
                                                                  e)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(AssignmentStatement)
};

// if x then y  else z
struct IfStatement : public Statement {
  std::unique_ptr<Expression> condition;
  std::vector<std::unique_ptr<Statement>> thenBranch;
  std::vector<std::unique_ptr<Statement>> elseBranch;

public:
  IfStatement(std::unique_ptr<Expression> &&c,
              std::vector<std::unique_ptr<Statement>> &&t,
              std::vector<std::unique_ptr<Statement>> &&e)
      : Statement{AK_IfStatement}, condition{std::move(c)},
        thenBranch{std::move(t)}, elseBranch{std::move(e)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(IfStatement)
};

// while exp1 do exp2
struct WhileStatement : public Statement {
  std::unique_ptr<Expression> condition;
  std::vector<std::unique_ptr<Statement>> body;

public:
  WhileStatement(std::unique_ptr<Expression> &&c,
                 std::vector<std::unique_ptr<Statement>> &&b)
      : Statement{AK_WhileStatement}, condition{std::move(c)}, body{std::move(
                                                                   b)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(WhileStatement)
};

// for id := exp1 to exp2 do exp3
struct ForStatement : public Statement {
  bool isEscape;
  Symbol const var;
  std::unique_ptr<Expression> lowerBound;
  std::unique_ptr<Expression> higherBound;
  std::vector<std::unique_ptr<Statement>> body;

public:
  ForStatement(Symbol v, std::unique_ptr<Expression> &&l,
               std::unique_ptr<Expression> &&h,
               std::vector<std::unique_ptr<Statement>> &&b)
      : Statement{AK_ForStatement}, var{v}, lowerBound{std::move(l)},
        higherBound{std::move(h)}, body{std::move(b)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(ForStatement)
};

// break
struct BreakStatement : public Statement {
public:
  BreakStatement() : Statement{AK_BreakStatement} {}

  std::string prettyPrint() const override;
  ASTCLASSOF(BreakStatement)
};

// return
struct ReturnStatement : public Statement {
  std::unique_ptr<Expression> expr;

public:
  ReturnStatement(std::unique_ptr<Expression> &&e)
      : Statement{AK_ReturnStatement}, expr{std::move(e)} {}

  std::string prettyPrint() const override;
  ASTCLASSOF(ReturnStatement)
};

// let decs in expseq end
struct LetStatement : public Statement {
  std::vector<std::unique_ptr<Declaration>> const declarations;
  std::vector<std::unique_ptr<Statement>> const body;

public:
  LetStatement(std::vector<std::unique_ptr<Declaration>> &&decls,
               std::vector<std::unique_ptr<Statement>> &&b)
      : Statement{AK_LetStatement},
        declarations{std::move(decls)}, body{std::move(b)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(LetStatement)
};

// type-id [exp1] of exp2
struct ArrayExpression : public Expression {
  Type *type;
  std::unique_ptr<Expression> size;
  std::unique_ptr<Expression> initializer;

public:
  ArrayExpression(Type *t, std::unique_ptr<Expression> &&s,
                  std::unique_ptr<Expression> &&i)
      : Expression{AK_ArrayExpression}, type{t}, size{std::move(s)},
        initializer{std::move(i)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(ArrayExpression)
};

struct Declaration : public ASTNode {
public:
  Declaration(ASTNodeKind k = AK_Declaration) : ASTNode{k} {}

  ASTCLASSOFRANG(Declaration)
};

struct Parameter {
  Symbol const name;
  std::unique_ptr<Type> type;
  bool const isEscape;

  Parameter(Symbol const n, std::unique_ptr<Type> &&t, bool escape = false)
      : name{n}, type{std::move(t)}, isEscape{escape} {}

  std::string prettyPrint() const;
};

struct RecordField {
  Symbol const name;
  std::unique_ptr<Type> type;

  RecordField(Symbol n, std::unique_ptr<Type> &&t)
      : name{n}, type{std::move(t)} {}
  std::string prettyPrint() const;
};

// function id (parameters): resultType = exp;
struct FunctionDeclaration : public Declaration {
  Symbol const name;
  std::vector<Parameter> const parameters;
  std::unique_ptr<Type> resultType;
  std::vector<std::unique_ptr<Statement>> body;

public:
  FunctionDeclaration(Symbol const &n, std::vector<Parameter> &&ps,
                      std::unique_ptr<Type> &&r,
                      std::vector<std::unique_ptr<Statement>> &&b)
      : Declaration{AK_FunctionDeclaration}, name{n}, parameters{std::move(ps)},
        resultType{std::move(r)}, body{std::move(b)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(FunctionDeclaration)
};

// var id: type := exp
struct VariableDeclaration : public Declaration {
  Symbol const name;
  bool const isEscape;
  std::unique_ptr<Type> type;
  std::unique_ptr<Expression> initializer;

public:
  VariableDeclaration(Symbol const &n, bool e, std::unique_ptr<Type> &&t,
                      std::unique_ptr<Expression> &&i)
      : Declaration{AK_VariableDeclaration}, name{n}, isEscape{e},
        type{std::move(t)}, initializer{std::move(i)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(VariableDeclaration)
};

struct Type;

// type name = type-id | {type_fields} | array of type-id
// type_field =  id: type-id
struct TypeDeclaration : public Declaration {
  Symbol const name;
  std::unique_ptr<Type> type;

public:
  TypeDeclaration(Symbol const &n, std::unique_ptr<Type> &&t)
      : Declaration{AK_TypeDeclaration}, name{n}, type{std::move(t)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(TypeDeclaration)
};

struct Type : public ASTNode {
public:
  Type(ASTNodeKind k = AK_Type) : ASTNode{k} {}
  ASTCLASSOFRANG(Type)
};

// type myInt = int

struct NamedType : public Type {
  Symbol const name;

public:
  NamedType(Symbol const &n) : Type{AK_NamedType}, name{n} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(NamedType)
};

// {a: int, b: string }
struct RecordType : public Type {
  std::vector<RecordField> const fields;

public:
  RecordType(std::vector<RecordField> &fs)
      : Type{AK_RecordType}, fields{std::move(fs)} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(RecordType)
};

// array [10] of type-id
struct ArraryType : public Type {
  std::unique_ptr<Type> type;
  size_t size;

public:
  ArraryType(std::unique_ptr<Type> &&ty, size_t s)
      : Type{AK_NamedType}, type{std::move(ty)}, size{s} {}

  std::string prettyPrint() const override;

  ASTCLASSOF(ArrayType)
};

#undef ASTCLASSOF
#undef ASTCLASSOFRANG

using TigerProgram = AST::LetStatement;

} // namespace AST

} // namespace KT

#endif /* AST_AST_H */
