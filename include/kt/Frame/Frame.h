//===-- Frame.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef FRAME_FRAME_H
#define FRAME_FRAME_H

#include "kt/IR/IR.h"
#include "kt/Support/LLVMRTTI.h"
#include "kt/Temporary/Temporary.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/Error.h"
#include <cstdlib>
#include <list>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace KT {

namespace LCCFrame {
constexpr int wordSize = 1;

struct Access {
public:
  enum AccessKind { AK_InFrame, AK_InReg };

private:
  AccessKind const Kind;

public:
  auto getKind() const { return Kind; }

  Access(AccessKind k) : Kind{k} {}

public:
  virtual ~Access() = 0;
};

#define ACCESSCLASSOF(CLASSNAME) CLASSOF(CLASSNAME, Access, AK)

struct InFrame : Access {
  int offset;

  InFrame(int n) : Access{AK_InFrame}, offset{n} {}

  ACCESSCLASSOF(InFrame);
};
struct InReg : Access {
  Temporary reg;
  InReg(Temporary r) : Access{AK_InReg}, reg{r} {}

  ACCESSCLASSOF(InReg);
};

#undef ACCESSCLASSOF

struct Frame {
  Label name;
  std::list<std::unique_ptr<Access>> formals;
  // top of frame stacks
  int locals;
  // IR instructions for moving incoming InReg args into Stack
  // Nil for LCC, because all arguments are passed in stack
  // std::vector<std::unique_ptr<IR::Statement>> instrs;

  Frame(Label const n, std::vector<bool> const &fs);
  Frame(Frame const &f) = delete;
  Frame(Frame &&f);

  std::unique_ptr<Access> allocLocal() {
    return std::make_unique<InFrame>(-(++locals * wordSize));
  }

  Access *allocLocal(bool inReg) {
    std::unique_ptr<Access> access;
    if (inReg) {
      access = std::make_unique<InReg>(Temporary());
    } else {
      access = std::make_unique<InFrame>(-(++locals * wordSize));
    }
    formals.push_back(std::move(access));
    return formals.back().get();
  }
};

struct Fragment {
  LLVMRTTIENUM2(Fragment, FK, Procedure, String)
  LLVMRTTIPURE(Fragment)
};
struct Procedure : Fragment {
  std::unique_ptr<IR::Statement> body;
  std::unique_ptr<Frame> frame;

  Procedure(std::unique_ptr<IR::Statement> &&b, std::unique_ptr<Frame> &&f)
      : Fragment{FK_Procedure}, body{std::move(b)}, frame{std::move(f)} {}

  CLASSOF(Procedure, Fragment, FK);
};

struct String : Fragment {
  Label name;
  std::string content;

  String(std::string const c) : Fragment{FK_String}, name{}, content{c} {}
  String(std::string const n, std::string const c)
      : Fragment{FK_String}, name{n}, content{c} {}

  CLASSOF(String, Fragment, FK);

  std::string toAsm() {
    return (name.sym.name + ": .string\"" + content + "\"\n");
  }
};

struct RegisterInfo {
  static Register r1;
  static Register r2;
  static Register r3;
  static Register r4;

  static Register FP;
  static Register SP;
  static Register RA;
  static Register RV;

  static std::vector<Register *> specialArgs;
  static std::vector<Register *> argRegs;
  static std::vector<Register *> calleeSaves;
  static std::vector<Register *> callerSaves;
  static std::map<std::string, Register *> regList;
  static std::map<Register *, std::string> registerToName;
  static std::vector<std::string> registers;

  static std::string registerName(Register *r);
  static bool isRegister(Temporary t);
  static std::string registerName(Temporary t);
};

IR::ExprPtr toIRExpr(Access *frameAccess, IR::ExprPtr &&temp);

} // namespace LCCFrame

} // namespace KT

#endif /* FRAME_FRAME_H */
