//===-- Translate.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef TRANSLATE_TRANSLATE_H
#define TRANSLATE_TRANSLATE_H

#include "kt/AST/AST.h"
#include "kt/Frame/Frame.h"
#include "kt/IR/IR.h"
#include "kt/Support/LLVMRTTI.h"
#include "kt/Temporary/Temporary.h"
#include "llvm/Support/Casting.h"
#include <functional>
#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace KT {

namespace Sema {

namespace Trans {

struct Level {
  Level *parent;
  std::unique_ptr<Level> child;

public:
  enum LevelKind { LK_Top, LK_Lev };

private:
  LevelKind const Kind;

public:
  LevelKind getKind() const { return Kind; }

  Level(LevelKind k) : parent{nullptr}, child{nullptr}, Kind{k} {}
  Level(LevelKind k, Level *p) : parent{p}, Kind{k} {}

  virtual ~Level() = 0;
};

#define LEVELCLASSOF(CLASSNAME) CLASSOF(CLASSNAME, Level, LK)
struct Top : Level {
  Top() : Level{LK_Top} {}
  LEVELCLASSOF(Top);
};

struct Lev : Level {
  std::unique_ptr<LCCFrame::Frame> frame;
  Lev(Level *p, std::string const &n, std::vector<bool> const &fs)
      : Level{LK_Lev, p}, frame{std::make_unique<LCCFrame::Frame>(Label(n),
                                                                  fs)} {}
  LEVELCLASSOF(Lev);
};
#undef LEVELCLASSOF

struct Access {
  Level *level;
  std::unique_ptr<LCCFrame::Access> frameAccess;

  Access(Level *l, std::unique_ptr<LCCFrame::Access> &&f)
      : level{l}, frameAccess{std::move(f)} {}
};

struct Expr {
  LLVMRTTIENUM3(Expr, EK, Ex, Nx, Cx);
  LLVMRTTIPURE(Expr);

public:
  virtual std::string prettyPrint() const {
    std::stringstream ss;
    ss << "<TransExpr></TransExpr>";
    return ss.str();
  };
};

#define EXPRCLASSOF(CLASSNAME) CLASSOF(CLASSNAME, Expr, EK)

struct Ex : Expr {
  IR::ExprPtr irExp;
  Ex(IR::ExprPtr &&e) : Expr{EK_Ex}, irExp{std::move(e)} {}
  Ex(int n) : Expr{EK_Ex}, irExp{std::make_unique<IR::ConstantExpression>(n)} {}

  EXPRCLASSOF(Ex);

public:
  std::string prettyPrint() const override;
};

struct Nx : Expr {
  IR::StmPtr irStm;
  Nx(IR::StmPtr &&e) : Expr{EK_Nx}, irStm{std::move(e)} {}

  EXPRCLASSOF(Nx);

public:
  std::string prettyPrint() const override;
};

struct Cx : Expr {
  std::function<IR::StmPtr(Label, Label)> conditionJumpStmGen;
  Cx(std::function<IR::StmPtr(Label, Label)> const &f)
      : Expr{EK_Cx}, conditionJumpStmGen{f} {}

  EXPRCLASSOF(Cx);

public:
  std::string prettyPrint() const override;
};

#undef EXPRCLASSOF

IR::ExprPtr externalCall(std::string const func,
                         std::vector<IR::ExprPtr> &&arguments);
IR::StmPtr toIRSeqStm(std::list<IR::StmPtr> &&stms);
IR::StmPtr toIRSeqStm(std::vector<IR::StmPtr> &&stms);

using TExprPtr = std::unique_ptr<Expr>;
IR::ExprPtr unEx(TExprPtr &&exp);
IR::StmPtr unNx(TExprPtr &&exp);
std::function<IR::StmPtr(Label, Label)> unCx(TExprPtr &&exp);
std::unique_ptr<Ex> errExp();
std::unique_ptr<Ex> nilExp();

TExprPtr binop(AST::BinaryOperator op, std::unique_ptr<Expr> &&lhs,
               std::unique_ptr<Expr> &&rhs);

TExprPtr relOp(AST::BinaryOperator op, std::unique_ptr<Expr> &&lhs,
               std::unique_ptr<Expr> &&rhs);

std::unique_ptr<IR::MemoryExpression> memPlus(IR::ExprPtr e1, IR::ExprPtr e2);

struct Translator {
  Top outermost;
  std::list<std::unique_ptr<LCCFrame::Fragment>> fragments;

  void reset() { fragments.clear(); }
  Level *currentLevel();
  void enterFunction(std::string const &name, std::vector<bool> const &formals);
  void exitFunction();
  std::unique_ptr<Access> allocLocal();

  std::unique_ptr<Ex> intLiteralsToEx(int i);
  std::unique_ptr<Ex> stringLiteralsToEx(std::string const &str);
  TExprPtr simpleVar(Access *acc, Level *level);
  TExprPtr subscriptVar(TExprPtr &&base, TExprPtr &&offset);
  TExprPtr fieldVar(TExprPtr &&base, std::string const &id,
                    std::vector<std::string> const &fieldList);
  TExprPtr ifelse(TExprPtr &&test, TExprPtr &&then, TExprPtr &&else_);
  TExprPtr record(std::vector<TExprPtr> &&fields);
  TExprPtr array(TExprPtr &&size, TExprPtr &&init);
  TExprPtr assign(TExprPtr &&lvalue, TExprPtr &&rvalue);
  TExprPtr loop(TExprPtr &&test, TExprPtr &&body, Label done);
  TExprPtr break_(Label b);
  TExprPtr call(Level *use, Level *def, Label lab, std::vector<TExprPtr> &&exps,
                bool isProcedure);
  TExprPtr sequence(std::vector<TExprPtr> &&exps);
  TExprPtr letexp(std::vector<TExprPtr> &&decls, std::vector<TExprPtr> &&body);
  void prettyPrint(Expr *exp);
  void procEntryExit(Level *lv, TExprPtr &&body);
};

} // namespace Trans

} // namespace Sema

} // namespace KT

#endif /* TRANSLATE_TRANSLATE_H */
