//===-- Record.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SYMBOLTABLE_RECORD_H
#define SYMBOLTABLE_RECORD_H

#include <memory>
#include <string>
#include <vector>

class Record;

typedef std::shared_ptr<Record> RecordPtr;

class Record {
public:
  int scopeLevel = 0;
  std::string type = ""; // for symbol -> Type
  int dimension; // for array dimension
  std::vector<std::string> parameters; // for function type
  std::vector<std::string> parameterTypes; // for function type
  std::vector<int> parameterDimensions; // for function
  std::string returnType = ""; // for function

  Record(int scopeLevel) { this->scopeLevel = scopeLevel; }

  /// get type
  std::string getType() const { return type; }
  /// get dimentsion
  int getDimension() const { return dimension; }
  /// get return type
  std::string getReturnType() const { return returnType; }
  /// get parameter types
  const std::vector<std::string> &getParameterTypes() const {
    return parameterTypes;
  }
  /// get parameter dimensions
  const std::vector<int> &getParameterDimensions() const {
    return parameterDimensions;
  }
  /// get paramenters
  const std::vector<std::string> &getParameters() const { return parameters; }
};
#endif /* SYMBOLTABLE_RECORD_H */
