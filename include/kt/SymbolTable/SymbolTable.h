//===-- SymbolTable.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SYMBOLTABLE_SYMBOLTABLE_H
#define SYMBOLTABLE_SYMBOLTABLE_H

#include <map>
#include <memory>
#include <vector>

#include "kt/SymbolTable/Record.h"
#include "kt/SymbolTable/Symbol.h"
#include "kt/SymbolTable/SymbolTableEntry.h"
#include "kt/SymbolTable/TokenPair.h"
#include "kt/SymbolTable/Types.h"
#include "kt/Token/TokenType.h"
#include "kt/Translate/Translate.h"

namespace KT {
namespace Sema {

// Symbol table for types
class TypeSymbolTable final {
  std::map<std::string, std::unique_ptr<Type>> bindings;

public:
  void enter(std::string const &s, std::unique_ptr<Type> &&t) {
    bindings[s] = std::move(t);
  }
  Type *lookup(std::string const &s) {
    if (bindings.count(s)) {
      return bindings[s].get();
    } else {
      return nullptr;
    }
  }
};

// Symbol table for values: variable and functions
class ValueSymbolTable final {

public:
  struct Entry {
    // LLVM-style RTTI begin
  public:
    enum EntryKind { EK_VariableEntry, EK_FunctionEntry };

  private:
    EntryKind const Kind;

  public:
    EntryKind getKind() const { return Kind; }

    Entry(EntryKind k) : Kind{k} {}

    // LLVM-style RTTI end

  public:
    virtual ~Entry() {}
  };
#define STCLASSOF(CLASSNAME)                                                     \
  static bool classof(Entry const *const n) {                                  \
    return n->getKind() == EK_##CLASSNAME;                                     \
  }

  struct VariableEntry : Entry {
    Type *type;
    std::unique_ptr<Trans::Access> access;

    VariableEntry(Type *t) : Entry{EK_VariableEntry}, type{t} {}
    VariableEntry(std::unique_ptr<Trans::Access> acc, Type *t)
        : Entry{EK_VariableEntry}, type{t}, access{std::move(acc)} {}

    STCLASSOF(VariableEntry)
  };

  struct FunctionEntry : Entry {
    std::string name;
    std::vector<Type *> parameterTypes;
    Type *returnType;

    FunctionEntry(std::string const &n, std::vector<Type *> &&pts, Type *rt)
        : Entry{EK_FunctionEntry}, name{n}, parameterTypes{std::move(pts)},
          returnType{rt} {}

    STCLASSOF(FunctionEntry)
  };

#undef STCLASSOF

private:
  std::map<std::string, std::unique_ptr<Entry>> bindings;

public:
  void enter(std::string const &s, std::unique_ptr<Entry> &&t) {
    bindings[s] = std::move(t);
  }

  Entry *lookup(std::string const &s) {
    if (bindings.count(s)) {
      return bindings[s].get();
    } else {
      return nullptr;
    }
  }
};

} // namespace Sema

} // namespace KT

#endif /* SYMBOLTABLE_SYMBOLTABLE_H */
