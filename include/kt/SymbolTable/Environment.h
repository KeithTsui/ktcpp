//===-- Environment.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SYMBOLTABLE_ENVIRONMENT_H
#define SYMBOLTABLE_ENVIRONMENT_H

#include "kt/SymbolTable/SymbolTable.h"
#include "kt/Support/macro.h"
#include <memory>

namespace KT {

namespace Env {

struct Environment {
  static void setBase(Environment&);
  Sema::TypeSymbolTable typeEnv;
  Sema::ValueSymbolTable valueEnv;
  Environment();
  //DISABLE_COPY_ONLY(Environment)
};


} // namespace Env

} // namespace KT

#endif /* SYMBOLTABLE_ENVIRONMENT_H */
