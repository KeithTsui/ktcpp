//===-- SymbolTablePair.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
//===-- SymbolTableEntry.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SYMBOLTABLE_SYMBOLTABLEENTRY_H
#define SYMBOLTABLE_SYMBOLTABLEENTRY_H

#include "kt/Token/TokenType.h"
#include <string>
#include <utility>
namespace KT {

enum EntryType {Variables, Types, Functions};

using SymbolTableEntry = std::pair<EntryType, std::string>;

} // namespace KT
#endif /* SYMBOLTABLE_SYMBOLTABLEENTRY_H */
