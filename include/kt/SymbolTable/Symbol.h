//===-- Symbol.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SYMBOLTABLE_SYMBOL_H
#define SYMBOLTABLE_SYMBOL_H

#include <sstream>
#include <string>

namespace KT {

/**
   Symbol is a pair of id number and name.
 */
struct Symbol {
  using ID = unsigned int;

private:
  static ID nextID;

public:
  std::string name;
  ID id;

  Symbol(std::string const &n, ID const &_id);

  static Symbol make(std::string const &name = "") {
    auto thisId = nextID++;
    auto thisName = name;
    if (thisName.empty()) {
      std::stringstream ss;
      ss << "SymID" << thisId;
      thisName = ss.str();
    }
    return Symbol(thisName, thisId);
  }

  friend bool operator<(Symbol const &lhs, Symbol const &rhs) {
    return lhs.id < rhs.id;
  }

  std::string prettyPrint() const;
};

} // namespace KT

#endif /* SYMBOLTABLE_SYMBOL_H */
