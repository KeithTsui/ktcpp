//===-- SymbolTerminalPair.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

//===-- TokenPair.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SYMBOLTABLE_TOKENPAIR_H
#define SYMBOLTABLE_TOKENPAIR_H

#include "kt/Token/TokenType.h"
#include <map>
#include <utility>

namespace KT {
using TokenPair = std::pair<TokenType, TokenType>;

} // namespace KT

namespace std {
template <> struct less<KT::TokenPair> {
  bool operator()(const KT::TokenPair &lhs, const KT::TokenPair &rhs) const {
    return (lhs.first < rhs.first) ||
           (lhs.first == rhs.first && lhs.second < rhs.second);
  }
};
} // namespace std

#endif /* SYMBOLTABLE_TOKENPAIR_H */
