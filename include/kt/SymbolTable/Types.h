//===-- Types.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SEMANTICS_TYPES_H
#define SEMANTICS_TYPES_H

#include "kt/SymbolTable/Symbol.h"
#include <cstddef>
#include <memory>
#include <vector>

namespace KT {

namespace Sema {

#define LLRTTI(CLASSNAME)                                                      \
  struct CLASSNAME : Type {                                                    \
    CLASSNAME() : Type{TK_##CLASSNAME} {}                                      \
    static bool classof(Type const *const n) {                                 \
      return n->getKind() == TK_##CLASSNAME;                                   \
    }                                                                          \
  }

#define TYCLASSOF(CLASSNAME)                                                     \
  static bool classof(Type const *const n) {                                   \
    return n->getKind() == TK_##CLASSNAME;                                     \
  }

struct Type {

  // LLVM-style RTTI begin
public:
  enum TypeKind {
    TK_IntType,
    TK_FloatType,
    TK_StringType,
    TK_RecordType,
    TK_ArrayType,
    TK_NilType,
    TK_UnitType,
    TK_NameType,
    TK_AliasType,
  };

private:
  TypeKind const Kind;

public:
  TypeKind getKind() const { return Kind; }

  Type(TypeKind k) : Kind{k} {}

  // LLVM-style RTTI end

public:
  virtual ~Type() {}

  friend bool operator==(Type const &lhs, Type const &rhs) {
    return lhs.Kind == rhs.Kind;
  }
  friend bool operator!=(Type const &lhs, Type const &rhs) {
    return !(lhs == rhs);
  }
};

LLRTTI(IntType);
LLRTTI(FloatType);
LLRTTI(StringType);
LLRTTI(NilType);
LLRTTI(UnitType);

struct NameType : Type {
  std::string name;

  NameType(std::string const &n) : Type{TK_NameType}, name{n} {}

  TYCLASSOF(NameType);
};

struct AliasType : Type {
  Type *refType;

  AliasType(Type *ty) : Type{TK_AliasType}, refType{ty} {}

  TYCLASSOF(AliasType);
};

struct RecordType : Type {
  struct Field {
    Symbol sym;
    Type *type;
    Field(Symbol &s, Type *t) : sym{s}, type{t} {}
  };
  std::vector<Field> fields;

  RecordType(std::vector<Field> &&fs)
      : Type{TK_RecordType}, fields{std::move(fs)} {}

  TYCLASSOF(RecordType)
};

struct ArrayType : Type {
  Type *type;
  size_t size;
  ArrayType(Type *t, size_t s) : Type{TK_ArrayType}, type{t}, size{s} {}

  TYCLASSOF(ArrayType)
};

#undef TYCLASSOF
#undef LLRTTI

} // namespace Sema

} // namespace KT

#endif /* SEMANTICS_TYPES_H */
