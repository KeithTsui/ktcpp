//===-- Semantics.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SEMANTICS_SEMANTICS_H
#define SEMANTICS_SEMANTICS_H

#include "kt/AST/AST.h"
#include "kt/SymbolTable/Environment.h"
#include "kt/SymbolTable/SymbolTable.h"
#include "kt/SymbolTable/Types.h"
#include "kt/Temporary/Temporary.h"
#include "kt/Translate/Translate.h"
#include <list>
#include <memory>
#include <vector>

namespace KT {

namespace Sema {

class Semantics final {
  std::list<Env::Environment> env_list;
  AST::TigerProgram *program;
  Trans::Translator translator;
  std::unique_ptr<Label> breakLabel;

public:
  struct TransResult {
    std::unique_ptr<Trans::Expr> expr;
    Type *type;

    TransResult(Type *t) : type{t} {};
    TransResult(std::unique_ptr<Trans::Expr> &&e, Type *t)
        : expr{std::move(e)}, type{t} {}
  };

  Semantics(AST::TigerProgram *p);

  std::list<std::unique_ptr<LCCFrame::Fragment>> *transProgram();

private:
  Env::Environment &getCurrnetScopeEnv() { return env_list.back(); }

  void enterScope() { env_list.emplace_back(Env::Environment()); }

  void exitScope() { env_list.pop_back(); }

  Type *lookupType(std::string name);

  ValueSymbolTable::Entry *lookupValue(std::string name);

  void insertType(std::string name, std::unique_ptr<Type> &&ty);

  void insertVariable(std::string name, Type *ty);

  void insertVariable(std::string name, Type *ty,
                      std::unique_ptr<Trans::Access> &&acc);

  void insertFunction(std::string name, std::vector<Type *> &&paraTypes,
                      Type *retType);

  TransResult transExp(AST::Expression *exp);
  TransResult transExpNil(AST::NilExpression *exp);
  TransResult transExpInt(AST::IntExpression *exp);
  TransResult transExpFloat(AST::FloatExpression *exp);
  TransResult transExpString(AST::StringExpression *exp);
  TransResult transExpCall(AST::CallExpression *callExp);
  TransResult transExpOp(AST::BinaryOperatorExpression *exp);
  TransResult transExpRecord(AST::RecordExpression *exp);
  TransResult transExpArray(AST::ArrayExpression *exp);
  TransResult transExpSeq(AST::SequenceExpression *exp);

  TransResult transVar(AST::Variable *var);
  TransResult transVarSimple(AST::SimpleVariable *var);
  TransResult transVarField(AST::FieldVariable *fieldvar);
  TransResult transVarSubscript(AST::SubscriptVariable *subVar);
  TransResult transExpVar(AST::VariableExpression *exp);

  TransResult transExpAssign(AST::AssignmentStatement *assignStm);
  TransResult transExpIf(AST::IfStatement *exp);
  TransResult transExpWhile(AST::WhileStatement *exp);
  TransResult transExpFor(AST::ForStatement *forStatement);
  TransResult transExpBreak(AST::BreakStatement *exp);
  TransResult transExpLet(AST::LetStatement *exp);
  TransResult transExpReturn(AST::ReturnStatement *exp);

  // update environment (symbol tables)
  void transDecl(AST::Declaration *decl);
  void transDeclVar(AST::VariableDeclaration *var);
  void transDeclFunc(AST::FunctionDeclaration *func);
  void transDeclType(AST::TypeDeclaration *type);

  Type* transType(AST::Type *astType);

  std::unique_ptr<Type> makeTypeFrom(AST::Type* astType);
};

} // namespace Sema

} // namespace KT

#endif /* SEMANTICS_SEMANTICS_H */
