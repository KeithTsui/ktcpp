//===-- Temporary.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef TEMPORARY_TEMPORARY_H
#define TEMPORARY_TEMPORARY_H

#include "kt/SymbolTable/Symbol.h"
#include <sstream>
#include <string>

namespace KT {

class Temporary {
public:
  using TemporaryID = unsigned;

private:
  static TemporaryID nextID;

protected:
  TemporaryID id;

public:
  Temporary() : id{nextID++} {}

public:
  std::string prettyPrint() const {
    std::stringstream ss;
    ss << "t" << id;
    return ss.str();
  }

  friend bool operator!=(Temporary const &lhs, Temporary const &rhs) {
    return lhs.id != rhs.id;
  }

  friend bool operator==(Temporary const &lhs, Temporary const &rhs) {
    return !(lhs != rhs);
  }

  friend bool operator<(Temporary const &lhs, Temporary const &rhs) {
    return lhs.id < rhs.id;
  }
};

struct Register final : Temporary {};

class Label final {
public:
  using LabelID = unsigned;

private:
  static LabelID nextID;
  static std::string labelName() {
    std::stringstream ss;
    ss << "L" << nextID++;
    return ss.str();
  }

public:
  Symbol sym;
  Label() : sym{Symbol::make(labelName())} {}
  Label(std::string name) : sym{Symbol::make(name)} {}

  bool operator<(const Label &rhs) const { return sym < rhs.sym; }
};

} // namespace KT

#endif /* TEMPORARY_TEMPORARY_H */
