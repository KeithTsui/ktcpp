//===-- Utilities.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SUPPORT_UTILITIES_H
#define SUPPORT_UTILITIES_H

#include <cassert>
#include <iostream>
#include <ostream>

namespace KT {
template <typename... Ts> struct _isIn {
  constexpr operator bool() {
    assert(false && "Arguments should be at least two.");
    return false;
  }
};

template <typename T, typename U> struct _isIn<T, U> {
  bool const value;
  constexpr _isIn(T const &a, U const &b) : value{a == b} {}
  constexpr operator bool() { return value; }
};

template <typename T, typename U, typename... Ts>
struct _isIn<T, U, Ts...> : _isIn<T, Ts...> {
  bool const value;
  constexpr _isIn(T const &a, U const &b, Ts const &...ts)
      : _isIn<T, Ts...>{a, ts...}, value{_isIn<T, Ts...>::value || a == b} {}
  constexpr operator bool() { return value; }
};

// check if the first argument is equal to any of the rest of arguments.
template <typename T, typename U, typename... Ts>
constexpr bool isIn(T const &a, U const &b, Ts const &...ts) {
  return _isIn<T, U, Ts...>(a, b, ts...);
}

template <typename Container>
std::ostream &outputContainer(std::ostream &os, Container const &c) {
  for (auto &e : c) {
    os << e.prettyPrint() << "\n";
  }
  return os;
}

template <typename Container>
std::ostream &outputPtrContainer(std::ostream &os, Container const &c) {
  for (auto &e : c) {
    os << e->prettyPrint() << "\n";
  }
  return os;
}

} // namespace KT
#endif /* SUPPORT_UTILITIES_H */
