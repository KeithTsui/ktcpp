//===-- macro.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef SUPPORT_MACRO_H
#define SUPPORT_MACRO_H

/**
 * Disable copy macro.
 */
#define DISABLE_COPY_ONLY(class_name)                                          \
  /*Copy Constructor*/                                                         \
  class_name(const class_name &other) = delete;                                \
  /*Copy assignment*/                                                          \
  class_name &operator=(const class_name &other) = delete;                     \
  /*Move Constructor*/                                                         \
  class_name(class_name &&) = default;                                         \
  /*Move assignment*/                                                          \
  class_name &operator=(class_name &&other) = default;

#define DISABLE_MOVE_ONLY(class_name)                                          \
  /*Copy Constructor*/                                                         \
  class_name(const class_name &other) = default;                               \
  /*Copy assignment*/                                                          \
  class_name &operator=(const class_name &other) = default;                    \
  /*Move Constructor*/                                                         \
  class_name(class_name &&) = delete;                                          \
  /*Move assignment*/                                                          \
  class_name &operator=(class_name &&other) = delete;

#endif /* SUPPORT_MACRO_H */
