//===-- LLVMRTTI.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#ifndef SUPPORT_LLVMRTTI_H
#define SUPPORT_LLVMRTTI_H

#define ENUM_ELEMENT(PREFIX, EN) PREFIX##_##EN

#define LLVMRTTIENUM1(ROOT, PREFIX, E1)                                        \
public:                                                                        \
  enum ROOT##Kind{ENUM_ELEMENT(PREFIX, E1)};

#define LLVMRTTIENUM2(ROOT, PREFIX, E1, E2)                                    \
public:                                                                        \
  enum ROOT##Kind{ENUM_ELEMENT(PREFIX, E1), ENUM_ELEMENT(PREFIX, E2)};

#define LLVMRTTIENUM3(ROOT, PREFIX, E1, E2, E3)                                \
public:                                                                        \
  enum ROOT##Kind{                                                             \
      ENUM_ELEMENT(PREFIX, E1),                                                \
      ENUM_ELEMENT(PREFIX, E2),                                                \
      ENUM_ELEMENT(PREFIX, E3),                                                \
  };

#define LLVMRTTIENUM4(ROOT, PREFIX, E1, E2, E3, E4)                            \
public:                                                                        \
  enum ROOT##Kind{ENUM_ELEMENT(PREFIX, E1), ENUM_ELEMENT(PREFIX, E2),          \
                  ENUM_ELEMENT(PREFIX, E3), ENUM_ELEMENT(PREFIX, E4)};

#define LLVMRTTIENUM5(ROOT, PREFIX, E1, E2, E3, E4, E5)                        \
public:                                                                        \
  enum ROOT##Kind{ENUM_ELEMENT(PREFIX, E1), ENUM_ELEMENT(PREFIX, E2),          \
                  ENUM_ELEMENT(PREFIX, E3), ENUM_ELEMENT(PREFIX, E4),          \
                  ENUM_ELEMENT(PREFIX, E5)};

#define LLVMRTTIENUM6(ROOT, PREFIX, E1, E2, E3, E4, E5, E6)                    \
public:                                                                        \
  enum ROOT##Kind{ENUM_ELEMENT(PREFIX, E1), ENUM_ELEMENT(PREFIX, E2),          \
                  ENUM_ELEMENT(PREFIX, E3), ENUM_ELEMENT(PREFIX, E4),          \
                  ENUM_ELEMENT(PREFIX, E5), ENUM_ELEMENT(PREFIX, E6)};

#define LLVMRTTIPURE(ROOT)                                                     \
private:                                                                       \
  ROOT##Kind const Kind;                                                       \
                                                                               \
public:                                                                        \
  auto getKind() const { return Kind; }                                        \
  ROOT(ROOT##Kind k) : Kind{k} {}                                              \
                                                                               \
public:                                                                        \
  virtual ~ROOT() = 0;

#define CLASSOF(CLASSNAME, ROOTCLASS, PREFIX)                                  \
public:                                                                        \
  static bool classof(ROOTCLASS const *const n) {                              \
    return n->getKind() == PREFIX##_##CLASSNAME;                               \
  }

#define CLASSOFRANG(CLASSNAME, ROOTCLASS, PREFIX, SUFFIX)                      \
public:                                                                        \
  static bool classof(ROOTCLASS const *const n) {                              \
    return n->getKind() >= PREFIX##_##CLASSNAME &&                             \
           n->getKind() < PREFIX##_##CLASSNAME##_##SUFFIX;                     \
  }

#endif /* SUPPORT_LLVMRTTI_H */
