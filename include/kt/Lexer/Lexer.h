//===-- Lexer.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef LEXER_LEXER_H
#define LEXER_LEXER_H

#include "kt/Token/Token.h"
#include "kt/Token/TokenType.h"
#include <cstddef>
#include <fstream>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include <map>

namespace KT {

/**
   A KT Lexer which transform a string of character into Tokens.
   Not intended to be inherited.
 */
class Lexer final {

  /**
     Keywords of Tiger programming language.
   */
  static std::map<std::string, TokenType::Terminal> keywords;

  size_t curLineNum, curColumnNum, columnNumBak;
  std::string parentLine;
  std::string prefix;
  std::string filename;
  size_t numErrors;
  std::ifstream file;
  std::vector<Token> tokenPeekBuffer;
  bool isDebug = true;

public:
  Lexer(std::string const &filename);
  int getNumErrors() { return numErrors; }
  int getCurrLine() { return curLineNum; }
  int getCurColumn() { return curColumnNum; }
  std::string getPrefix() { return prefix; }
  Token getToken();
  void putToken(Token const &t);
  void reset();

private:
  Token getTokenImpl();

  void error(std::string message);


  // get a char from file
  char get();

  // undo get char by pushing back a char into file
  void putback(char c);

  // Return the longest string of consecutive digits
  std::string readDigits();

  // Return the longest string of consecutive alphanumerics and '_'
  std::string readWordChars();

  // return a string literals between double quote.
  std::string readStringLiterals();
};

} // namespace KT

#endif /* LEXER_LEXER_H */
