//===-- Color.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef REGISTERALLOCATION_COLOR_H
#define REGISTERALLOCATION_COLOR_H

#include "kt/Analystics/InterferenceGraph.h"
#include "kt/Analystics/Liveness.h"
#include "kt/Frame/Frame.h"
#include "kt/Temporary/Temporary.h"
#include <functional>
#include <list>
#include <map>
#include <set>
#include <string>
#include <utility>

namespace KT {

namespace RA {

using Analystics::IGNode;
using Analystics::InterferenceGraph;

// a move is a pair of node connected by a move instruction.
// where the first node is the source of move and the second one is the
// destination of move.
using Analystics::Move;

using Allocation = std::map<Temporary, Register *>;

class Color {

public:
  Color(InterferenceGraph *ig, Allocation initAlloc,
        std::function<double(Temporary)> sc, std::set<Register *> const &regs)
      : ifGraph{ig}, initialAllocation{initAlloc}, spillCost{sc}, registers{
                                                                      regs} {}

private:
  InterferenceGraph *ifGraph;

  Allocation initialAllocation;

  //(* register ID to register machine name *)
  //(*e.g.100->"$a0" *)
  std::map<Register *, std::string> registerToName;

  // a function to calculate the spill cost for a temporary
  std::function<double(Temporary)> spillCost;

  // available register for allocation
  std::set<Register *> registers;

  // (* # of colors available *)
  int K;

  std::list<IGNode *> simplifyWorklist;
  std::list<IGNode *> freezeWorklist;
  std::list<IGNode *> spillWorklist;

  std::set<Move> coalescedMoves;
  std::set<Move> constraintedMoves;
  std::set<Move> frozenMoves;
  std::set<Move> worklistMoves;
  std::set<Move> activeMoves;

  std::set<IGNode *> spillNodes;
  std::set<IGNode *> coalescedNodes;
  std::set<IGNode *> coloredNodes;

  std::list<IGNode *> selectStatck;

  // temporay -> a set of (moves)
  // std::list<Move> moveList;
  std::map<Temporary, std::set<Move>> moveList;

  std::set<IGNode *> precolored;

  // initial is a list of uncolored nodes
  std::list<IGNode *> initialUncoloredNodes;
  std::map<Temporary, IGNode *> alias;

  void println(std::string const &msg) const;
  void remove(std::list<IGNode *> &l, IGNode const *n);
  bool isMemberOf(std::list<IGNode *> const &l, IGNode const *n) const;
  std::string nameOf(IGNode const *n) const;

  //(* get degree of a node *)
  int degreeOf(IGNode const *n) const;

  void addMove(IGNode *n, Move mv);

  //(* precolorTable is a mapping from temp to register,
  //* while initial is a list of uncolored nodes *)
  void buildAndInitialize();

  std::set<Move> movesOfNode(IGNode const *n);

  bool isMoveRelated(IGNode const *n);

  void makeWorkList();
  void enableMoves(std::set<IGNode *> const &nodes);

  void addWorkList(IGNode *node);

  IGNode *getAliasOf(IGNode *n);

  // (* adjacenent nodes *)
  std::set<IGNode *> getAdjacencies(IGNode const *n);

  //(* decrement degree for graph node n, return
  //* modified degreeMap and a (possibly augmented) simplify worklist *)
  // (* only decrement those non-precolored nodes - for *)
  //(* precolored nodes, we treat as if they have infinite *)
  //(* degree, since we shouldn't reassign them to different registers *)

  void decrementDegree(IGNode *n);

  bool inAdjacent(IGNode const *a, IGNode const *b);

  bool isOK(IGNode *a, IGNode const *b);
  bool isAllOK(IGNode *a, std::set<IGNode *> const &nodes);

  bool isConservative(std::set<IGNode *> const &nodes);

  void addEdge(IGNode *a, IGNode *b);

  void combine(IGNode *a, IGNode *b);

  void coalesce();

  void freezeMoves(IGNode *n);

  void freeze();

  void simplify();

  void selectSpill();

  Register *pickColor(std::set<Register *> const &registers);

  //(* assign color to all nodes on select stack. The parameter
  //* colored is all nodes that are already assigned a color. *)
  void assignColors();

  void iteration();

public:
  // the result of runing Coloring
  Allocation colored;
  std::list<Temporary> spills;
  // execute coloring algorithm
  void run();
};

} // namespace RA

} // namespace KT

#endif /* REGISTERALLOCATION_COLOR_H */
