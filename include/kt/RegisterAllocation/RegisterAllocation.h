//===-- RegisterAllocation.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef REGISTERALLOCATION_REGISTERALLOCATION_H
#define REGISTERALLOCATION_REGISTERALLOCATION_H

#include "kt/Analystics/CFG.h"
#include "kt/Analystics/InterferenceGraph.h"
#include "kt/Analystics/Liveness.h"
#include "kt/Frame/Frame.h"
#include "kt/MIR/mir.h"
#include "kt/RegisterAllocation/Color.h"
#include "kt/Temporary/Temporary.h"
#include <functional>
#include <list>
#include <memory>
#include <utility>

namespace KT {

namespace RA {

struct RegisterAllocator {
  // the result of Register allocation
  std::list<std::unique_ptr<MIR::MachineInstruction>> program;

  std::unique_ptr<Analystics::ControlFlowGraph> cfg;
  std::unique_ptr<Analystics::InterferenceGraph> ifGraph;
  LCCFrame::Frame *frame;

  Allocation allocation;
  std::list<Temporary> spills;

  RegisterAllocator(std::list<std::unique_ptr<MIR::MachineInstruction>> &&prog,
                    LCCFrame::Frame *f);

  double spillCost(Temporary temp);

  bool isRedundant(MIR::MachineInstruction *insrt);

  // MIRs
  void regAlloc();

  // rewrite program to accommodate spilled temporary.
  // list of instruction: program.
  // frame
  // spills
  // output: list of instruction
  void doSpilling();

  std::list<std::unique_ptr<MIR::MachineInstruction>> genInstrs(bool isDef,
                                                                Temporary t);

  std::pair<std::list<std::unique_ptr<MIR::MachineInstruction>>,
            std::list<Temporary>>
  alloc_du(bool isDef, std::list<Temporary> dus, Temporary t);

  std::list<std::unique_ptr<MIR::MachineInstruction>>
  transformInstruction(std::unique_ptr<MIR::MachineInstruction> &&i,
                       Temporary t);
};

} // namespace RA

} // namespace KT

#endif /* REGISTERALLOCATION_REGISTERALLOCATION_H */
