//===-- InterferenceGraph.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef ANALYSTICS_INTERFERENCEGRAPH_H
#define ANALYSTICS_INTERFERENCEGRAPH_H

#include "kt/Analystics/CFG.h"
#include "kt/Analystics/Liveness.h"
#include "kt/Temporary/Temporary.h"
#include "llvm/Support/ErrorHandling.h"
#include <list>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <utility>

namespace KT {

namespace Analystics {

struct IGNode {
  enum Status { InGraph, Removed, Colored };
  Temporary temp;
  std::set<IGNode *> adjacent;
  Status status;
  int degree; // when InGraph.
  //  std::string color; // when Colored.

  friend bool operator!=(IGNode const &lhs, IGNode const &rhs) {
    return lhs.temp == rhs.temp;
  }

  friend bool operator==(IGNode const &lhs, IGNode const &rhs) {
    return !(lhs != rhs);
  }

  bool operator<(IGNode const &m) const { return temp < m.temp; }

  std::string prettyPrint() {
    std::stringstream ss;
    ss << "{"
       << "temp: " << temp.prettyPrint() << ";"
       << "status: " << status << ";"
       << "degree: " << degree << ";"
       << "adjacents: [";
    for (auto adj : adjacent) {
      ss << adj->temp.prettyPrint() << ",";
    }
    ss << "]"
       << "}";
    return ss.str();
  }
};

// a move is a pair of node connected by a move instruction.
// where the first node is the source of move and the second one is the
// destination of move.

struct Move {
  IGNode *source;
  IGNode *destination;
  Move(IGNode *s, IGNode *d) : source{s}, destination{d} {}
  Move(Move const &m) : source(m.source), destination(m.destination) {}
  Move(Move &&m) : source(m.source), destination(m.destination) {}
  Move &operator=(Move const &m) {
    source = m.source;
    destination = m.destination;
    return *this;
  }

  Move &operator=(Move &&m) {
    source = m.source;
    destination = m.destination;
    return *this;
  }

  friend bool operator==(Move const &lhs, Move const &rhs) {
    return *lhs.source == *rhs.source && *lhs.destination == *rhs.destination;
  }

  friend bool operator!=(Move const &lhs, Move const &rhs) {
    return !(lhs == rhs);
  }

  bool operator<(Move const &m) const {
    return *source == *m.source ? source < m.source
                                : destination < m.destination;
  }

  std::string prettyPrint() {
    std::stringstream ss;
    ss << "{"
       << "source: " << source->prettyPrint() << "\n"
       << "destination:" << destination->prettyPrint() << "\n"
       << "}";
    return ss.str();
  }
};

class InterferenceGraph {
public:
  ControlFlowGraph *cfg;
  Liveness liveness;
  std::list<std::unique_ptr<IGNode>> graph;
  std::list<Move> moves;
  std::map<Temporary, IGNode *> temporayIGNodeMapping;

public:
  InterferenceGraph(ControlFlowGraph *cf) : cfg{cf}, liveness{cf} {

    // update nodes in cfg.
    for (auto &n : cfg->nodes) {
      if (liveness.liveOuts.count(n->ID)) {
        n->liveOuts = liveness.liveOuts[n->ID];
      } else {
        std::stringstream ss;
        ss << "Liveout map is not one-to-one: ";
        ss << "CFGNodeID: " << n->ID << "\n";
        llvm_unreachable(ss.str().c_str());
      }
    }

    // liveouts in cfgnodes;
    for (auto &n : cfg->nodes) {
      std::cout << n->prettyPrint() << std::endl;
    }

    computeOutgraph();
    computeAllMoves();
    computeInterferenceGraph();
  }

private:
  std::list<std::pair<Temporary, Temporary>> findEdges(CFGNode *cfgNode) {
    std::list<std::pair<Temporary, Temporary>> result;
    std::list<std::pair<Temporary, Temporary>> intermediateResult;

    for (auto &def : cfgNode->defs) {
      intermediateResult.clear();
      for (auto &liveOut : cfgNode->liveOuts) {
        if (def != liveOut) {
          intermediateResult.push_back(std::make_pair(def, liveOut));
        }
      }
      for (auto &r : intermediateResult) {
        result.push_back(r);
      }
    }
    return result;
  }
  std::list<std::pair<Temporary, Temporary>> findAllEdges() {
    std::list<std::pair<Temporary, Temporary>> result;
    for (auto &node : cfg->nodes) {
      auto res = findEdges(node.get());
      for (auto &r : res) {
        result.push_back(r);
      }
    }
    return result;
  }

  void computeOutgraph() {
    std::set<Temporary> temporaries;
    for (auto &node : cfg->nodes) {
      temporaries.insert(node->defs.begin(), node->defs.end());
      temporaries.insert(node->uses.begin(), node->uses.end());
    }
    for (auto &t : temporaries) {
      if (temporayIGNodeMapping.find(t) == temporayIGNodeMapping.end()) {
        auto ign = new IGNode{t, {}, IGNode::Status::InGraph, 0};
        std::unique_ptr<IGNode> ignPtr{ign};
        graph.push_front(std::move(ignPtr));
        temporayIGNodeMapping[t] = ign;
      }
    }
  }

  void computeAllMoves() {
    moves.clear();
    for (auto &node : cfg->nodes) {
      if (node->isMoveInstruction) {
        auto src = *node->uses.begin();
        auto dst = *node->defs.begin();
        auto srcIGN = temporayIGNodeMapping[src];
        auto dstIGN = temporayIGNodeMapping[dst];
        moves.push_back({srcIGN, dstIGN});
      }
    }
  }

  void computeInterferenceGraph() {

    std::cout << "the degrees of IGNodes in InterferenceGraph. \n";
    for (auto &igNode : graph) {
      std::cout << igNode->temp.prettyPrint() << ": " << igNode->degree << "\n";
    }

    auto allEdges = findAllEdges();
    for (auto &edge : allEdges) {
      std::cout << "Edge: " << edge.first.prettyPrint() << " --- "
                << edge.second.prettyPrint() << "\n";
      auto srcTemp = edge.first;
      auto dstTemp = edge.second;
      auto srcNode = temporayIGNodeMapping[srcTemp];
      auto dstNode = temporayIGNodeMapping[dstTemp];

      if (!(std::find(srcNode->adjacent.begin(), srcNode->adjacent.end(),
                      dstNode) != srcNode->adjacent.end() &&
            std::find(dstNode->adjacent.begin(), dstNode->adjacent.end(),
                      srcNode) != dstNode->adjacent.end())) {

        srcNode->adjacent.insert(dstNode);
        dstNode->adjacent.insert(srcNode);

        ++srcNode->degree;
        ++dstNode->degree;
      }
    }
  }

public:
  std::string prettyPrint() {
    std::stringstream ss;
    ss << "\nInterference Graph: \n";
    ss << "Nodes: \n";
    for (auto &n : graph) {
      ss << n->prettyPrint() << "\n";
    }
    ss << "Moves: \n";
    for (auto &m : moves) {
      ss << m.prettyPrint();
    }
    ss << "\n";
    return ss.str();
  }
};

} // namespace Analystics

} // namespace KT

#endif /* ANALYSTICS_INTERFERENCEGRAPH_H */
