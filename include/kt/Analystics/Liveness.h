//===-- Liveness.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef ANALYSTICS_LIVENESS_H
#define ANALYSTICS_LIVENESS_H

#include "kt/Analystics/CFG.h"
#include "kt/Temporary/Temporary.h"
#include <iostream>
#include <map>
#include <set>
#include <sstream>

namespace KT {

namespace Analystics {

class Liveness {
private:
  ControlFlowGraph *cfg;

public:
  std::map<CFGNodeID, std::set<Temporary>> liveIns;
  std::map<CFGNodeID, std::set<Temporary>> liveOuts;
  bool done = false;

  Liveness(ControlFlowGraph *cf) : cfg{cf} {
    iterativelyComputeLiveOuts();
    setLiveOuts();

    std::stringstream ss;
    ss << "Liveness LiveOuts: \n";
    for (auto &m : liveOuts) {
      ss << "{";
      ss << "CFGNodeID: " << m.first << ";\n";
      ss << "Temporaries: [ ";
      for (auto &e : m.second) {
        ss << e.prettyPrint() << ", ";
      }
      ss << "]\n";
      ss << "}\n";
    }
    std::cout << ss.str();
  }

private:
  //(* Given livein map, compute liveout set for node n *)
  std::set<Temporary> computeLiveOut(CFGNode *node) {
    std::set<Temporary> liveOut;
    for (auto &succ : node->successors) {
      auto liveIn = liveIns[succ->ID];
      liveOut.insert(liveIn.begin(), liveIn.end());
    }
    return liveOut;
  }

  template <typename Set> bool set_compare(Set const &lhs, Set const &rhs) {
    if (lhs.size() != rhs.size())
      return false;
    if (lhs.size() == rhs.size()) {
      for (auto &e : lhs) {
        if (rhs.count(e) == 0)
          return false;
      }
    }
    return true;
  }

  void iterativelyComputeLiveOuts() {
    while (!done) {
      for (auto &node : cfg->nodes) {
        auto const oldLiveIn = liveIns[node->ID];
        auto const oldLiveOut = liveOuts[node->ID];
        auto const defs = node->defs;
        auto const uses = node->uses;
        std::set<Temporary> newLiveIn;
        newLiveIn.insert(uses.begin(), uses.end());
        std::set<Temporary> diff = oldLiveOut;
        for (auto &t : defs) {
          diff.erase(t);
        }

        newLiveIn.insert(diff.begin(), diff.end());
        auto const newLiveOut = computeLiveOut(node.get());

        if (set_compare(newLiveIn, oldLiveIn) &&
            set_compare(newLiveOut, oldLiveOut)) {
          // if (newLiveIn == oldLiveIn && newLiveOut == oldLiveOut) {
          done = true;
        } else {
          done = false;

          liveIns[node->ID] = newLiveIn;
          liveOuts[node->ID] = newLiveOut;
        }
      }
    }
  }

  void setLiveOuts() {
    for (auto &node : cfg->nodes) {
      node->liveOuts = liveOuts[node->ID];
    }
  }
};

} // namespace Analystics

} // namespace KT

#endif /* ANALYSTICS_LIVENESS_H */
