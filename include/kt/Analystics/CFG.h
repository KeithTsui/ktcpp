//===-- CFG.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef ANALYSTICS_CFG_H
#define ANALYSTICS_CFG_H

#include "kt/MIR/mir.h"
#include "kt/Support/Utilities.h"
#include "kt/Temporary/Temporary.h"
#include <cassert>
#include <cstddef>
#include <list>
#include <memory>
#include <set>
#include <sstream>
#include <utility>
#include <vector>

namespace KT {

namespace Analystics {

using CFGNodeID = unsigned;

struct CFGNode {
  CFGNodeID ID;
  std::set<Temporary> defs;
  std::set<Temporary> uses;
  bool isMoveInstruction;
  std::set<CFGNode *> predecessors;
  std::set<CFGNode *> successors;
  MIR::MachineInstruction *orgMIR;

  // liveOuts is set by liveness.
  std::set<Temporary> liveOuts;

  std::string prettyPrint() {
    std::stringstream ss;
    ss << "{"
       << "ID: " << ID << "\n";

    ss << "defs: ";
    outputContainer(ss, defs);

    ss << "uses: ";
    outputContainer(ss, uses);

    ss << "liveOuts:";
    outputContainer(ss, liveOuts);

    ss << "}";

    return ss.str();
  }
};

class ControlFlowGraph {
public:
  std::list<std::unique_ptr<CFGNode>> nodes;

  std::list<MIR::MachineInstruction *> instrs;

  std::vector<CFGNode *> nodeVector;
  std::vector<MIR::MachineInstruction *> instrVector;

  ControlFlowGraph(std::list<MIR::MachineInstruction *> &&prog)
      : instrs{std::move(prog)} {
    // transform machine instruction to node;
    for (auto instr : instrs) {
      nodes.push_back(makeNode(instr));
    }

    // connect nodes into control flow graph.
    // for case of jump instruction;
    for (auto &node : nodes) {
      nodeVector.push_back(node.get());
    }
    for (auto i : instrs) {
      instrVector.push_back(i);
    }
    assert(nodeVector.size() == instrVector.size() && "..");

    for (size_t i = 0; i < instrVector.size(); ++i) {
      auto n = nodeVector[i];
      auto ins = instrVector[i];
      doJump(ins, n);
    }

    // for case of others;
    connect(instrVector, nodeVector);

    // done;
  }

private:
  std::unique_ptr<CFGNode> makeNode(MIR::MachineInstruction *i) {
    auto ret = new CFGNode{};
    ret->ID = (unsigned)nodes.size();
    if (auto instr = llvm::dyn_cast_or_null<MIR::MIROperation>(i)) {
      for (auto t : instr->dst) {
        ret->defs.insert(t);
      }
      for (auto t : instr->src) {
        ret->uses.insert(t);
      }
      ret->isMoveInstruction = false;
    } else if (llvm::dyn_cast_or_null<MIR::MIRLabel>(i)) {
      ret->isMoveInstruction = false;
    } else if (auto inst = llvm::dyn_cast_or_null<MIR::MIRMove>(i)) {
      ret->defs.insert(inst->dst);
      ret->uses.insert(inst->src);
      ret->isMoveInstruction = true;
    }
    return std::unique_ptr<CFGNode>(ret);
  };

  void makeEdge(CFGNode *from, CFGNode *to) {
    if (std::find(from->successors.begin(), from->successors.end(), to) ==
        from->successors.end()) {
      from->successors.insert(to);
      to->predecessors.insert(from);
    }
  };

  void connect(std::vector<MIR::MachineInstruction *> &is,
               std::vector<CFGNode *> &ns) {
    if (instrs.size() < 2)
      return;
    for (size_t i = 0; i < instrs.size() - 1; ++i) {
      auto i1 = is[i];
      // auto i2 = instrs[i + 1];
      auto n1 = ns[i];
      auto n2 = ns[i + 1];
      auto op = llvm::dyn_cast_or_null<MIR::MIROperation>(i1);
      if (op == nullptr || op->jump.empty()) {
        makeEdge(n1, n2);
      }
    }
  };

  void doJump(MIR::MachineInstruction *instr, CFGNode *node) {
    if (auto op = llvm::dyn_cast_or_null<MIR::MIROperation>(instr)) {
      for (auto l : op->jump) {
        size_t index = 0;
        for (; index < instrVector.size(); ++index) {
          if (auto label =
                  llvm::dyn_cast_or_null<MIR::MIRLabel>(instrVector[index])) {
            if (label->lab.sym.id == l.sym.id) {
              makeEdge(node, nodeVector[index]);
              break;
            }
          }
        }
      }
    }
  };
};

} // namespace Analystics

} // namespace KT

#endif /* ANALYSTICS_CFG_H */
