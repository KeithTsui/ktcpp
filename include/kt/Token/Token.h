//===-- Token.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef LEXER_TOKEN_H
#define LEXER_TOKEN_H

#include "kt/Token/TokenType.h"
#include <sstream>
#include <string>

namespace KT {

class Token {
private:
  TokenType type;
  std::string content;

public:
  int line, column, length;

  Token(int tokenType, std::string const &contentString, int l = 0, int c = 0,
        int len = 0)
      : type{tokenType}, content{contentString}, line{l}, column{c}, length{
                                                                         len} {}

  TokenType getType() { return type; }

  std::string getTokenString() { return content; }

  bool operator<(Token const &other) {
    return (type < other.type) ||
           ((type == other.type) && (content < other.content));
  }

  std::string emit() {
    auto tokenTypeID = type.getValue();
    std::stringstream ss;
    ss << "( Token: { typeID: " << tokenTypeID
       << "; typeName: " << TokenType::tokenTypeNames[tokenTypeID]
       << "; content: "
       << "'" << content << "'})";
    return ss.str();
  }
};

} // namespace KT

#endif /* LEXER_TOKEN_H */
