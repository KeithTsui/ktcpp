//===-- TokenType.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef LEXER_TOKENTYPE_H
#define LEXER_TOKENTYPE_H

#include <string>
#include <unordered_map>

namespace KT {

class TokenType {
  int value;

public:
  static constexpr int BeginOfTerminal = 0;

  enum Terminal {
    // Keyword tokens
    BOT = BeginOfTerminal,
    ARRAY,
    BREAK,
    DO,
    ELSE,
    END,
    FOR,
    FUNCTION,
    IF,
    IN,
    LET,
    OF,
    THEN,
    TO,
    TYPE,
    VAR,
    WHILE,
    ENDIF,
    BEGIN,
    ENDDO,
    RETURN,
    INT,
    FLOAT,
    STRING,
    BOOL,
    TRUE,
    FALSE,

    // Regular tokens
    COMMA,
    COLON,
    SEMI,
    LPAREN,
    RPAREN,
    LBRACK,
    RBRACK,
    LBRACE,
    RBRACE,
    PERIOD,
    PLUS,
    MINUS,
    MULT,
    DIV,
    EQ,
    NEQ,
    LESSER,
    GREATER,
    LESSEREQ,
    GREATEREQ,
    AND,
    OR,
    ASSIGN,
    //
    // Type tokens
    ID,
    INTLIT,
    FLOATLIT,
    STRINGLIT,

    // NULL token
    NULLL,

    // End of file token
    EOFF,

    // End of Terminal
    EOT
  };

  static constexpr int EndOfTerminal = EOT;
  static constexpr int BeginOfNonterminal = 100;

  enum Nonterminal {
    BON = BeginOfNonterminal,
    TIGER_PROGRAM,
    DECLARATION_LIST,
    DECLARATION,
    STAT_SEQ,
    TYPE_DECLARATION_LIST,
    VAR_DECLARATION_LIST,
    FUNCT_DECLARATION_LIST,
    TYPE_DECLARATION,
    VAR_DECLARATION,
    FUNCT_DECLARATION,
    TYPE_EXPR,
    TYPE_ID,
    ARRAY_TYPE,
    RECORD_TYPE,
    ID_LIST,
    OPTIONAL_INIT,
    ID_LIST_TAIL,
    CONST,
    PARAM_LIST,
    RET_TYPE,
    PARAM,
    PARAM_LIST_TAIL,
    STAT,
    STAT_SEQ_TAIL,
    EXPR,
    STAT_IF_TAIL,
    STAT_FUNCT_OR_ASSIGN,
    LVALUE_TAIL,
    STAT_ASSIGN,
    EXPR_LIST,
    STAT_ASSIGN_STUFF,
    STAT_ASSIGN_TAIL,
    EXPR_TAIL,
    OR_EXPR_TAIL,
    AND_EXPR_TAIL,
    COMPARE_TAIL,
    TERM_TAIL,
    OR_EXPR,
    OR_OP,
    AND_EXPR,
    AND_OP,
    COMPARE,
    COMPARE_OP,
    TERM,
    ADD_OP,
    FACTOR,
    MUL_OP,
    LVALUE,
    EXPR_LIST_TAIL,
    EON
  };

  static constexpr int EndOfNonterminal = EON;

  static std::unordered_map<int, std::string> tokenTypeNames;

  static constexpr int BeginOfAction = 200;

  enum Action {
    BOA = BeginOfAction,

    // Dealing with main
    MakeMainLabel,

    // Scope
    InitializeScope,
    FinalizeScope,

    // type
    MakeTypesBegin,
    MakeTypesEnd,

    // variable
    MakeVariablesBegin,
    MakeVariablesEnd,

    // function
    MakeFunctionsBegin,
    MakeFunctionsMid,
    MakeFunctionsEnd,

    // assign
    MakeAssignBegin,
    MakeAssignEnd,

    // expression
    MakeExprBegin,
    MakeExprEnd,
    EmdExprBegin,
    EmdExprEnd,

    // for statement
    MakeForBegin,
    MakeForMid,
    MakeForEnd,

    // lvalue
    MakeArrayBegin,
    MakeArrayEnd,

    // if statement
    MakeIfBegin,
    MakeIfMid,
    MakeElseLabel,
    MakeIfEnd,

    // return statement
    MakeReturnBegin,
    MakeReturnEnd,

    // while statment
    MakeWhileBegin,
    MakeWhileMid,
    MakeWhileEnd,

    // break
    MakeBreak,
    BeforeAssignLabel,
    EndAssignLabel,

    EOA
  };

  TokenType(int val) : value{val} {}
  TokenType() : TokenType(0) {}

  static constexpr int EndOfAction = EOA;

  void operator=(int val) { value = val; }

  friend bool operator<(TokenType const &lhs, TokenType const &rhs) {
    return lhs.value < rhs.value;
  }

  friend bool operator==(TokenType const &lhs, TokenType const &rhs) {
    return lhs.value == rhs.value;
  }

  friend bool operator!=(TokenType const &lhs, TokenType const &rhs) {
    return !(lhs == rhs);
  }

  int getValue() const { return value; }
};

using Nonterm = TokenType::Nonterminal;
using Term = TokenType::Terminal;

} // namespace KT

#endif /* LEXER_TOKENTYPE_H */
