//===-- mir.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef MIR_MIR_H
#define MIR_MIR_H

#include "kt/Temporary/Temporary.h"
#include "llvm/Support/Casting.h"
#include <list>
#include <sstream>
#include <string>

namespace KT {

namespace MIR {

struct MachineInstruction {
  virtual std::string prettyPrint() const = 0;
  virtual std::string toAsm() const = 0;
  virtual ~MachineInstruction() {}

  // LLVM-style RTTI Begin
public:
  enum MachineInstructionKind { MIK_MIROperation, MIK_MIRLabel, MIK_MIRMove };

private:
  MachineInstructionKind const Kind;

public:
  MachineInstructionKind getKind() const { return Kind; }
  MachineInstruction(MachineInstructionKind k) : Kind{k} {}
};

#define MIRCLASSOF(CLASSNAME)                                                     \
  static bool classof(MachineInstruction const *const n) {                     \
    return n->getKind() == MIK_##CLASSNAME;                                    \
  }

struct MIROperation : MachineInstruction {
  std::string asmFormat;
  std::list<Temporary> dst;
  std::list<Temporary> src;
  std::list<Label> jump;

  MIROperation(std::string const &asmFm, std::list<Temporary> &&s,
               std::list<Temporary> &&d, std::list<Label> &&j)
      : MachineInstruction{MIK_MIROperation}, asmFormat{asmFm},
        dst{std::move(d)}, src{std::move(s)}, jump{std::move(j)} {}

  std::string prettyPrint() const override {
    std::stringstream ss;
    ss << asmFormat << " ; ";
    ss << "src: ";
    for (auto &t : src) {
      ss << t.prettyPrint() << " ";
    }
    ss << "; dst: ";
    for (auto &t : dst) {
      ss << t.prettyPrint() << " ";
    }
    ss << "\n";

    return ss.str();
  }
  std::string toAsm() const override { return ""; }

public:
  MIRCLASSOF(MIROperation);
};

struct MIRLabel : MachineInstruction {
  std::string asmFormat;
  Label lab;

  MIRLabel(std::string const &asmFm, Label l)
      : MachineInstruction{MIK_MIRLabel}, asmFormat{asmFm}, lab{l} {}

  std::string prettyPrint() const override {
    std::stringstream ss;
    ss << asmFormat << " ; ";
    ss << "lab: " << lab.sym.prettyPrint();
    ss << "\n";
    return ss.str();
  }
  std::string toAsm() const override { return ""; }

public:
  MIRCLASSOF(MIRLabel);
};

struct MIRMove : MachineInstruction {
  std::string asmFormat;
  Temporary dst;
  Temporary src;

  MIRMove(std::string const &asmFm, Temporary d, Temporary s)
      : MachineInstruction{MIK_MIRMove}, asmFormat{asmFm}, dst{d}, src{s} {}

  std::string prettyPrint() const override {
    std::stringstream ss;
    ss << asmFormat << " ; ";
    ss << "src: " << src.prettyPrint();
    ss << "; dst: " << dst.prettyPrint();
    ss << "\n";
    return ss.str();
  }
  std::string toAsm() const override { return ""; }

  MIRCLASSOF(MIRMove);
};

#undef MIRCLASSOF

} // namespace MIR

} // namespace KT

#endif /* MIR_MIR_H */
