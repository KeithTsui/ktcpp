//===-- Parser.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef PARSER_PARSER_H
#define PARSER_PARSER_H

#include <cstring>
#include <memory>
#include <unordered_map>

#include "kt/AST/AST.h"
#include "kt/Lexer/Lexer.h"
#include "kt/SymbolTable/SymbolTable.h"
#include "kt/SymbolTable/SymbolTableEntry.h"
#include "kt/SymbolTable/TokenPair.h"

namespace KT {

/**
 * Disable copy macro.
 */
#define DISABLE_COPY(class_name)                                               \
  class_name(class_name &&) = delete;                                          \
  class_name(const class_name &other) = delete;                                \
  class_name &operator=(const class_name &other) = delete

/**
 * Template used for reverse iteration in C++11 range-based for loops.
 *
 *  std::vector<int> v = {1, 2, 3, 4, 5};
 *  for (auto x : reverse_iterate(v))
 *    std::cout << x << " ";
 */
template <typename T> class reverse_range final {
private:
  T &x;

public:
  explicit reverse_range(T &x) : x(x) {}
  auto begin() const -> decltype(this->x.rbegin()) { return x.rbegin(); }
  auto end() const -> decltype(this->x.rend()) { return x.rend(); }
};

template <typename T> reverse_range<T> reverse_iterate(T &x) {
  return reverse_range<T>(x);
}

class Parser final {
public:
  /// disable implicit copy
  DISABLE_COPY(Parser);

  /// enable explicit constructor
  explicit Parser(std::string const &fileName);

  /// parse scanner's tokens
  void parse();

  /// generate IR code
  void ir_code();

  /// get IR code
  std::vector<std::string> &get_ir() { return IR; }

  /// parse error info
  void error(int expr, Token *word);

  /// insert items into parse table
  void addToParseTable(const int nonterm, const std::vector<int> &terminals,
                       const std::vector<int> &expand_rule);

  /// get function info
  std::unordered_map<std::string,
                     std::vector<std::pair<std::string, std::string>>> &
  get_func_info() {
    return func_map_;
  }

  /// output file
  std::ofstream outFile;

  /// print debug
  bool printDebug;

private:
  /// initialize terminal map data structure: terminalMapped
  // void initializeTerminalMapped();

  /// initial symbol string for IR
  void initializeIRMapped();

  /// create parse table for Tiger
  void initParseTable();

  /// parse action like TYPES, VARIABLES, FUNCTIONS declaration
  void parseAction(int expr, std::vector<Token> &tempBuffer);

  /// parse for action
  void parseForAction(std::vector<Token> &blockBuffer);

  /// parse for statement end
  void parseForActionEnd(std::vector<Token> &blockBuffer);

  /// parse function action: function name (x:int) : return-type
  void parseFuncAction(std::vector<Token> &tempBuffer);

  /// parse if statement action
  void parseIfAction(std::vector<Token> &tempBuffer);

  /// parse return statement action
  void parseReturnAction(std::vector<Token> &tempBuffer);

  /// parse while statement action
  void parseWhileAction(std::vector<Token> &tempBuffer);

  /**
   * @brief parse expression from infix to postfix expression.
   *
   * @note  posifix expression is convenient way to do semantic
   *        check and generate IR.
   */
  std::vector<Token> cvt2PostExpr(std::vector<Token> &tempBuffer, size_t index);

  /// generate IR and symbol table elements from postfix expression
  Token evaPostfix(std::vector<Token> &expr);

  /// initialize Scop
  inline void initScoping() {
    ++currentLevel;
    SymbolTablePtr st = std::make_shared<SymbolTable>(currentLevel);
    g_SymbolTable[currentLevel] = st;
  }

  /// finalize Scope
  inline void finalizeScoping() {
    g_SymbolTable[currentLevel]->dump();
    g_SymbolTable.erase(currentLevel);
    --currentLevel;
  }

  /// generate new temp name
  inline std::string new_temp() { return "t" + std::to_string(numTemps++); }
  inline std::string new_fp_temp() {
    return "f" + std::to_string(numFPTemps++);
  }

  /// generate new loop label name
  inline std::string new_loop_label() {
    return "loop_label" + std::to_string(numLoops++);
  }
  /// generate new if label name
  inline std::string new_if_label() {
    return "if_label" + std::to_string(numIfs++);
  }

  /// get sub token pairs buffer
  inline std::vector<Token> subTokens(const std::vector<Token> &buffer,
                                      size_t actBegin, size_t actEnd);

  /// get terminal symbol type
  std::string getSymbolType(Token A);

  /// detect action
  bool detectAction(int symbol, bool &enable_block,
                    std::vector<Token> &blockBuffer, bool &enable_buffer,
                    std::vector<Token> &tempBuffer);

  Lexer lexer;           /// code scanner
  int numErrors = 0;     /// how many errors detected
  int currentLevel = -1; /// current paser code's scope level
  int numIfs = 0;        /// generate if labels for IR
  int numTemps = 0;      /// generate temp variable for IR
  int numFPTemps = 0;
  int numLoops = 0;           /// generate loop label name for IR
  std::string globalFileName; /// global file name
  std::stack<int> parseStack; /// parse stack

  std::unordered_map<int, std::string> OperatorMapped;
  std::vector<std::string> IR; /// IR container

  /// terminal symbol's string output for error info
  // static std::unordered_map<int, std::string> terminalMapped_;

  /// parse table for parsing
  std::map<TokenPair, std::vector<int>> parseTable_;

  /// for begin and end expr stack
  std::stack<Token> tempStack_;

  /// loop label stack
  std::stack<std::pair<std::string, std::string>> labelStack_;

  /// current action begin and end postion in temp buffer
  size_t actBegin_;
  size_t actEnd_;

  /// left side array in assignment
  size_t is_arr_ = -1;
  std::string array_store_;

  /// inside a function return type
  bool isInside_func_ = false;
  bool isInside_while_ = false;
  bool isInside_for_ = false;
  std::stack<std::pair<std::string, std::string>> blockStack_;
  /// tracking funcRetType if inside a function
  std::string funcRetType_;
  bool isFuncRet_ = false;
  bool isMainRet_ = false;

  std::unordered_map<std::string,
                     std::vector<std::pair<std::string, std::string>>>
      func_map_;




};
} // namespace KT

#endif /* PARSER_PARSER_H */
