//===-- RDParser.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef PARSER_RDPARSER_H
#define PARSER_RDPARSER_H

#include "kt/AST/AST.h"
#include "kt/Lexer/Lexer.h"
#include "kt/Token/Token.h"
#include <functional>
#include <list>
#include <memory>
#include <utility>
#include <vector>

namespace KT {

/**
 * Disable copy macro.
 */
#define DISABLE_COPY(class_name)                                               \
  class_name(class_name &&) = delete;                                          \
  class_name(const class_name &other) = delete;                                \
  class_name &operator=(const class_name &other) = delete

class RDParser final {
private:
  Lexer lexer; /// code scanner
  std::string filename;
  unsigned numErrors;

public:
  bool printDebug;

public:
  DISABLE_COPY(RDParser);
#undef DISABLE_COPY

  /// enable explicit constructor
  explicit RDParser(std::string const &fileName);

  /// parse scanner's tokens
  std::unique_ptr<AST::TigerProgram> parse();

  // tokenize the input program
  std::vector<Token> tokenize();

  // reset parser
  void reset();

  // TIger PL Grammar Parsing
private:
  Token peek1() {
    auto t = lexer.getToken();
    lexer.putToken(t);
    return t;
  }

  std::pair<Token, Token> peek2() {
    auto f = lexer.getToken();
    auto s = lexer.getToken();
    lexer.putToken(s);
    lexer.putToken(f);
    return {f, s};
  }

  /**
     <tiger-program> -> <let-statement>
   */
  std::unique_ptr<AST::TigerProgram> parseTigerProgram();

  /**
   <let-statement> -> let <declaration-list> in <statement-list> end ;
 */
  std::unique_ptr<AST::TigerProgram> parseLetStatement();

  /**
   <declaration-list> -> <declaration> <declaration-list>
                       | NULL
 */
  std::list<std::unique_ptr<AST::Declaration>> parseDeclarationList();

  /**
   <declaration> -> <type-declaration>
               | <var-declaration>
               | <function-declaration>
 */
  std::unique_ptr<AST::Declaration> parseDeclaration();

  /**
   <type-declaration> -> type id = <type> ;
 */
  std::unique_ptr<AST::TypeDeclaration> parseTypeDeclaration();

  /**
   <type> -> <named-type>
           | <array-type>
           | <record-type>
 */
  std::unique_ptr<AST::Type> parseType();

  /**
   <named-type> -> int | float | string | id
 */
  std::unique_ptr<AST::NamedType> parseNamedType();

  /**
   <arrary-type> -> array [ IntLiteral ] of <type-id> # array type
 */
  std::unique_ptr<AST::ArraryType> parseArrayType();

  /**
   <record-type> -> {<name-type-pair-list>} # record type
 */
  std::unique_ptr<AST::RecordType> parseRecordType();

  /**
   <var-declaration> -> var id : <type> <optional-initializer> ;
 */
  std::unique_ptr<AST::VariableDeclaration> parseVaraibleDeclaration();

  /**
     <optional-initializer> -> := <expression>
                               | NULL
 */
  std::unique_ptr<AST::Expression> parseOptionalInitializer();

  /**
   <function-declaration> -> function id ( <name-type-pair-list> ) : <type>
                             begin <statement-list> end ;
 */
  std::unique_ptr<AST::FunctionDeclaration> parseFunctionDeclaration();

  /**
   <name-type-pair-list> -> <name-type-pair> <name-type-pair-tail>
                          | NULL
 */
  std::vector<AST::Parameter> parseNameTypePairList();

  /**
   <name-type-pair-tail> -> , <name-type-pair> <name-type-pair-tail>
                          | NULL
 */
  std::vector<AST::Parameter> parseParameterTail();

  // 26 : <ret-type> -> : <type>
  std::unique_ptr<AST::Type> parseReturnType();

  /**
   <name-type-pair> -> id : <type>
 */
  std::unique_ptr<AST::Parameter> parseParameter();

  /**
   <statement-list> -> <statement> <statement-list>
                     | NULL
 */
  std::vector<std::unique_ptr<AST::Statement>> parseStatementList();

  /**
     <statement> -> <if-statement>
                 -> <while-statement>
                 -> <for-statement>
                 -> <break-statement>
                 -> <let-statement>
                 -> <call-or-assignment-statement>
                 -> <return-statement>
   */
  std::unique_ptr<AST::Statement> parseStatement();

  /**
   <if-statement> -> if <expression> then <statement-list> <if-statement-tail>
   <if-statement-tail> -> else <statement-list> endif ;
                        | endif ;
 */
  std::unique_ptr<AST::IfStatement> parseIfStatement();

  /**
   <while-statement> -> while <expression>
                        do <statement-list> enddo ;
 */
  std::unique_ptr<AST::WhileStatement> parseWhileStatement();

  /**
   <for-statement> -> for id := <expression> to <expression>
                      do <statement-list> enddo;
 */
  std::unique_ptr<AST::ForStatement> parseForStatement();

  /**
   <break-statement> -> break ;
 */
  std::unique_ptr<AST::BreakStatement> parseBreakStatement();

  /**
   <call-or-assignment-statement> -> <call-expression> ;
                                   | <lvalue-expression> := <expression> ;
 */
  std::unique_ptr<AST::Statement> parseCallOrAssignStatement();

  /**
     <return-statement> -> return;
                         | return <expression>;
   */
  std::unique_ptr<AST::Statement> parseReturnStatement();

  /**
     <call-statement> -> ID ( <expression-list> ) ;
  */
  std::unique_ptr<AST::CallExpression> parseCallExpression();

  /**
   <expression-list> -> <expression> <expression-list-tail>
                      | NULL
   <expression-list-tail> -> , <expression> <expression-list-tail>
                           | NULL
 */
  std::vector<std::unique_ptr<AST::Expression>> parseExpressions();

  /**
   <expression-list-tail> -> , <expression> <expression-list-tail>
                           | NULL
 */
  std::vector<std::unique_ptr<AST::Expression>> parseExpressionTail();

  /**
   <expression> -> <or-expression>
 */
  std::unique_ptr<AST::Expression> parseExpression();

  /**
   <or-expression> -> <and-expression> <or-op> <and-expression>
 */
  std::unique_ptr<AST::Expression> parseOrExpression();

  /**
   <and-expression> -> <compare-expression> <and-op> <compare-expression>
 */
  std::unique_ptr<AST::Expression> parseAndExpression();

  /**
   <compare-expression> -> <term-expression>
                         | <term-expression> <cmp-op> <term-expression>
 */
  std::unique_ptr<AST::Expression> parseCmpExpression();

  /**
   <term-expression> -> <factor-expression>
                      | <factor-expression> <term-op> <factor-expression>
 */
  std::unique_ptr<AST::Expression> parseTermExpression();

  /**
   <factor-expression> -> <atom-expression>
                        | <atom-expression> <factor-op> <atom-expression>
 */
  std::unique_ptr<AST::Expression> parseFactorExpression();

  /**
   <atom-expression> -> ( <expression> )
                      | <constant-expression>
                      | <lvalue-expression>
                      | <call-expression>
 */
  std::unique_ptr<AST::Expression> parseAtomExpression();

  /**
   <constant-expression> -> <int-expression>
                          | <float-expression>
                          | <string-expression>
 */
  std::unique_ptr<AST::Expression> parseConstantExpression();

  /**
   <int-expression> -> IntegerLiteral
 */
  std::unique_ptr<AST::IntExpression> parseIntExpression();

  /**
   <float-expression> -> FloatLiteral
 */
  std::unique_ptr<AST::FloatExpression> parseFloatExpression();

  /**
   <string-expression> -> StringLiteral
 */
  std::unique_ptr<AST::StringExpression> parseStringExpression();

  /**
     <lvalue-expression> -> id <lvalue-expression-tail>
   */
  std::unique_ptr<AST::Variable> parseLvalue();

  /**
   <lvalue-expression-tail> -> [ <int-expression> ] <lvalue-expression-tail>
                             | . id <lvalue-expression-tail>
                             | NULL
 */
  std::unique_ptr<AST::Variable>
  parseLvalueTail(std::unique_ptr<AST::Variable> &&var);
};

} // namespace KT

#endif /* PARSER_RDPARSER_H */
