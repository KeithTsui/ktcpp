//===-- LCCFrame.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef LCC_LCCFRAME_H
#define LCC_LCCFRAME_H

#include "kt/Temporary/Temporary.h"
#include <cstddef>
#include <memory>
#include <string>
#include <vector>
namespace KT {

namespace LCC {

class LCCFrame {
public:
  using Register = std::string;

  struct Access {
    virtual ~Access() {}
  };
  struct InFrame : Access {
    int Index;
  };
  struct InReg : Access {
    Temporary reg;
  };

  struct Info {
    Label name;
    std::vector<std::unique_ptr<Access>> formals;
    int locals;
  };
};

extern size_t wordSize;

} // namespace LCC

} // namespace KT

#endif /* LCC_LCCFRAME_H */
