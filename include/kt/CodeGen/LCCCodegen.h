//===-- LCCCodegen.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef CODEGEN_LCCCODEGEN_H
#define CODEGEN_LCCCODEGEN_H

#include "kt/Frame/Frame.h"
#include "kt/IR/IR.h"
#include "kt/MIR/mir.h"
#include "kt/Temporary/Temporary.h"
#include <functional>
#include <list>
#include <memory>
#include <sstream>
#include <string>

namespace KT {

namespace LCC {
struct LCCCodeGen {

  std::list<std::unique_ptr<MIR::MachineInstruction>> instrs;

  void emit(std::unique_ptr<MIR::MachineInstruction> &&i);

  Temporary result(std::function<void(Temporary)> instructionGenerator);

  std::string intToStr(int n);

  std::list<Temporary> const &getCallDefs();

  void munchStatement(IR::Statement *stm);

  Temporary munchExpression(IR::Expression *exp);

  void munchArgs(std::list<IR::Expression *> args);

  std::list<std::unique_ptr<MIR::MachineInstruction>>
  codegen(IR::Statement *stm);
};

} // namespace LCC

} // namespace KT

#endif /* CODEGEN_LCCCODEGEN_H */
