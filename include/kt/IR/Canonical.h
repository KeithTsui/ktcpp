//===-- Canonical.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef IR_CANONICAL_H
#define IR_CANONICAL_H

#include "kt/IR/IR.h"
#include <list>
#include <vector>

namespace KT {

namespace IR {

// A IR tree is rewritten into a list of canonical trees without SEQ or ESEQ
// nodes, and moving CALLs to top level.
// Because we want the linear execution order of each computation,
// therefore no need for SEQ and ESEQ.
// 1. No SEQ and ESEQ.
// 2. The parent of each CALL is either EXP(...) or MOVE(TEMP t, ...).
std::list<StmPtr> linearize(StmPtr &&stm);

using BasicBlock = std::list<StmPtr>;

// Group canonical trees into a set of basic block,
// which contains no internal jumps or labels.
std::list<BasicBlock> toBasicBlocks(std::list<StmPtr>&& statements);

// the basic blocks  are ordered into  a set of traces
// in which every CJUMP is immediately followed by its false label.
std::list<StmPtr> traceScheduling(std::list<BasicBlock> &&basicBlocks);

} // namespace IR

} // namespace KT

#endif /* IR_CANONICAL_H */
