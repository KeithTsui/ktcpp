//===-- IR.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef IR_IR_H
#define IR_IR_H

#include "kt/Support/LLVMRTTI.h"
#include "kt/Temporary/Temporary.h"
#include "llvm/Support/Casting.h"
#include <map>
#include <memory>
#include <sstream>
#include <vector>

namespace KT {
namespace IR {
/**
Expr = CONST i
     | NAME label
     | TEMP t
     | BINOP Expr * op * Expr
     | MEM Expr
     | CALL Expr * Expr List
     | ESEQ Stm * Expr

Stm = MOVE Expr * Expr
    | EXP Expr
    | JUMP Expr
    | CJUMP rop * Expr * Expr * label * label
    | SEQ stm * stm
    | LABEL label
 */

enum class BiOp {
  PLUS,
  MINUS,
  MUL,
  DIV,
  AND,
  OR,
  LSHIFT,
  RSHIFT,
  ARSHIFT,
  XOR
};

extern std::map<BiOp, std::string> biOpNames;

enum class RelOp {
  EQ,
  NE,
  // signed integer
  LT,
  GT,
  LE,
  GE,
  // unsigned integer
  ULT,
  ULE,
  UGT,
  UGE
};

inline RelOp notRel(RelOp op) {
  switch (op) {
  case RelOp::EQ:
    return RelOp::NE;
  case RelOp::NE:
    return RelOp::EQ;
  case RelOp::LT:
    return RelOp::GE;
  case RelOp::GT:
    return RelOp::LE;
  case RelOp::LE:
    return RelOp::GT;
  case RelOp::GE:
    return RelOp::LT;
  case RelOp::ULT:
    return RelOp::UGE;
  case RelOp::ULE:
    return RelOp::UGT;
  case RelOp::UGT:
    return RelOp::ULE;
  case RelOp::UGE:
    return RelOp::ULT;
  }
}

extern std::map<RelOp, std::string> relOpNames;

struct IRNode {

  // LLVM-style RTTI begin
public:
  enum IRNodeKind {
    IK_Statement,
    IK_ExpressionStatement,
    IK_SequenceStatement,
    IK_LabelStatement,
    IK_JumpStatement,
    IK_CJumpStatement,
    IK_MoveStatement,
    IK_Statement_End,
    IK_Expression,
    IK_BinaryOperationExpression,
    IK_MemoryExpression,
    IK_TemporaryExpression,
    IK_SequenceExpression,
    IK_NameExpression,
    IK_ConstantExpression,
    IK_CallExpression,
    IK_Expression_End,
  };

private:
  IRNodeKind const Kind;

public:
  IRNodeKind getKind() const { return Kind; }

  IRNode(IRNodeKind k) : Kind{k} {}

  // LLVM-style RTTI end

public:
  virtual ~IRNode() {}

public:
  virtual std::string prettyPrint() const {
    std::stringstream ss;
    ss << "<IRNode><IRNode/>";
    return ss.str();
  };
};

#define IRCLASSOF(CLASSNAME) CLASSOF(CLASSNAME, IRNode, IK)
#define IRCLASSOFRANG(CLASSNAME) CLASSOFRANG(CLASSNAME, IRNode, IK, End)

// stand for the computation of some value (possibly with side effects)
struct Expression : public IRNode {
public:
  Expression(IRNodeKind k = IK_Expression) : IRNode{k} {}
  IRCLASSOFRANG(Expression)
};

// to perform side effects and control flow.
struct Statement : public IRNode {
public:
  Statement(IRNodeKind k = IK_Statement) : IRNode{k} {}
  IRCLASSOFRANG(Statement)
};

using ExprPtr = std::unique_ptr<Expression>;
using StmPtr = std::unique_ptr<Statement>;
using IRNodePtr = std::unique_ptr<IRNode>;

// The integer constant i.
struct ConstantExpression : Expression {
  int i;
  ConstantExpression(int c = 0) : Expression{IK_ConstantExpression}, i{c} {}

  IRCLASSOF(ConstantExpression)

public:
  std::string prettyPrint() const override;
};

// The symbolic constant name, corresponding to an assembly language label.
struct NameExpression : Expression {
  Label name;
  NameExpression() : NameExpression(Label()) {}
  NameExpression(Label n) : Expression{IK_NameExpression}, name{n} {}

  IRCLASSOF(NameExpression)

public:
  std::string prettyPrint() const override;
};

// A temporary in the abstract machine is similar to a register in a real
// machine.
// The abstract machine has infinite number of registers.
struct TemporaryExpression : Expression {
  Temporary temp;
  TemporaryExpression(Temporary t)
      : Expression{IK_TemporaryExpression}, temp{t} {}
  IRCLASSOF(TemporaryExpression)

public:
  std::string prettyPrint() const override;
};

// The application of binary operator op to operands lhs, rhs.
// lhs is evaluated before rhs.
struct BinaryOperationExpression : Expression {
  BiOp op;
  std::unique_ptr<Expression> lhs, rhs;
  BinaryOperationExpression(BiOp o, std::unique_ptr<Expression> &&l,
                            std::unique_ptr<Expression> &&r)
      : Expression{IK_BinaryOperationExpression}, op{o}, lhs{std::move(l)},
        rhs{std::move(r)} {}

  IRCLASSOF(BinaryOperationExpression)

public:
  std::string prettyPrint() const override;
};

// The contents of word size bytes of memory starting at address e.
// When Mem is used as left child of a Move, it means "store";
// but anywhere else it means "fetch".
struct MemoryExpression : Expression {
  std::unique_ptr<Expression> exp;
  MemoryExpression(std::unique_ptr<Expression> &&e)
      : Expression{IK_MemoryExpression}, exp{std::move(e)} {}
  IRCLASSOF(MemoryExpression)

public:
  std::string prettyPrint() const override;
};

// A procedure call: the application of function func to arguments.
// func is evaluated before arguments.
struct CallExpression : Expression {
  ExprPtr func;
  std::vector<ExprPtr> arguments;
  CallExpression(ExprPtr &&f, std::vector<ExprPtr> &&args)
      : Expression{IK_CallExpression}, func{std::move(f)}, arguments{std::move(
                                                               args)} {}
  IRCLASSOF(CallExpression)

public:
  std::string prettyPrint() const override;
};

// The statement stm is evaluated for side effects;
// then e is evaluated for a result.
struct SequenceExpression : Expression {
  StmPtr stm;
  ExprPtr exp;
  SequenceExpression(StmPtr &&s, ExprPtr &&e)
      : Expression{IK_SequenceExpression}, stm{std::move(s)}, exp{std::move(
                                                                  e)} {}
  IRCLASSOF(SequenceExpression)

public:
  std::string prettyPrint() const override;
};

// Copy the value of src to the location of dst.
struct MoveStatement : Statement {
  ExprPtr dst, src;
  MoveStatement(ExprPtr &&d, ExprPtr &&s)
      : Statement{IK_MoveStatement}, dst{std::move(d)}, src{std::move(s)} {}
  IRCLASSOF(MoveStatement)

public:
  std::string prettyPrint() const override;
};

// Evaluates exp and discards its result.
struct ExpressionStatement : Statement {
  ExprPtr exp;
  ExpressionStatement(ExprPtr &&e)
      : Statement{IK_ExpressionStatement}, exp{std::move(e)} {}
  IRCLASSOF(ExpressionStatement)

public:
  std::string prettyPrint() const override;
};

// Transfer control to address exp.
// The list of labels specifies all the possible locations
// that the expression exp can be evaluated to.
// This is necessary for dataflow analysis later.
struct JumpStatement : Statement {
  ExprPtr exp;
  std::vector<Label> possibleLocations;
  JumpStatement(ExprPtr &&e, std::vector<Label> &&ls)
      : Statement{IK_JumpStatement}, exp{std::move(e)}, possibleLocations{
                                                            std::move(ls)} {}
  IRCLASSOF(JumpStatement)

public:
  std::string prettyPrint() const override;
};

// Evaluates lhs, then rhs, yielding values a, b.
// Then compare a,b using op.
// If true, jump to yes; else jump to no.
struct CJumpStatement : Statement {
  RelOp op;
  ExprPtr lhs, rhs;
  Label yes, no;
  CJumpStatement(RelOp o, ExprPtr &&l, ExprPtr &&r, Label y, Label n)
      : Statement{IK_CJumpStatement}, op{o}, lhs{std::move(l)},
        rhs{std::move(r)}, yes{y}, no{n} {}

  // CJumpStatement(){}

  IRCLASSOF(CJumpStatement)

public:
  std::string prettyPrint() const override;
};

// head followed by tail.
struct SequenceStatement : Statement {
  std::unique_ptr<Statement> head;
  std::unique_ptr<Statement> tail;
  SequenceStatement(std::unique_ptr<Statement> &&h,
                    std::unique_ptr<Statement> &&t)
      : Statement{IK_SequenceStatement}, head{std::move(h)}, tail{std::move(
                                                                 t)} {}
  IRCLASSOF(SequenceStatement)

public:
  std::string prettyPrint() const override;
};

// Define constant value of label name to be the current machine code address.
// This is like a label definition in assembly language.
// The value name may be the target of jumps, calls or etc.
struct LabelStatement : Statement {
  Label name;
  LabelStatement(Label n) : Statement{IK_LabelStatement}, name{n} {}
  IRCLASSOF(LabelStatement)

public:
  std::string prettyPrint() const override;
};

#undef IRCLASSOF
#undef IRCLASSOFRANG

} // namespace IR
} // namespace KT

#endif /* IR_IR_H */
