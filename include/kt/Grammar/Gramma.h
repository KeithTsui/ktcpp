//===-- Gramma.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef GRAMMAR_GRAMMA_H
#define GRAMMAR_GRAMMA_H

#include "kt/Token/TokenType.h"
#include <vector>

namespace KT {

namespace Grammar {

struct ProductionRule {
  TokenType::Nonterminal nonterm;
  std::vector<int> production;
};

extern std::vector<ProductionRule> ktGrammar;

} // namespace Grammar

} // namespace KT

#endif /* GRAMMAR_GRAMMA_H */
